if __name__ == '__main__':
    from BernsteinFEM import BernsteinFEM
    from bernstein_mesh import BernsteinMesh
    import numpy as np
    import time
    from scipy.linalg import solve_triangular

    coords_oneelement = np.array([[0, 0, 0], [0, 0, 1], [0, 1, 0], [1, 0, 0]])
    elnode_oneelement = np.array([[0, 1, 2, 3]])
    faces = set([(0, 1, 2), (0, 1, 3), (0, 2, 3), (1, 2, 3)])
    lines = set()
    for face in faces:
        lines.add(tuple(sorted((face[0], face[1]))))
        lines.add(tuple(sorted((face[0], face[2]))))
        lines.add(tuple(sorted((face[1], face[2]))))
    bc_oneelement = {'faces': faces, 'lines': lines}

    for p in range(18, 19):
        print(p, end=' ')
        mesh = BernsteinMesh(coords_oneelement, elnode_oneelement, {'faces': set(), 'lines': set()}, p)

        fem = BernsteinFEM.BernsteinFEM(mesh)
        m, s, f = fem.assemble_matrix()

        # Retrieve interior dofs
        num_int_dofs = (-3 + p)*(-2 + p)*(-1 + p)/6
        m_ii = m[2*(1+p*p):, 2*(1+p*p):]

        L = np.linalg.cholesky(m_ii.todense())
        rhs = np.random.rand(L.shape[0])
        start = time.time()
        y = solve_triangular(L, rhs, lower=True, check_finite=False, overwrite_b=True)
        x = solve_triangular(L, y, trans=1, lower=True, check_finite=False, overwrite_b=True)
        print(time.time() - start)

        # print(np.linalg.norm(m_ii.dot(x) - rhs))

