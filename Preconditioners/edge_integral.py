"""
File contaning the functions needed for integration (e.g. determining the diagonals of the Schur complement)
"""
import numpy as np
from scipy.special import eval_jacobi as jac, roots_jacobi as rj
from quad import sum_quads


# Barycentric coordinates
def l1(x):
    return (- x[0] - x[1] - x[2] - 1) / 2


def l2(x):
    return (1 + x[0]) / 2


def l3(x):
    return (1 + x[1]) / 2


def l4(x):
    return (1 + x[2]) / 2


def cap_phi(x, m, p):
    """
    Magic Cap phi function which helps with the "tensor" product on simplices

    :param x: values
    :param m: weight
    :param p: order
    """
    return (-1) ** p * jac(p, m, 1, x) / (p + 1)


def gamma_sym(x: np.ndarray, i: int, p):
    j = np.floor((p - 2 - i) / 2)
    k = p - 2 - i - j
    total = l1(x) * l2(x) * (-1) ** (2 - i) * (-l1(x) - l2(x)) ** i * jac(i, 2, 2,
                                                                          (l2(x) - l1(x)) / (1 - l3(x) - l4(x))) \
            * ((1 - l4(x)) ** j * cap_phi(2 * l3(x) / (1 - l4(x)) - 1, 2 * i + 5, j) *
               cap_phi(2 * l4(x) - 1, 2 * i + 2 * j + 6, k)
               + (1 - l3(x)) ** j * cap_phi(2 * l4(x) / (1 - l3(x)) - 1, 2 * i + 5, j) *
               cap_phi(2 * l3(x) - 1, 2 * i + 2 * j + 6, k)) / 2

    return total


def gamma_nonsym(x: np.ndarray, i: int, p):
    j = np.floor((p - 2 - i) / 2)
    k = p - 2 - i - j
    total = l1(x) * l2(x) * (-1) ** (2 - i) * (-l1(x) - l2(x)) ** i * jac(i, 2, 2,
                                                                          (l2(x) - l1(x)) / (1 - l3(x) - l4(x))) \
            * ((1 - l4(x)) ** j * cap_phi(2 * l3(x) / (1 - l4(x)) - 1, 2 * i + 5, j) *
               cap_phi(2 * l4(x) - 1, 2 * i + 2 * j + 6, k))

    return total

def phi(x, p: int):
    i = np.floor(p / 2)
    j = np.floor(i / 2)
    k = p - 1 - i - j

    # j = np.floor((p-3)/3)
    # k = np.floor((p-3)/3)
    # i = p-1 - j - k
    total = l1(x) *  \
            ((-1 + l3(x) + l4(x)) ** i * cap_phi((l2(x) - l1(x)) / (1 - l3(x) - l4(x)), 2, i) * \
            (l1(x) + l2(x) + l3(x)) ** j * cap_phi(2 * l3(x) / (1 - l4(x)) - 1, 2 * i + 3, j) * \
            cap_phi(2*l4(x)-1, 2 * i + 2 * j + 4, k) +
             (-1 + l4(x) + l2(x)) ** i * cap_phi((l3(x) - l1(x)) / (1 - l4(x) - l2(x)), 2, i) * \
             (l1(x) + l3(x) + l4(x)) ** j * cap_phi(2 * l4(x) / (1 - l2(x)) - 1, 2 * i + 3, j) * \
            cap_phi(2*l2(x)-1, 2 * i + 2 * j + 4, k) +
            (-1 + l2(x) + l3(x)) ** i * cap_phi((l4(x) - l1(x)) / (1 - l2(x) - l3(x)), 2, i) * \
            (l1(x) + l4(x) + l2(x)) ** j * cap_phi(2 * l2(x) / (1 - l3(x)) - 1, 2 * i + 3, j) * \
            cap_phi(2*l3(x)-1, 2 * i + 2 * j + 4, k))/3
    return total

def find_diagonal_vertex(p:int) -> np.ndarray:
    """
    Given a degree p, finds the p-1 values on the diagonal of the edge Schur complement

    :param p: order of FEM
    :param sym: choice to use symmetric basis or not
    :return: array of diagonals
    """

    # Calculates the required quadrature nodes/points
    x00, w00 = rj(p + 1, 0, 0)
    x10, w10 = rj(p + 1, 1, 0)
    x20, w20 = rj(p + 1, 2, 0)

    # Create np array of points to have one function eval
    points1 = np.zeros(len(w00) * len(w10) * len(w20))
    points2 = np.zeros(len(w00) * len(w10) * len(w20))
    points3 = np.zeros(len(w00) * len(w10) * len(w20))
    func_counter = 0
    for i in range(len(w00)):
        for j in range(len(w10)):
            for k in range(len(w20)):
                points1[func_counter], points2[func_counter], points3[func_counter] = (
                    (.25 * (-3 + x00[i] - x10[j] - x00[i] * x10[j] + (1 + x00[i]) * (-1 + x10[j]) * x20[k]),
                     .5 * (-1 + x10[j] - x20[k] - x10[j] * x20[k]), x20[k]))
                func_counter += 1

    # Loop through the basis

    quad_point_array = np.reshape(phi((points1, points2, points3), p),
                                          (len(w00), len(w10), len(w20)))

    return sum_quads(quad_point_array, quad_point_array, w00, w10, w20)

def find_diagonal_edge(p:int, sym=False) -> np.ndarray:
    """
    Given a degree p, finds the p-1 values on the diagonal of the edge Schur complement

    :param p: order of FEM
    :param sym: choice to use symmetric basis or not
    :return: array of diagonals
    """

    # Calculates the required quadrature nodes/points
    x00, w00 = rj(p + 1, 0, 0)
    x10, w10 = rj(p + 1, 1, 0)
    x20, w20 = rj(p + 1, 2, 0)

    # Create np array of points to have one function eval
    points1 = np.zeros(len(w00) * len(w10) * len(w20))
    points2 = np.zeros(len(w00) * len(w10) * len(w20))
    points3 = np.zeros(len(w00) * len(w10) * len(w20))
    func_counter = 0
    for i in range(len(w00)):
        for j in range(len(w10)):
            for k in range(len(w20)):
                points1[func_counter], points2[func_counter], points3[func_counter] = (
                    (.25 * (-3 + x00[i] - x10[j] - x00[i] * x10[j] + (1 + x00[i]) * (-1 + x10[j]) * x20[k]),
                     .5 * (-1 + x10[j] - x20[k] - x10[j] * x20[k]), x20[k]))
                func_counter += 1

    # Loop through the basis
    output = np.zeros((p-1,))
    for i in range(p - 1):
        if sym:
            quad_point_array = np.reshape(gamma_sym((points1, points2, points3), i, p),
                                          (len(w00), len(w10), len(w20)))
        else:
            quad_point_array = np.reshape(gamma_nonsym((points1, points2, points3), i, p),
                                          (len(w00), len(w10), len(w20)))

        output[i] = sum_quads(quad_point_array, quad_point_array, w00, w10, w20)

    # See if this is correct
    return output

if __name__ == '__main__':
    print(find_diagonal_edge(4, sym=True))