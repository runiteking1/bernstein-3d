import numpy as np

import read_msh
from BernsteinFEM import BernsteinFEM
from bernstein_mesh import BernsteinMesh
from scipy.sparse import csr_matrix
from scipy.sparse.linalg import spsolve, LinearOperator

# from matplotlib import pyplot as plt

class Preconditioner(LinearOperator):
    """
    General preconditioner object to handle 3D FEM matrices
    """
    def __init__(self, matrix: np.ndarray, finiteElement: BernsteinFEM):
        """
        :param matrix: Matrix to pass in (assume we don't do matrix less)
        :param finiteElement: BernsteinFEM class which contains all the information
        """
        super(Preconditioner, self).__init__(dtype=matrix.dtype, shape=matrix.shape)

        # Set some parameters related to partitioning the int, face, edges and vertices
        self.num_vertices = len(finiteElement.coordinates)
        self.num_edges = finiteElement.num_edges
        self.num_faces = finiteElement.num_faces
        self.num_elements = finiteElement.elements
        self.p = finiteElement.p

        if self.p < 4:
            raise NotImplementedError("Degree is too low for interior matrices")

        self.edge_dofs = self.p - 1
        self.face_dofs = int((self.p - 2)*(self.p - 1) / 2) # Number of dofs per face
        self.int_dofs = int((self.p - 3) * (self.p - 2) * (self.p - 1)/6) # Number of dofs by interior

        # Now need to save some indexing information
        self.first_edge_index = self.num_vertices
        self.first_face_index = self.num_vertices + self.edge_dofs * self.num_edges
        self.first_interior_index = self.first_face_index + self.face_dofs * self.num_faces

        # Save the interior matrix
        self.interior_matrix = csr_matrix((self.int_dofs * self.num_elements, self.int_dofs * self.num_elements))
        self.boundary_length = matrix.shape[0] - self.interior_matrix.shape[0]
        self.interior_matrix = matrix[self.boundary_length:, self.boundary_length:].tocsc()

        # Save boundary condition
        self.dirchlet_dofs = np.array(list(finiteElement.dirchlet_dofs), dtype=np.int)

        # Create Schur complement
        self.b = matrix[0:self.boundary_length, self.boundary_length:].tocsr()
        # self.schur =  matrix[0:self.boundary_length, 0:self.boundary_length] - \
        #              self.b @ spsolve(self.interior_matrix, self.b.T)

    def solve(self, f: np.ndarray):
        raise NotImplementedError("Please implement preconditioner.")

    def _matvec(self, x):
        return self.solve(x)

if __name__ == '__main__':
    vertices, elements, boundary = read_msh.load_file('../cube.geo', refine=0)
    mesh = BernsteinMesh(vertices, elements, boundary, 5)

    fem = BernsteinFEM.BernsteinFEM(mesh)
    m, s, f = fem.assemble_matrix()

    Preconditioner(m, fem)
