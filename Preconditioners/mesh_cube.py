import numpy as np


def mesh_cube(corres_dofs: np.ndarray) -> np.ndarray:
    """
    Given a cube with vertices laid out in order:
    {-1, -1, -1}, {1, -1, -1}, {1, 1, -1}, {-1, 1, -1}, {-1, -1, 1}, {1, -1, 1}, {1, 1, 1}, {-1, 1, 1}

    and the corresponding dofs of the respective vertices, gives a ELNODE array which generates 6 tetrahedrons.

    :type corres_dofs: np.ndarray
    :param corres_dofs: Degree of freedom numbering of cube in the above numbering.
    :return: 2D array of ELNODE connectivity data which meshes the cube
    """
    # Data from Mathematica file
    tid = np.array([[1, 2, 6, 7], [1, 2, 3, 7], [1, 5, 6, 7], [1, 4, 7, 8], [1, 5, 7, 8], [1, 3, 4, 7]])
    tid = tid - 1  # Scale

    return np.sort(corres_dofs[tid])  # Sort such that we have smallest to largest dof ordering


if __name__ == '__main__':
    # Simple testing
    dofs = np.array([0, 1, 2, 10, 4, 5, 6, 7], dtype=np.int)
    print(mesh_cube(dofs))

    # Now let's test with a more complicated example
    dofs_smallcube = np.array([0, 1, 2, 3, 4, 5, 6, 7], dtype=np.int)
    dofs_column = np.array([8, 9, 10, 11, 0, 1, 2, 3], dtype=np.int)

    print(mesh_cube(dofs_smallcube) )
    print(mesh_cube(dofs_column) + 1)
