from preconditioner import *
from scipy.sparse import  block_diag

from matplotlib import pyplot as plt

class NaivePreconditioner(Preconditioner):
    def __init__(self, matrix: np.ndarray, finiteElement: BernsteinFEM):
        # Creates the basic Schur complement
        super(NaivePreconditioner, self).__init__(matrix, finiteElement)

        # Save face Schur blocks
        block_list = list()
        counter = self.first_face_index
        for face in range(self.num_faces):
            block_list.append(self.schur[counter:counter + self.face_dofs, counter:counter + self.face_dofs])
            counter += self.face_dofs
        self.face_matrix = block_diag(block_list).tocsc()

        # Save edge Schur blocks
        block_list = list()
        counter = self.first_edge_index
        for edge in range(self.num_edges):
            block_list.append(self.schur[counter:counter + self.edge_dofs, counter:counter + self.edge_dofs])
            # print(block_list[edge].todense())
            counter += self.edge_dofs
        self.edge_matrix = block_diag(block_list).tocsc()

        # Save vertex Schur block
        self.vertex_matrix = self.schur[0:self.num_vertices, 0:self.num_vertices]

        # print(self.num_vertices, self.num_edges, self.num_faces, self.num_elements)

    def solve(self, f: np.ndarray):
        """
        Outputs P^{-1}f

        :param f: rhs to invert
        :return: preconditioner applied to f
        """
        out = np.zeros_like(f)

        # Inverst interior functions first
        out[self.boundary_length:] = spsolve(self.interior_matrix, f[self.boundary_length:])

        # Calculate rhs
        rhs = f[0:self.boundary_length] - self.b @ out[self.boundary_length:]

        out[0:self.num_vertices] = spsolve(self.vertex_matrix, rhs[0:self.num_vertices])
        out[self.first_edge_index:self.first_face_index] = spsolve(self.edge_matrix,
                                                                   rhs[self.first_edge_index:self.first_face_index])
        out[self.first_face_index:self.first_interior_index] = spsolve(self.face_matrix,
                                                                    rhs[self.first_face_index:self.first_interior_index])

        return out

if __name__ == '__main__':
    vertices, elements, boundary = read_msh.load_file('../cube.geo', refine=0)
    mesh = BernsteinMesh(vertices, elements, {'faces': set(), 'lines': set()}, 5)
    coords_threeelement = np.array([[0, 0, 0], [0, 0, 1], [0, 1, 0], [1, 0, 0], [1, 1, 1], [1, 0, 1]])
    elnode_threeelement = np.array([[0, 1, 2, 3], [1, 2, 3, 4], [1, 3, 4, 5]])
    faces = {(0, 1, 2), (0, 1, 3), (0, 2, 3), (1, 2, 4), (2, 3, 4), (1, 3, 5), (1, 4, 5), (3, 4, 5)}
    lines = set()
    for face in faces:
        lines.add(tuple(sorted((face[0], face[1]))))
        lines.add(tuple(sorted((face[0], face[2]))))
        lines.add(tuple(sorted((face[1], face[2]))))
    bc_threeelement = {'faces': faces, 'lines': lines}
    # mesh = BernsteinMesh(coords_threeelement, elnode_threeelement, bc_threeelement, 10)

    fem = BernsteinFEM.BernsteinFEM(mesh)
    m, s, f = fem.assemble_matrix()
    Solver = NaivePreconditioner(m, fem)

    # print(Solver.solve(m @ np.ones(m.shape[0])))
    import scipy
    from scipy.sparse.linalg import LinearOperator
    def mult_precond(v: np.ndarray) -> np.ndarray:
        return Solver.dot((m) @ v)

    PinvS = LinearOperator(dtype=m.dtype, shape=m.shape, matvec=mult_precond)

    w = scipy.sparse.linalg.eigs(PinvS, which='LM', return_eigenvectors=False, k=1, tol=1E-8,
                             maxiter=m.shape[0] * 50, ncv=m.shape[0] * 5)
    large = max(np.real(w))

    w = scipy.sparse.linalg.eigs(PinvS, which='SM', return_eigenvectors=False, k=1, maxiter=m.shape[0] * 100,
                             tol=1E-9, ncv=m.shape[0] * 30)
    small = min(np.real(w))
    print(large, small)
    print("{:1.2f}".format((large / small)), end='\n')

