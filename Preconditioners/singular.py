import numpy as np
import mesh_cube
from BernsteinFEM import BernsteinFEM
from Mesh import bernstein_mesh
from mass_preconditioner import NaivePreconditioner
from pcg import pcg
import scipy
from scipy.sparse.linalg import LinearOperator


def solve_singular_perturbed(varepsilon_squared: float, p: int):
    # width = p * np.sqrt(varepsilon_squared)
    width = .45
    if (width  > 1):
        print("Width too big")
        return
    print(width)
    # nodes = np.array([[-width, -width, -width],     # 0
    #                   [width, -width, -width],      # 1
    #                   [width, width, -width],       # 2
    #                   [-width, width, -width],      # 3
    #                   [-width, -width, width],      # 4
    #                   [width, -width, width],       # 5
    #                   [width, width, width],        # 6
    #                   [-width, width, width],       # 7
    #                   [-width, -width, -1 + 2 * width],         # 8
    #                   [width, -width, -1 + 2 * width],          # 9
    #                   [width, width, -1 + 2 * width],           # 10
    #                   [-width, width, -1 + 2 * width],          # 11
    #                   [-1 + 2 * width, -width, -width],         # 12
    #                   [-1 + 2 * width, width, -width],          # 13
    #                   [-1 + 2 * width, -width, width],          # 14
    #                   [-1 + 2 * width, width, width],           # 15
    #                   [-width, 1 - 2 *width, -width],          # 16
    #                   [width, 1 - 2 *width, -width],           # 17
    #                   [-width, 1 - 2 *width, width],           # 18
    #                   [width, 1 - 2 *width, width],            # 19
    #                   [-1 + 2 * width, 1 - 2 *width, -width],              # 20
    #                   [-1 + 2 * width, 1 - 2 *width, width],               # 21
    #                   [-1 + 2 * width, -width, -1 + 2 * width],             # 22
    #                   [-1 + 2 * width, width, -1 + 2 * width],              # 23
    #                   [width, 1 - 2 *width, -1 + 2 * width],               # 24
    #                   [-width, 1 - 2 *width, -1 + 2 * width],              # 25
    #                   [-1 + 2 * width, 1 - 2 *width, -1 + 2 * width]                   # 26
    #                   ])

    nodes = np.array([[-width, -width, -width],  # 0
                      [width, -width, -width],  # 1
                      [width, width, -width],  # 2
                      [-width, width, -width],  # 3
                      [-width, -width, width],  # 4
                      [width, -width, width],  # 5
                      [width, width, width],  # 6
                      [-width, width, width],  # 7
                      [-width, -width, -1 + 1 * width],  # 8
                      [width, -width, -1 + 1 * width],  # 9
                      [width, width, -1 + 1 * width],  # 10
                      [-width, width, -1 + 1 * width],  # 11
                      [-1 + 1 * width, -width, -width],  # 12
                      [-1 + 1 * width, width, -width],  # 13
                      [-1 + 1 * width, -width, width],  # 14
                      [-1 + 1 * width, width, width],  # 15
                      [-width, 1 - 1 * width, -width],  # 16
                      [width, 1 - 1 * width, -width],  # 17
                      [-width, 1 - 1 * width, width],  # 18
                      [width, 1 - 1 * width, width],  # 19
                      [-1 + 1 * width, 1 - 1 * width, -width],  # 20
                      [-1 + 1 * width, 1 - 1 * width, width],  # 21
                      [-1 + 1 * width, -width, -1 + 1 * width],  # 22
                      [-1 + 1 * width, width, -1 + 1 * width],  # 23
                      [width, 1 - 1 * width, -1 + 1 * width],  # 24
                      [-width, 1 - 1 * width, -1 + 1 * width],  # 25
                      [-1 + 1 * width, 1 - 1 * width, -1 + 1 * width]  # 26
                      ])

    # print(nodes[11], nodes[13], nodes[16])
    nodes = 100 * nodes

    dofs_smallcube = np.array([0, 1, 2, 3, 4, 5, 6, 7], dtype=np.int)
    dofs_lower_column = np.array([8, 9, 10, 11, 0, 1, 2, 3], dtype=np.int)
    dofs_left_column = np.array([12, 0, 3, 13, 14, 4, 7, 15], dtype=np.int)
    dofs_right_column = np.array([3, 2, 17, 16, 7, 6, 19, 18], dtype=np.int)
    dofs_top_slab = np.array([13, 3, 16, 20, 15, 7, 18, 21], dtype=np.int)
    dofs_left_slab = np.array([22, 8, 11, 23, 12, 0, 3, 13], dtype=np.int)
    dofs_right_slab = np.array([11, 10, 24, 25, 3, 2, 17, 16], dtype=np.int)
    dofs_cube = np.array([23, 11, 25, 26, 13, 3, 16, 20], dtype=np.int)
    elnode = np.vstack((mesh_cube.mesh_cube(dofs_smallcube),
                        mesh_cube.mesh_cube(dofs_lower_column),
                        mesh_cube.mesh_cube(dofs_left_column),
                        mesh_cube.mesh_cube(dofs_right_column),
                        mesh_cube.mesh_cube(dofs_top_slab),
                        mesh_cube.mesh_cube(dofs_left_slab),
                        mesh_cube.mesh_cube(dofs_right_slab),
                        mesh_cube.mesh_cube(dofs_cube)))

    # print(nodes)
    # print(elnode + 1)
    # # Now we have to set the boundary conditions
    faces = {(15, 18, 21), (7, 15, 18), (7, 14, 15), (4, 7, 14), (4, 6, 7), (4, 5, 6), (7, 18, 19), (6, 7, 19),
             (4, 12, 14), (0, 4, 12), (0, 12, 22), (0, 8, 22), (0, 1, 8), (0, 1, 5), (0, 4, 5), (1, 8, 9),
             (1, 5, 6), (1, 2, 9), (1, 2, 6), (2, 9, 10), (2, 6, 19), (2, 17, 19), (2, 10, 17), (10, 17, 24)}

    lines = set()
    for face in faces:
        lines.add(tuple(sorted((face[0], face[1]))))
        lines.add(tuple(sorted((face[0], face[2]))))
        lines.add(tuple(sorted((face[1], face[2]))))

    mesh = bernstein_mesh.BernsteinMesh(nodes, elnode, {'faces': faces, 'lines': lines}, p)
    # mesh = bernstein_mesh.BernsteinMesh(nodes, elnode, {'faces': set(), 'lines': set()}, p)
    fem = BernsteinFEM.BernsteinFEM(mesh)

    m, s, f = fem.assemble_matrix()
    print("Matrix constructed", m.shape)
    Solver = NaivePreconditioner(m, fem)
    print("Preconditioner constructed", m.shape)


    def mult_precond(v: np.ndarray) -> np.ndarray:
        return Solver.dot((m + varepsilon_squared * s) @ v)
        # return (m + varepsilon_squared * s) @ v
        # return Solver.dot((s) @ v)
    PinvS = LinearOperator(dtype=m.dtype, shape=m.shape, matvec=mult_precond)

    w = scipy.sparse.linalg.eigs(PinvS, which='LM', return_eigenvectors=False, k=1, tol=1E-9,
                                 maxiter=m.shape[0] * 50, ncv=m.shape[0] * 5)
    # w = scipy.sparse.linalg.eigs(m, which='LM', return_eigenvectors=False, k=1, tol=1E-8,
    #                              maxiter=m.shape[0] * 50, ncv=m.shape[0] * 5)

    large = max(np.real(w))

    w = scipy.sparse.linalg.eigs(PinvS, which='SM', return_eigenvectors=False, k=1, maxiter=m.shape[0] * 100,
                                 tol=1E-9, ncv=m.shape[0] * 30)
    # w = scipy.sparse.linalg.eigs(m, which='SM', return_eigenvectors=False, k=1, maxiter=m.shape[0] * 100,
    #                              tol=1E-9, ncv=m.shape[0] * 30)

    small = min(np.real(w))
    print("Order: ", p, " {:1.2f}".format((large / small)), large, small, mesh.elements, end='\n')
    #
    # x, it, resid = pcg(m + varepsilon_squared*s, f, P=Solver, eps=1e-10)
    # # x, it, resid = pcg(m , f, P=Solver, eps=1e-8)
    # # print(it, resid)
    # # fem.plot_boundaries(x)
    # fem.plot(x)


if __name__ == '__main__':
    # Solves the singular perturbed problem
    eps_squred = 1.0e-1
    print(eps_squred)
    solve_singular_perturbed(eps_squred, 4)
    solve_singular_perturbed(eps_squred, 5)
    solve_singular_perturbed(eps_squred, 6)
    solve_singular_perturbed(eps_squred, 7)
    solve_singular_perturbed(eps_squred, 8)
    solve_singular_perturbed(eps_squred, 9)
    # #
    # eps_squred = 1.00e-3
    # print(eps_squred)
    # solve_singular_perturbed(eps_squred, 10)
    # solve_singular_perturbed(eps_squred, 5)
    # solve_singular_perturbed(eps_squred, 8)
    # solve_singular_perturbed(eps_squred, 7)
    # solve_singular_perturbed(eps_squred, 8)
    # solve_singular_perturbed(eps_squred, 9)
    # #
    # eps_squred = 1.0e-7
    # print(eps_squred)
    # solve_singular_perturbed(eps_squred, 4)
    # solve_singular_perturbed(eps_squred, 5)
    # solve_singular_perturbed(eps_squred, 6)
    # solve_singular_perturbed(eps_squred, 7)
    # solve_singular_perturbed(eps_squred, 8)
    # solve_singular_perturbed(eps_squred, 9)
    eps_squred = 1.00e-9
    # print(eps_squred)
    # solve_singular_perturbed(eps_squred, 4)
    # solve_singular_perturbed(eps_squred, 5)
    # solve_singular_perturbed(eps_squred, 6)
    # solve_singular_perturbed(eps_squred, 7)
    # solve_singular_perturbed(eps_squred, 8)
    # solve_singular_perturbed(eps_squred, 9)
