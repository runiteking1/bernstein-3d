from preconditioner import *
from scipy.sparse import block_diag, csr_matrix
import CythonKernels.face_interior as face_interior
from scipy.special import binom, factorial
from matplotlib import pyplot as plt
from BernsteinFEM import volume
from edge_integral import find_diagonal_edge, find_diagonal_vertex

#TODO: URGENT ADD DIRCHLET BOUNDARY CONDITION

class NaivePreconditioner(Preconditioner):
    def __init__(self, matrix: np.ndarray, finiteElement: BernsteinFEM):
        # Creates the basic Schur complement
        super(NaivePreconditioner, self).__init__(matrix, finiteElement)
        np.set_printoptions(linewidth=120, precision=4)
        # Save face Schur blocks
        # block_list = list()
        # counter = self.first_face_index
        # for face in range(self.num_faces):
        #     block_list.append(self.schur[counter:counter + self.face_dofs, counter:counter + self.face_dofs])
        #     counter += self.face_dofs
        # self.face_matrix = block_diag(block_list).tocsc()
        # print(self.face_matrix.todense())

        # Save edge Schur blocks
        # block_list = list()
        # counter = self.first_edge_index
        # for edge in range(self.num_edges):
        #     block_list.append(self.schur[counter:counter + self.edge_dofs, counter:counter + self.edge_dofs])
        #     # print(block_list[edge].todense())
        #     counter += self.edge_dofs
        # self.edge_matrix = block_diag(block_list).tocsc()

        # Save vertex Schur block
        # self.vertex_matrix = self.schur[0:self.num_vertices, 0:self.num_vertices]

        # print(self.num_vertices, self.num_edges, self.num_faces, self.num_elements)

        # Create the objects necessary for the preconditioner
        self.face_inverse = FaceInverse(finiteElement)
        self.edge_inverse = EdgeInverse(finiteElement)
        self.vert_inverse = VertexInverse(finiteElement)

    def solve(self, f: np.ndarray):
        """
        Outputs P^{-1}f

        :param f: rhs to invert
        :return: preconditioner applied to f
        """
        out = np.zeros_like(f, dtype=np.float)

        # Inverst interior functions first
        out[self.boundary_length:] = spsolve(self.interior_matrix, f[self.boundary_length:])

        # Calculate rhs
        rhs = f[0:self.boundary_length] - self.b @ out[self.boundary_length:]

        # Solve vertex portion
        # out[0:self.num_vertices] = spsolve(self.vertex_matrix, rhs[0:self.num_vertices])
        out[0:self.first_interior_index] += self.vert_inverse.dot(rhs[0:self.first_interior_index])

        # Solve edge portion
        # out[self.first_edge_index:self.first_face_index] = spsolve(self.edge_matrix,
        #                                                            rhs[self.first_edge_index:self.first_face_index])
        # print(np.shape(out[self.first_edge_index:self.first_face_index]))
        # print(np.shape(self.edge_inverse.dot(rhs[self.first_edge_index:self.first_interior_index])))
        out[self.first_edge_index:self.first_interior_index] += self.edge_inverse.dot(
            rhs[self.first_edge_index:self.first_interior_index])

        # Solve the faces
        out[self.first_face_index:self.first_interior_index] += self.face_inverse.dot(
            rhs[self.first_face_index:self.first_interior_index])
        # Explicit (inefficient)
        # out[self.first_face_index:self.first_interior_index] += spsolve(self.face_matrix,
        #                                                                rhs[
        #                                                                self.first_face_index:self.first_interior_index])

        # Apply dirichlet dofs
        out[self.dirchlet_dofs] = f[self.dirchlet_dofs]

        # Now need to correct the interior
        out[self.boundary_length:] -= spsolve(self.interior_matrix, self.b.T.dot(out[0:self.first_interior_index]))

        return out


class EdgeInverse(LinearOperator):
    """
    Object to work with the inverse on the edge.

    A possibility that I extend this even to apply to the vertices also
    """

    def __init__(self, fem: BernsteinFEM):
        self.p = fem.p
        self.elements = fem.elements
        self.num_edges = fem.num_edges
        self.num_faces = fem.num_faces
        self.lookup_multi = fem.lookup_multi
        self.lookup_dof = fem.lookup_dof
        self.eldof = fem.eldof

        # Find dofs
        self.face_dofs = int((self.p + 2) * (self.p + 1) / 2) - 3 * (self.p - 1) - 3
        self.edge_dofs = self.p - 1

        self.size_system = self.num_edges * self.edge_dofs + self.face_dofs * self.num_faces
        super(EdgeInverse, self).__init__(dtype=np.double, shape=(self.size_system, self.size_system))

        # Now we create the matrix which will hold the change of basis matrix
        self.transformation2chi = np.zeros((self.edge_dofs * self.num_edges, self.size_system))
        self.diagonal = np.zeros((self.num_edges * self.edge_dofs))

        # First find the coefficients for transformation
        coefficients = np.zeros((self.p - 1, self.edge_dofs + 2, self.edge_dofs + 2))  # There are p - 1 total basis
        for i in range(self.p - 1):  # Loop over the number of basis functions
            # Set the parameter for j
            j = int(np.floor((self.p - i - 2) / 2))

            # Number for m, r (see Manuel's interior solve explanation in paper)
            r = i
            m = r + j
            # print(i, j, m, r)

            # Using symmetric basis, hence there's the bottom part and the front faces (assuming ref tetrahedron)
            holder_bottom = np.zeros((self.edge_dofs + 2, self.edge_dofs + 2))
            holder_front = np.zeros((self.edge_dofs + 2, self.edge_dofs + 2))
            for alpha3 in range(m + 1):
                for alpha2 in range(m + 1 - alpha3):
                    alpha1 = m - alpha3 - alpha2
                    # print(i,j,k,a_coef(m, r, np.array([i,j,k]), 2, 2, 2*r + 5, 1), bern(x, [i,j,k]))
                    # print(a_coef(m, r, np.array([alpha1, alpha2, alpha3]), 2, 2, 2 * r + 5, 1) * (alpha1 + 1) * (alpha2 + 1) / ((m + 1) * (m + 2)))
                    # holder_bottom[alpha1 + 1, alpha2 + 1] = a_coef(m, r, np.array([alpha1, alpha2, alpha3]), 2, 2,
                    #                                                2 * r + 5, 1) * (alpha1 + 1) * (alpha2 + 1) / (
                    #                                                     (m + 1) * (m + 2))

                    # We can either use the more flexible version of a_coef_front
                    holder_bottom[alpha1 + 1, alpha2 + 1] = a_coef_front(m, r, np.array([alpha1, alpha2, alpha3]), 2, 2,
                                                                        2 * r + 5, 1,
                                                                        0) * (alpha1 + 1) * (alpha2 + 1) / (
                                                                       (m + 0 + 1) * (m + 0 + 2))

            holder_bottom *= (-1) ** (m - r) / (m - r + 1)

            # print(holder)
            if m + 2 < self.p:
                for counter in range(self.p - m - 2):
                    self.degree_raise_2d(holder_bottom, m + 2 + counter)

            r = i
            m = self.p - 2 - j
            k = self.p - 2 - m
            for alpha3 in range(m - r + 1):
                for alpha2 in range(m - alpha3 + k + 1):
                    alpha1 = m - alpha3 - alpha2 + k
                    holder_front[alpha1 + 1, alpha2 + 1] = a_coef_front(m, r, np.array([alpha1, alpha2, alpha3]), 2, 2,
                                                                        2 * r + 2 * self.p - 2 * m + 2, 1,
                                                                        k) * (alpha1 + 1) * (alpha2 + 1) / (
                                                                       (m + k + 1) * (m + k + 2))

            holder_front *= (-1) ** (m - r) / (m - r + 1)

            # The holder for front is of full degree p already; no need for degree raise
            # print(holder_bottom)
            # print(holder_front)
            # print(holder_bottom/2 + holder_front/2)
            # self.edge_face((0, 0, self.p, 0), (0, self.p, 0, 0), holder_bottom/2 + holder_front/2)
            # print()

            coefficients[i] = holder_front / 2 + holder_bottom / 2

        offset = len(fem.coordinates)
        diagonal_ref = find_diagonal_edge(self.p, sym=True)

        self.diagonal = np.zeros(((self.p - 1) * self.num_edges))
        for element in range(fem.elements):
            # Find the volume scaling
            element_volume_scaling = volume.volume(fem.coordinates[fem.elnode[element, 0]],
                                                   fem.coordinates[fem.elnode[element, 1]],
                                                   fem.coordinates[fem.elnode[element, 2]],
                                                   fem.coordinates[fem.elnode[element, 3]])[0] / (4 / 3.0)
            # print("Volume scaling is: ", element_volume_scaling)

            # print(element, fem.eldof[element])
            # Go through all the edges
            # print(fem.lookup_dof[fem.eldof[element][0]], fem.lookup_dof[fem.eldof[element][1]])
            for j in range(self.p - 1):
                # First edge
                localdofs, localcoefs = self.edge_face(fem.lookup_dof[0],
                                                       fem.lookup_dof[1], coefficients[j])
                global_dofs = self.eldof[element][localdofs] - offset
                self.transformation2chi[global_dofs[j], global_dofs] = localcoefs
                self.diagonal[global_dofs[j]] += element_volume_scaling * diagonal_ref[j]

                # Second edge
                localdofs, localcoefs = self.edge_face(fem.lookup_dof[0],
                                                       fem.lookup_dof[2], coefficients[j])

                global_dofs = self.eldof[element][localdofs] - offset
                self.transformation2chi[global_dofs[j], global_dofs] = localcoefs
                self.diagonal[global_dofs[j]] += element_volume_scaling * diagonal_ref[j]

                # Third edge
                localdofs, localcoefs = self.edge_face(fem.lookup_dof[0],
                                                       fem.lookup_dof[3], coefficients[j])

                global_dofs = self.eldof[element][localdofs] - offset
                self.transformation2chi[global_dofs[j], global_dofs] = localcoefs
                self.diagonal[global_dofs[j]] += element_volume_scaling * diagonal_ref[j]

                # Forth edge
                localdofs, localcoefs = self.edge_face(fem.lookup_dof[1],
                                                       fem.lookup_dof[2], coefficients[j])

                global_dofs = self.eldof[element][localdofs] - offset
                self.transformation2chi[global_dofs[j], global_dofs] = localcoefs
                self.diagonal[global_dofs[j]] += element_volume_scaling * diagonal_ref[j]

                # Fifth edge
                localdofs, localcoefs = self.edge_face(fem.lookup_dof[1],
                                                       fem.lookup_dof[3], coefficients[j])

                global_dofs = self.eldof[element][localdofs] - offset
                self.transformation2chi[global_dofs[j], global_dofs] = localcoefs
                self.diagonal[global_dofs[j]] += element_volume_scaling * diagonal_ref[j]

                # Sixth edge
                localdofs, localcoefs = self.edge_face(fem.lookup_dof[2],
                                                       fem.lookup_dof[3], coefficients[j])

                global_dofs = self.eldof[element][localdofs] - offset
                self.transformation2chi[global_dofs[j], global_dofs] = localcoefs
                self.diagonal[global_dofs[j]] += element_volume_scaling * diagonal_ref[j]

        # Have to invert first
        self.diagonal = csr_matrix(np.diag(1/self.diagonal))
        self.transformation2chi = csr_matrix(self.transformation2chi)
        # print(self.diagonal)

        # self.total_transform = csr_matrix(self.transformation2chi.T @ np.diag(self.diagonal) @ self.transformation2chi)
        # from matplotlib import pyplot as plt
        # plt.spy(self.total_transform)
        # plt.show()

    @staticmethod
    def degree_raise_2d(cpoints: np.ndarray, m: int):
        """
        Degree raising algorithm; m is the
        :param cpoints:
        :param m: order to be raised to!
        :return:
        """
        for k in range(m + 1, -1, -1):
            for i in range(m + 1 - k, -1, -1):
                holder = 0
                if i > 0:
                    holder = holder + i * cpoints[i - 1, k] / (m + 1.0)
                if k > 0:
                    holder = holder + k * cpoints[i, k - 1] / (m + 1.0)
                if m + 1 - k - i > 0:
                    holder = holder + (m + 1 - k - i) * cpoints[i, k] / (m + 1.0)

                cpoints[i, k] = holder

    def _matvec(self, x: np.ndarray):
        # print(np.diag(self.diagonal).dot(self.transformation2chi))
        # print(self.transformation2chi.shape)
        # print(np.shape(self.transformation2chi.dot(x)))
        # print(self.total_transform.dot(x))
        return self.transformation2chi.T @ self.diagonal @ self.transformation2chi.dot(x)

    def edge_face(self, mi_one, mi_two, face_coef: np.ndarray):
        """

        :param mi_one: multiindex-one
        :param mi_two: multiindex-two
        :param face_coef: the coefficient of the face
        :return:
        """
        verbose = False

        nonzero_one = np.nonzero(mi_one)[0][0]
        nonzero_two = np.nonzero(mi_two)[0][0]

        # Indices for the additional face
        new_face = [0, 1, 2, 3]

        if verbose:
            print(nonzero_one, nonzero_two, mi_one, mi_two)

        new_face.remove(nonzero_one)
        new_face.remove(nonzero_two)

        # Create arrays for output
        local_dofs = np.zeros(((self.p - 1) + (self.p - 2) * (self.p - 1)), dtype=np.int)
        corrs_coef = np.zeros_like(local_dofs, dtype=np.float)
        counter = 0

        if verbose:
            print("Multiindices:", mi_one, mi_two)
            print("Face coefficients are as follows:")
            print(face_coef)

        # First look at the edge between the two
        holder = np.zeros_like(mi_one)
        for i in range(self.p - 1):
            holder[:] = 0
            holder[nonzero_one] = self.p - (i + 1)
            holder[nonzero_two] = i + 1
            local_dofs[counter] = self.lookup_multi[tuple(holder)]
            corrs_coef[counter] = face_coef[self.p - (i + 1), i + 1]
            counter += 1
            if verbose:
                print("Multiindex: ", holder, " corresponds to", self.lookup_multi[tuple(holder)], "with coefficient",
                      face_coef[p - (i + 1), i + 1])

        # Next, look at the faces
        for i in range(self.p - 1):
            holder[:] = 0
            holder[new_face[0]] = i + 1

            # Now loop over the row
            for j in range(self.p - 1 - (i + 1)):
                holder[nonzero_one] = (self.p - (i + 1)) - (j + 1)
                holder[nonzero_two] = j + 1

                local_dofs[counter] = self.lookup_multi[tuple(holder)]
                corrs_coef[counter] = face_coef[self.p - (i + 1) - (j + 1), j + 1]
                counter += 1

                if verbose:
                    print("Multiindex: ", holder, " corresponds to", self.lookup_multi[tuple(holder)],
                          "with coefficient",
                          face_coef[p - (i + 1) - (j + 1), j + 1])

            # Now the second face
            holder[:] = 0
            holder[new_face[1]] = i + 1

            # Now loop over the row
            for j in range(self.p - 1 - (i + 1)):
                holder[nonzero_one] = (self.p - (i + 1)) - (j + 1)
                holder[nonzero_two] = j + 1

                local_dofs[counter] = self.lookup_multi[tuple(holder)]
                corrs_coef[counter] = face_coef[self.p - (i + 1) - (j + 1), j + 1]
                counter += 1

                if verbose:
                    print("Multiindex: ", holder, " corresponds to", self.lookup_multi[tuple(holder)],
                          "with coefficient",
                          face_coef[p - (i + 1) - (j + 1), j + 1])

        return local_dofs, corrs_coef


def numr(m: int, r: int, alpha3: int, c: int, d: int):
    return (-1) ** (m - r - alpha3) * binom(m - r + c, alpha3) * binom(m - r + d, m - r - alpha3) / \
           binom(m, alpha3)


def gammarj(r: int, j: int, a: int, b: int):
    if j < 0:
        return 0
    elif r < j:
        return 0
    else:
        return (-1.0) ** (r - j) * binom(r + a, j) * binom(r + b, r - j) / binom(r, j)


def gamma_sum(r: int, malpha3: int, alpha2: int, a: int, b: int):
    total = 0
    for l in range(malpha3 - r + 1):
        # print(gammarj(r, alpha2 - l, a, b),r,alpha2-l)
        total += gammarj(r, alpha2 - l, a, b) * binom(malpha3 - r, l) * binom(r, alpha2 - l) / \
                 binom(malpha3, alpha2)
    return total


def a_coef(m: int, r: int, alpha: np.ndarray, a: int, b: int, c: int, d: int):
    if alpha[2] <= m - r:
        return numr(m, r, alpha[2], c, d) * gamma_sum(r, m - alpha[2], alpha[1], a, b)
    else:
        return 0


def a_coef_front(m: int, r: int, alpha: np.ndarray, a: int, b: int, c: int, d: int, k: int):
    if alpha[2] <= m - r:
        return numr_front(m, r, alpha[2], c, d, k) * gamma_sum(r, m - alpha[2] + k, alpha[1], a, b)
    else:
        return 0


def numr_front(m: int, r: int, alpha3: int, c: int, d: int, k: int):
    """
    Needs different coefficient for the front face
    """
    return (-1) ** (m - r - alpha3) * binom(m - r + c, alpha3) * binom(m - r + d, m - r - alpha3) / \
           binom(m + k, alpha3)


class VertexInverse(LinearOperator):
    """
    Object to work with the inverse on the edge.

    A possibility that I extend this even to apply to the vertices also
    """

    def __init__(self, fem: BernsteinFEM):
        self.p = fem.p
        self.elements = fem.elements
        self.num_edges = fem.num_edges
        self.num_faces = fem.num_faces
        self.num_vertices = len(fem.coordinates)
        self.lookup_multi = fem.lookup_multi
        self.lookup_dof = fem.lookup_dof
        self.eldof = fem.eldof

        # Find dofs
        self.face_dofs = int((self.p + 2) * (self.p + 1) / 2) - 3 * (self.p - 1) - 3
        self.edge_dofs = self.p - 1

        self.size_system = self.num_edges * self.edge_dofs + self.face_dofs * self.num_faces + self.num_vertices
        super(VertexInverse, self).__init__(dtype=np.double, shape=(self.size_system, self.size_system))

        # Now we create the matrix which will hold the change of basis matrix
        self.transformation2chi = np.zeros((self.num_vertices, self.size_system))
        self.diagonal = np.zeros((self.num_vertices, ))

        # First find the coefficients for transformation
        coefficients = np.zeros((self.edge_dofs + 2, self.edge_dofs + 2))

        # Adjustable parameters for vertex
        i = int(np.floor(self.p / 2))
        j = int(np.floor(i / 2))
        holder_bottom = np.zeros((self.edge_dofs + 2, self.edge_dofs + 2))
        holder_front = np.zeros((self.edge_dofs + 2, self.edge_dofs + 2))
        holder_side = np.zeros((self.edge_dofs + 2, self.edge_dofs + 2))

        r = i
        m = r + j
        for alpha3 in range(m + 1):
            for alpha2 in range(m + 1 - alpha3):
                alpha1 = m - alpha3 - alpha2
                # print(a_coef(m, r, np.array([alpha1, alpha2, alpha3]), 2, 2, 2 * r + 5, 1) * (alpha1 + 1) * (alpha2 + 1) / ((m + 1) * (m + 2)))
                holder_bottom[alpha1 + 1, alpha2] = a_coef(m, r, np.array([alpha1, alpha2, alpha3]), 2, 1,
                                                               2 * r + 3, 1) * (alpha1 + 1) / (m + 1)

        holder_bottom *= (-1) ** (m) / ((r + 1) * (m - r + 1))

        if m + 1 < self.p:
            for counter in range(self.p - m - 1):
                self.degree_raise_2d(holder_bottom, m + 1 + counter)
        # print(holder_bottom)

        r = i
        m = self.p - 1 - j
        k = self.p - 1 - m
        for alpha3 in range(m - r + 1):
            for alpha2 in range(m - alpha3 + k + 1):
                alpha1 = m - alpha3 - alpha2 + k
                holder_front[alpha1 + 1, self.p - alpha1 - 1 - alpha2] = a_coef_front(m, r, np.array([alpha1, alpha2, alpha3]), 2, 1,
                                                                        2 * r + 2 * self.p - 2 * m + 2, 1,
                                                                        k) * (alpha1 + 1) / (m + k + 1)

                # print(alpha1 + 1, alpha2, p - alpha1 - 1 - alpha2)
        holder_front *= (-1) ** (m) / ((m - r + 1) * (r + 1))

        # Need to "flip" this matrix
        # print(holder_front)
        r = j
        m = self.p - 1 - i
        for alpha3 in range(m - r + 1):
            for alpha2 in range(m - alpha3 + 1):
                alpha1 = m - alpha3 - alpha2

                c = factorial(self.p - 1 - i) / factorial(self.p)
                c *= factorial(alpha1 + i + 1) / factorial(alpha1)

                holder_side[alpha1 + i + 1, alpha2] = a_coef_front(m, r, np.array([alpha1, alpha2, alpha3]),
                                                               2 * i + 3, 1, 2 * i + 2 * j + 4, 1, 0) * c

        holder_side *= (-1) ** (m) / ((r + 1) * (m - r + 1))
        # print(holder_side)

        # Coefficient
        coefficients = holder_front / 3 + holder_bottom / 3 + holder_side / 3
        # print(coefficients)

        # Coefficients calculated; now we need to place them
        offset = len(fem.coordinates)
        diagonal_ref = find_diagonal_vertex(self.p)

        self.diagonal = np.zeros((self.num_vertices,))
        for element in range(fem.elements):
            # Find the volume scaling
            element_volume_scaling = volume.volume(fem.coordinates[fem.elnode[element, 0]],
                                                   fem.coordinates[fem.elnode[element, 1]],
                                                   fem.coordinates[fem.elnode[element, 2]],
                                                   fem.coordinates[fem.elnode[element, 3]])[0] / (4 / 3.0)
            # print("Volume scaling is: ", element_volume_scaling)

            # print(element, fem.eldof[element])
            # Place the vertex correctly
            # print(fem.lookup_dof[fem.eldof[element][0]], fem.lookup_dof[fem.eldof[element][1]])
            localdofs, localcoefs = self.vertex_face(fem.lookup_dof[0], coefficients)
            global_dofs = self.eldof[element][localdofs]
            self.transformation2chi[global_dofs[0], global_dofs] = localcoefs
            self.diagonal[global_dofs[0]] += element_volume_scaling * diagonal_ref

            localdofs, localcoefs = self.vertex_face(fem.lookup_dof[1], coefficients)
            global_dofs = self.eldof[element][localdofs]
            self.transformation2chi[global_dofs[0], global_dofs] = localcoefs
            self.diagonal[global_dofs[0]] += element_volume_scaling * diagonal_ref

            localdofs, localcoefs = self.vertex_face(fem.lookup_dof[2], coefficients)
            global_dofs = self.eldof[element][localdofs]
            self.transformation2chi[global_dofs[0], global_dofs] = localcoefs
            self.diagonal[global_dofs[0]] += element_volume_scaling * diagonal_ref

            localdofs, localcoefs = self.vertex_face(fem.lookup_dof[3], coefficients)
            global_dofs = self.eldof[element][localdofs]
            self.transformation2chi[global_dofs[0], global_dofs] = localcoefs
            self.diagonal[global_dofs[0]] += element_volume_scaling * diagonal_ref

        # Have to invert first
        self.diagonal = csr_matrix(np.diag(1/self.diagonal))
        self.transformation2chi = csr_matrix(self.transformation2chi)

        # self.total_transform = csr_matrix(self.transformation2chi.T @ np.diag(self.diagonal) @ self.transformation2chi)
        # from matplotlib import pyplot as plt
        # plt.spy(self.transformation2chi)
        # plt.show()

    @staticmethod
    def degree_raise_2d(cpoints: np.ndarray, m: int):
        """
        Degree raising algorithm; m is the
        :param cpoints:
        :param m: order to be raised to!
        :return:
        """
        for k in range(m + 1, -1, -1):
            for i in range(m + 1 - k, -1, -1):
                holder = 0
                if i > 0:
                    holder = holder + i * cpoints[i - 1, k] / (m + 1.0)
                if k > 0:
                    holder = holder + k * cpoints[i, k - 1] / (m + 1.0)
                if m + 1 - k - i > 0:
                    holder = holder + (m + 1 - k - i) * cpoints[i, k] / (m + 1.0)

                cpoints[i, k] = holder

    def _matvec(self, x: np.ndarray):
        # print(np.diag(self.diagonal).dot(self.transformation2chi))
        # print(self.transformation2chi.shape)
        # print(np.shape(self.transformation2chi.dot(x)))
        # print(self.total_transform.dot(x))
        return self.transformation2chi.T @ self.diagonal @ self.transformation2chi.dot(x)

    def vertex_face(self, mi_one, face_coef: np.ndarray):
        """

        :param mi_one: multiindex-one
        :param face_coef: the coefficient of the face
        :return:
        """
        verbose = False

        nonzero_one = np.nonzero(mi_one)[0][0]

        # Indices for the additional face
        new_face = [0, 1, 2, 3]

        if verbose:
            print(nonzero_one, mi_one)

        new_face.remove(nonzero_one)

        # Create arrays for output
        # TODO: make correct dimension (e.g. just a tet with one smaller)
        local_dofs = np.zeros((1 + 3 * (self.p - 1) + int((3 * (self.p - 2) * (self.p - 1))/2),), dtype=np.int)
        corrs_coef = np.zeros_like(local_dofs, dtype=np.float)
        counter = 0

        if verbose:
            print("Multiindices:", mi_one)
            print("Face coefficients are as follows:")
            print(face_coef)

        # First place the single vertex
        local_dofs[counter] = self.lookup_multi[mi_one]
        corrs_coef[counter] = 1
        counter += 1

        # Next, place the three edges
        holder = np.zeros_like(mi_one)
        for i in range(self.p - 1):
            holder[:] = 0
            holder[nonzero_one] = self.p - (i + 1)
            holder[new_face[0]] = i + 1

            local_dofs[counter] = self.lookup_multi[tuple(holder)]
            corrs_coef[counter] = face_coef[self.p - (i + 1), i + 1]
            counter += 1

            if verbose:
                print("Multiindex: ", holder, " corresponds to", self.lookup_multi[tuple(holder)], "with coefficient",
                      face_coef[p - (i + 1), i + 1])

            holder[:] = 0
            holder[nonzero_one] = self.p - (i + 1)
            holder[new_face[1]] = i + 1

            local_dofs[counter] = self.lookup_multi[tuple(holder)]
            corrs_coef[counter] = face_coef[self.p - (i + 1), i + 1]
            counter += 1

            if verbose:
                print("Multiindex: ", holder, " corresponds to", self.lookup_multi[tuple(holder)], "with coefficient",
                      face_coef[p - (i + 1), i + 1])

            holder[:] = 0
            holder[nonzero_one] = self.p - (i + 1)
            holder[new_face[2]] = i + 1

            local_dofs[counter] = self.lookup_multi[tuple(holder)]
            corrs_coef[counter] = face_coef[self.p - (i + 1), i + 1]
            counter += 1

            if verbose:
                print("Multiindex: ", holder, " corresponds to", self.lookup_multi[tuple(holder)], "with coefficient",
                      face_coef[p - (i + 1), i + 1])

        # Next, look at the three faces
        for i in range(self.p - 1):
            holder[:] = 0
            holder[new_face[0]] = i + 1

            # Now loop over the row
            for j in range(self.p - 1 - (i + 1)):
                holder[nonzero_one] = (self.p - (i + 1)) - (j + 1)
                holder[new_face[1]] = j + 1

                local_dofs[counter] = self.lookup_multi[tuple(holder)]
                corrs_coef[counter] = face_coef[self.p - (i + 1) - (j + 1), j + 1]
                counter += 1

                if verbose:
                    print("Multiindex: ", holder, " corresponds to", self.lookup_multi[tuple(holder)],
                          "with coefficient",
                          face_coef[p - (i + 1) - (j + 1), j + 1])

            # Now the second face
            holder[:] = 0
            holder[new_face[0]] = i + 1

            # Now loop over the row
            for j in range(self.p - 1 - (i + 1)):
                holder[nonzero_one] = (self.p - (i + 1)) - (j + 1)
                holder[new_face[2]] = j + 1

                local_dofs[counter] = self.lookup_multi[tuple(holder)]
                corrs_coef[counter] = face_coef[self.p - (i + 1) - (j + 1), j + 1]
                counter += 1

                if verbose:
                    print("Multiindex: ", holder, " corresponds to", self.lookup_multi[tuple(holder)],
                          "with coefficient",
                          face_coef[self.p - (i + 1) - (j + 1), j + 1])

            # Now the last face
            holder[:] = 0
            holder[new_face[1]] = i + 1

            # Now loop over the row
            for j in range(self.p - 1 - (i + 1)):
                holder[nonzero_one] = (self.p - (i + 1)) - (j + 1)
                holder[new_face[2]] = j + 1

                local_dofs[counter] = self.lookup_multi[tuple(holder)]
                corrs_coef[counter] = face_coef[self.p - (i + 1) - (j + 1), j + 1]
                counter += 1

                if verbose:
                    print("Multiindex: ", holder, " corresponds to", self.lookup_multi[tuple(holder)],
                          "with coefficient",
                          face_coef[self.p - (i + 1) - (j + 1), j + 1])

        return local_dofs, corrs_coef


class FaceInverse(LinearOperator):
    """
    Object to represent the inverse of internal face nodes for Schur complement
    """

    def __init__(self, fem: BernsteinFEM):
        self.p = fem.p
        self.elements = fem.elements
        self.num_faces = fem.num_faces

        # Find shapes
        self.face_dofs = int((self.p + 2) * (self.p + 1) / 2) - 3 * (self.p - 1) - 3

        super(FaceInverse, self).__init__(dtype=np.double, shape=(self.face_dofs * self.num_faces, self.face_dofs
                                                                  * self.num_faces))

        # Extractor matrix for Manuel's code (probably wrong?)
        grid = np.indices((self.p - 2, self.p - 2))
        self.indices = grid[0] + grid[1] <= (self.p - 3)
        self.C = np.zeros((self.p + 1,) * 2)

        self.scaling = np.zeros((fem.num_faces,))
        # For each element, we need to add each face with the correct scaling, which is related to the volume relative
        # to reference tetrahedron
        for i in range(self.elements):
            element_volume_scaling = volume.volume(fem.coordinates[fem.elnode[i, 0]],
                                                   fem.coordinates[fem.elnode[i, 1]],
                                                   fem.coordinates[fem.elnode[i, 2]],
                                                   fem.coordinates[fem.elnode[i, 3]])[0] / (1 / 3.0)

            self.scaling[fem.el_face[i, 0]] += element_volume_scaling
            self.scaling[fem.el_face[i, 1]] += element_volume_scaling
            self.scaling[fem.el_face[i, 2]] += element_volume_scaling
            self.scaling[fem.el_face[i, 3]] += element_volume_scaling

        # Additional stuff for Cython
        self.j2b = self._jacobi22bernsteincoefficients(self.p - 2)
        self.nu = self._nucoefficients(self.p - 2)
        self.q = np.zeros((self.p - 2, self.p - 2), dtype=np.double)
        self.output = np.zeros(int((self.p - 1) * (self.p - 2) / 2), dtype=np.double)

        # print(fem.el_face)

    def _matvec(self, x):
        out = np.zeros((self.face_dofs * self.num_faces,))

        face_interior.process(x, self.face_dofs, self.num_faces, self.p, self.j2b, self.nu, self.q, self.scaling,
                              out)
        return out

    @staticmethod
    def _jacobi22bernsteincoefficients(n):
        gamma = np.zeros((n + 1, n + 1), dtype=np.double)
        alpha = 2.0
        beta = 2.0
        gamma[0, 0] = 1.0
        for r in range(0, n + 1):
            for i in range(0, r + 1):
                gamma[r, i] = (-1) ** (r + i) * binom(r + alpha, alpha + i) * (r + 1) * (r + 2) / ((r + 2 - i) *
                                                                                                   (r + 1 - i))

        return gamma

    @staticmethod
    def _nucoefficients(n):
        def nu22(nu_k, nu_n, r):
            nuknr = ((nu_n - r + 1.0) * (nu_n - r + 2.0) / 2.0) * ((-1) ** nu_k) * (
                    binom(nu_n - r, nu_k) * binom(nu_n + r + 5.0, nu_k) * 2.0) / (
                            (nu_k + 1.0) * (nu_k + 2.0) * binom(nu_n, nu_k))
            return nuknr

        nus = np.zeros((n + 1, n + 1, n + 1), dtype=np.double)
        for j in range(n + 1):
            for i in range(j + 1):
                for k in range(n + 1):
                    nus[i, j, k] = nu22(i, j, k)
        return nus


def area(t0, t1, t2) -> float:
    """
        Calculates the area of a triangle usign Heron's formula

        We need this for the 3D code

        :type t0: np.ndarray
        :type t1: np.ndarray
        :type t2: np.ndarray
        :param t0: side 0 (numpy vector)
        :param t1: side 1 (numpy vector)
        :param t2: side 2 (numpy vector)
        :return: area of triangle
        """
    t0 = np.linalg.norm(t0)
    t1 = np.linalg.norm(t1)
    t2 = np.linalg.norm(t2)
    s = (t0 + t1 + t2) / 2
    return np.sqrt(s * (s - t0) * (s - t1) * (s - t2))

if __name__ == '__main__':
    """
    Data saved in ref-element-cond.data
    """
    np.set_printoptions(linewidth=260, precision=4)
    import scipy
    from scipy.sparse.linalg import LinearOperator
    for p in range(15, 17):
        # vertices, elements, boundary = read_msh.load_file('../cube.geo', refine=0)
        # print(len(elements))
        # mesh = BernsteinMesh(vertices, elements, {'faces': set(), 'lines': set()}, p)
        # mesh = BernsteinMesh(vertices, elements, boundary, p)
        # coords_threeelement = np.array([[0, 0, 0], [0, 0, 1], [0, 1, 0], [1, 0, 0], [1, 1, 1], [1, 0, 1]])
        # elnode_threeelement = np.array([[0, 1, 2, 3], [1, 2, 3, 4], [1, 3, 4, 5]])
        # faces = {(0, 1, 2), (0, 1, 3), (0, 2, 3), (1, 2, 4), (2, 3, 4), (1, 3, 5), (1, 4, 5), (3, 4, 5)}
        # lines = set()
        # for face in faces:
        #     lines.add(tuple(sorted((face[0], face[1]))))
        #     lines.add(tuple(sorted((face[0], face[2]))))
        #     lines.add(tuple(sorted((face[1], face[2]))))
        # bc_threeelement = {'faces': faces, 'lines': lines}
        # mesh = BernsteinMesh(coords_threeelement, elnode_threeelement, bc_threeelement, p)
        #
        # Single element
        # coords_oneelement = np.array([[-1, -1, -1], [-1, -1, 1], [-1, 1, -1], [1, -1, -1]])
        # elnode_oneelement = np.array([[0, 1, 2, 3]])
        # mesh = BernsteinMesh(coords_oneelement, elnode_oneelement, {'faces': set(), 'lines': set()}, p)

        # Two element
        # coords_oneelement = np.array([[-1, -1, -1], [-1, -1, 1], [-1, 1, -1], [1, -1, -1], [-1, -1, -3]])
        # elnode_oneelement = np.array([[0, 1, 2, 3], [0, 4, 2, 3]])
        # mesh = BernsteinMesh(coords_oneelement, elnode_oneelement, {'faces': set(), 'lines': set()}, p)

        # One cube with 6 elements
        import mesh_cube
        nodes = np.array([[-1, -1, -1],  # 0
                          [1, -1, -1],  # 1
                          [1, 1, -1],  # 2
                          [-1, 1, -1],  # 3
                          [-1, -1, 1],  # 4
                          [1, -1, 1],  # 5
                          [1, 1, 1],  # 6
                          [-1, 1, 1]])
        elnode = mesh_cube.mesh_cube(np.array([0, 1, 2, 3, 4, 5, 6, 7], dtype=np.int))
        mesh = BernsteinMesh(nodes, elnode, {'faces': set(), 'lines': set()}, p)

        fem = BernsteinFEM.BernsteinFEM(mesh)
        m, s, f = fem.assemble_matrix()
        # c = fem.assemble_convection()
        print("Matrix constructed", m.shape)

        Solver = NaivePreconditioner(m, fem)
        print("Preconditioner constructed")

        # print(Solver.solve(m @ np.ones(m.shape[0])))


        def mult_precond(v: np.ndarray) -> np.ndarray:
            return Solver.dot((m) @ v)
            # return Solver.dot((m + 0.01 * s) @ v) # THIS IS SO weirdddd
            # return Solver.dot((s) @ v)

        PinvS = LinearOperator(dtype=m.dtype, shape=m.shape, matvec=mult_precond)

        w = scipy.sparse.linalg.eigs(PinvS, which='LM', return_eigenvectors=False, k=1, tol=1E-8,
                                 maxiter=m.shape[0] * 50, ncv=m.shape[0] * 5)
        # w = scipy.sparse.linalg.eigs(m, which='LM', return_eigenvectors=False, k=1, tol=1E-8,
        #                              maxiter=m.shape[0] * 50, ncv=m.shape[0] * 5)

        large = max(np.real(w))

        w = scipy.sparse.linalg.eigs(PinvS, which='SM', return_eigenvectors=False, k=1, maxiter=m.shape[0] * 100,
                                 tol=1E-8, ncv=m.shape[0] * 30)
        # w = scipy.sparse.linalg.eigs(m, which='SM', return_eigenvectors=False, k=1, maxiter=m.shape[0] * 100,
        #                              tol=1E-9, ncv=m.shape[0] * 30)

        small = min(np.real(w))
        print("Order: ", p, " {:1.2f}".format((large / small)), large, small, mesh.elements, end='\n')

