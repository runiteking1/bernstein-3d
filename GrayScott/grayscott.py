import numpy as np
from BernsteinFEM import BernsteinFEM
from Mesh import bernstein_mesh
from mass_preconditioner import NaivePreconditioner
from pcg import pcg
import read_msh
from volume import volume
from CythonKernels.evaluate import evaluate as cython_evaluate
from moment import moment
import scipy.special
from scipy.sparse.linalg import LinearOperator

class GrayScott(BernsteinFEM.BernsteinFEM):
    def __init__(self, mymesh):
        super(GrayScott, self).__init__(mymesh)

    def nonlinear(self, u: np.ndarray, v: np.ndarray):
        output = np.zeros((self.eldof.max() + 1))

        vertices = np.zeros((3, 4))

        for element in range(self.elements):
            vertices[:, 0] = self.coordinates[self.elnode[element, 0]]
            vertices[:, 1] = self.coordinates[self.elnode[element, 1]]
            vertices[:, 2] = self.coordinates[self.elnode[element, 2]]
            vertices[:, 3] = self.coordinates[self.elnode[element, 3]]

            vol, _, _, _, _ = volume(vertices[:, 0], vertices[:, 1], vertices[:, 2], vertices[:, 3])

            c = self.element_to_control(u, element)
            d = self.element_to_control(v, element)

            U = cython_evaluate(c, self.q, self.p, self.xis)
            V = cython_evaluate(d, self.q, self.p, self.xis)

            F = U * (V ** 2)

            fk = 6 * vol * moment(F, self.q, self.p, self.xis, self.omegas)

            for i in range(self.dofs_per_element):
                output[self.eldof[element, i]] += fk[self.lookup_dof[i][0:3]]

        return output


def jump(x, a, r):
    return np.exp(1 / ((x - a) ** 2 - r ** 2) + 1 / r ** 2)


def init_u(x: np.ndarray) -> float:
    # if x[0] > 5.1:
    #     return .5
    # elif x[0] > 5:
    #     return .5*jump(x[0], 5, .1) + .5
    # else:
    #     return 1
    # if x[0] > 2.6:
    #     return .5
    # elif x[0] > 2.5:
    #     return .5*jump(x[0], 2.5, .1) + .5
    # else:
    #     return 1
    if x[0] > 0.85:
        return .5
    elif x[0] > 0.75:
        return .5 * jump(x[0], .75, .1) + .5
    else:
        return 1


def init_v(x: np.ndarray) -> float:
    # if x[0] > 5.1:
    #     return .25
    # elif x[0] > 5:
    #     return .25*jump(x[0], 5, .1)
    # else:
    #     return 0
    # if x[0] > 2.6:
    #     return .25
    # elif x[0] > 2.5:
    #     return .25*jump(x[0], 2.5, .1)
    # else:
    #     return 0
    #
    if x[0] > 0.85:
        return .25
    elif x[0] > 0.75:
        return .25 * jump(x[0], .75, .1)
    else:
        return 0


def wrapper(p: int, delta_t = 1):
    np.set_printoptions(linewidth=260, precision=4)

    # Parameters
    d_u = 2*10**(-5)
    # d_u = .125
    d_v = 10 ** (-5)
    # d_v = .05

    k = .05
    # k = .065
    F = .02
    # F = .045

    # Load a gmsh file
    vertices, elements, boundary = read_msh.load_file('../sphere_2.geo', refine=0)
    mesh = bernstein_mesh.BernsteinMesh(vertices, elements, {'faces': set(), 'lines': set()}, p)

    fem = GrayScott(mesh)
    m, s, loadone = fem.assemble_matrix()
    u_init = fem.l2_projection(init_u)
    v_init = fem.l2_projection(init_v)
    Fw = F * loadone

    print("Total elements:", fem.elements, "Size:", m.shape[0])
    solver = NaivePreconditioner(m, fem)

    u, it, resid = pcg(m, u_init, P=solver)
    v, it, resid = pcg(m, v_init)

    from matplotlib import pyplot as plt
    for count, residual in enumerate(resid):
        print(count, residual)
    plt.semilogy(np.arange(0, it + 1), resid)
    v, it, resid = pcg(m, v_init, P=solver)
    for count, residual in enumerate(resid):
        print(count, residual)

    plt.semilogy(np.arange(0, it + 1), resid)
    plt.show()


    # def mult_precond(v: np.ndarray) -> np.ndarray:
    #     return solver.dot((m * (1 + delta_t * (F)) + delta_t * d_u / 2 * s) @ v)
    #     # return solver.dot((m + .1 * s) @ v)
    #     # return (m + varepsilon_squared * s) @ v
    #     # return Solver.dot((s) @ v)
    #
    # PinvS = LinearOperator(dtype=m.dtype, shape=m.shape, matvec=mult_precond)
    #
    # w = scipy.sparse.linalg.eigs(PinvS, which='LM', return_eigenvectors=False, k=1, tol=1E-7,
    #                              maxiter=m.shape[0] * 50, ncv=m.shape[0] * 5)
    # # w = scipy.sparse.linalg.eigs(m, which='LM', return_eigenvectors=False, k=1, tol=1E-8,
    # #                              maxiter=m.shape[0] * 50, ncv=m.shape[0] * 5)
    #
    # large = max(np.real(w))
    #
    # w = scipy.sparse.linalg.eigs(PinvS, which='SM', return_eigenvectors=False, k=1, maxiter=m.shape[0] * 100,
    #                              tol=1E-7, ncv=m.shape[0] * 30)
    # # w = scipy.sparse.linalg.eigs(m, which='SM', return_eigenvectors=False, k=1, maxiter=m.shape[0] * 100,
    # #                              tol=1E-9, ncv=m.shape[0] * 30)
    #
    # small = min(np.real(w))
    # print("Order: ", p, " {:1.2f}".format((large / small)), large, small, mesh.elements, end='\n')

    import time

    start = time.time()
    for steps in range(int(np.rint(1))):
        print(steps, end="\t")
        # First do nonlinear
        uv2 = fem.nonlinear(u, v)
        # First solve u
        u, it, resid = pcg(m * (1 + delta_t * (F)) + delta_t * d_u / 2 * s,
                               m.dot(u) + delta_t * (Fw - uv2 - d_u / 2 * s.dot(u)), P=solver, x0=u)
        # u, it, resid = pcg(m * (1 + delta_t * (F)) + delta_t * d_u / 2 * s,
        #                        m.dot(u) + delta_t * (Fw - uv2 - d_u / 2 * s.dot(u)), x0=u)

        # all_its.append(it)
        print(it, end="\t")
        # Now solve v
        v, it, resid = pcg(m * (1 + delta_t * (F + k)) + delta_t * d_v / 2 * s,
                               m.dot(v) + delta_t * (uv2 - d_v / 2 * s.dot(u)), P=solver, x0=v)
        # v, it, resid = pcg(m * (1 + delta_t * (F + k)) + delta_t * d_v / 2 * s,
        #                        m.dot(v) + delta_t * (uv2 - d_v / 2 * s.dot(u)), x0=v)

        print(it)
    #
    #     name = "sphere_{}"
    #     if steps % 50 == 0:
    #         fem.plot_boundaries(u, name.format(steps)) #think this was degree 6
    #     print(steps)
    end = time.time()
    print(end - start)
    return(resid)


    # print("Matrix constructed", m.shape)


if __name__ == '__main__':
    print("Delta t of 1")
    wrapper(8, delta_t=1)
    # wrapper(5, delta_t= 30)
    # wrapper(6, delta_t= 30)
    # wrapper(7, delta_t= 30)
    # wrapper(8, delta_t= 30)
    # wrapper(9, delta_t= 30)
    # wrapper(10, delta_t= 30)
    # wrapper(11, delta_t=1000)

    # print("p = 11")
    # wrapper(11, delta_t=1)
    # wrapper(11, delta_t=10)
    # wrapper(11, delta_t=100)
    #
    # print("p = 12")
    # wrapper(12, delta_t=1)
    # wrapper(12, delta_t=10)
    # wrapper(12, delta_t=100)
