//+
Point(1) = {0, 0, 0, 1.0};
//+
Point(2) = {1, 0, 0, 1.0};
//+
Point(3) = {0, 1, 0, 1.0};
//+
Point(4) = {0, 0, 1, 1.0};
//+
Point(5) = {1, 1, 1, 1.0};
//+
Line(1) = {1, 3};
//+
Line(2) = {1, 4};
//+
Line(3) = {1, 2};
//+
Line(4) = {2, 4};
//+
Line(5) = {4, 3};
//+
Line(6) = {3, 2};
//+
Line(7) = {2, 5};
//+
Line(8) = {5, 4};
//+
Line(9) = {5, 3};
//+
Line Loop(1) = {5, -1, 2};
//+
Plane Surface(1) = {1};
//+
Line Loop(2) = {6, -3, 1};
//+
Plane Surface(2) = {2};
//+
Line Loop(3) = {4, -2, 3};
//+
Plane Surface(3) = {3};
//+
Line Loop(4) = {9, -5, -8};
//+
Plane Surface(4) = {4};
//+
Line Loop(5) = {7, 8, -4};
//+
Plane Surface(5) = {5};
//+
Line Loop(6) = {6, 7, 9};
//+
Plane Surface(6) = {6};
//+
Surface Loop(1) = {4, 6, 2, 3, 5, 1};
//+
Volume(1) = {1};
//+
Line Loop(7) = {6, 4, 5};
//+
Plane Surface(7) = {7};
//+
Surface Loop(2) = {7, 2, 3, 1};
//+
Volume(2) = {2};
//+
Surface Loop(3) = {4, 6, 5, 7};
//+
Volume(3) = {3};
