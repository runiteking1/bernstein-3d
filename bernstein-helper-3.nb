(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     32810,       1052]
NotebookOptionsPosition[     29952,        952]
NotebookOutlinePosition[     30289,        967]
CellTagsIndexPosition[     30246,        964]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"b11", "[", 
    RowBox[{"x_", ",", " ", "y_", ",", " ", "z_"}], "]"}], " ", ":=", " ", 
   "x"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"b21", "[", 
    RowBox[{"x_", ",", " ", "y_", ",", " ", "z_"}], "]"}], " ", ":=", " ", 
   "y"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"b31", "[", 
    RowBox[{"x_", ",", " ", "y_", ",", " ", "z_"}], "]"}], " ", ":=", " ", 
   "z"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"b41", "[", 
    RowBox[{"x_", ",", " ", "y_", ",", " ", "z_"}], "]"}], " ", ":=", " ", 
   RowBox[{"(", 
    RowBox[{"1", "-", "x", "-", "y", "-", "z"}], ")"}]}], ";"}]}], "Input",
 CellChangeTimes->{{3.746199483325985*^9, 3.746199523130557*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"b12", "[", 
    RowBox[{"x_", ",", " ", "y_", ",", " ", "z_"}], "]"}], " ", ":=", " ", 
   RowBox[{
    FractionBox["1", "2"], 
    RowBox[{"(", 
     RowBox[{"1", "+", "x", "-", "y", "-", "z"}], ")"}]}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"b22", "[", 
    RowBox[{"x_", ",", " ", "y_", ",", " ", "z_"}], "]"}], " ", ":=", " ", 
   RowBox[{
    FractionBox["1", "2"], " ", 
    RowBox[{"(", 
     RowBox[{"1", "-", "x", "+", "y", "-", "z"}], ")"}]}]}], ";"}]}], "Input",\

 CellChangeTimes->{{3.746199568082768*^9, 3.7461995802022038`*^9}, {
  3.746199636915501*^9, 3.746199695920746*^9}, {3.746199797411096*^9, 
  3.746199821257587*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"b32", "[", 
    RowBox[{"x_", ",", " ", "y_", ",", " ", "z_"}], "]"}], " ", ":=", " ", 
   RowBox[{
    FractionBox["1", "2"], " ", 
    RowBox[{"(", 
     RowBox[{"1", "-", "x", "-", "y", "+", "z"}], ")"}]}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"b42", "[", 
    RowBox[{"x_", ",", " ", "y_", ",", " ", "z_"}], "]"}], " ", ":=", " ", 
   RowBox[{
    FractionBox["1", "2"], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"-", "1"}], "+", "x", "+", "y", "+", "z"}], ")"}]}]}], 
  ";"}]}], "Input",
 CellChangeTimes->{{3.746199670757279*^9, 3.74619972105824*^9}, {
  3.746199809195805*^9, 3.746199817648478*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"u", "[", 
   RowBox[{"x_", ",", " ", "y_", ",", " ", "z_"}], "]"}], " ", ":=", " ", 
  RowBox[{
   RowBox[{"b11", "[", 
    RowBox[{"x", ",", "y", ",", "z"}], "]"}], 
   RowBox[{"b21", "[", 
    RowBox[{"x", ",", "y", ",", "z"}], "]"}], 
   RowBox[{"b31", "[", 
    RowBox[{"x", ",", "y", ",", "z"}], "]"}], 
   RowBox[{"b12", "[", 
    RowBox[{"x", ",", " ", "y", ",", " ", "z"}], "]"}], " ", 
   RowBox[{"b22", "[", 
    RowBox[{"x", ",", " ", "y", ",", " ", "z"}], "]"}], " ", 
   RowBox[{"b32", "[", 
    RowBox[{"x", ",", " ", "y", ",", " ", "z"}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.746199885661924*^9, 3.746199935249607*^9}, {
  3.746207236997212*^9, 3.7462072630109873`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"u", "[", 
  RowBox[{"x", ",", "y", ",", "z"}], "]"}]], "Input",
 CellChangeTimes->{{3.746284643991158*^9, 3.746284645146737*^9}}],

Cell[BoxData[
 RowBox[{
  FractionBox["1", "8"], " ", "x", " ", "y", " ", 
  RowBox[{"(", 
   RowBox[{"1", "+", "x", "-", "y", "-", "z"}], ")"}], " ", 
  RowBox[{"(", 
   RowBox[{"1", "-", "x", "+", "y", "-", "z"}], ")"}], " ", "z", " ", 
  RowBox[{"(", 
   RowBox[{"1", "-", "x", "-", "y", "+", "z"}], ")"}]}]], "Output",
 CellChangeTimes->{3.7462846458292313`*^9, 3.746352473856515*^9, 
  3.746354884979578*^9, 3.746354996005308*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"u", "[", 
  RowBox[{".25", ",", " ", ".25", ",", " ", ".25"}], "]"}]], "Input",
 CellChangeTimes->{{3.746355114439309*^9, 3.746355118015613*^9}}],

Cell[BoxData["0.000823974609375`"], "Output",
 CellChangeTimes->{3.7463551182940617`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Expand", "[", 
  RowBox[{"u", "[", 
   RowBox[{"x", ",", "y", ",", "z"}], "]"}], "]"}]], "Input",
 CellChangeTimes->{{3.746200012489354*^9, 3.74620001527707*^9}, {
  3.7462077220886173`*^9, 3.7462077242382927`*^9}}],

Cell[BoxData[
 RowBox[{
  FractionBox[
   RowBox[{"x", " ", "y", " ", "z"}], "8"], "-", 
  RowBox[{
   FractionBox["1", "8"], " ", 
   SuperscriptBox["x", "2"], " ", "y", " ", "z"}], "-", 
  RowBox[{
   FractionBox["1", "8"], " ", 
   SuperscriptBox["x", "3"], " ", "y", " ", "z"}], "+", 
  RowBox[{
   FractionBox["1", "8"], " ", 
   SuperscriptBox["x", "4"], " ", "y", " ", "z"}], "-", 
  RowBox[{
   FractionBox["1", "8"], " ", "x", " ", 
   SuperscriptBox["y", "2"], " ", "z"}], "+", 
  RowBox[{
   FractionBox["1", "4"], " ", 
   SuperscriptBox["x", "2"], " ", 
   SuperscriptBox["y", "2"], " ", "z"}], "-", 
  RowBox[{
   FractionBox["1", "8"], " ", 
   SuperscriptBox["x", "3"], " ", 
   SuperscriptBox["y", "2"], " ", "z"}], "-", 
  RowBox[{
   FractionBox["1", "8"], " ", "x", " ", 
   SuperscriptBox["y", "3"], " ", "z"}], "-", 
  RowBox[{
   FractionBox["1", "8"], " ", 
   SuperscriptBox["x", "2"], " ", 
   SuperscriptBox["y", "3"], " ", "z"}], "+", 
  RowBox[{
   FractionBox["1", "8"], " ", "x", " ", 
   SuperscriptBox["y", "4"], " ", "z"}], "-", 
  RowBox[{
   FractionBox["1", "8"], " ", "x", " ", "y", " ", 
   SuperscriptBox["z", "2"]}], "+", 
  RowBox[{
   FractionBox["1", "4"], " ", 
   SuperscriptBox["x", "2"], " ", "y", " ", 
   SuperscriptBox["z", "2"]}], "-", 
  RowBox[{
   FractionBox["1", "8"], " ", 
   SuperscriptBox["x", "3"], " ", "y", " ", 
   SuperscriptBox["z", "2"]}], "+", 
  RowBox[{
   FractionBox["1", "4"], " ", "x", " ", 
   SuperscriptBox["y", "2"], " ", 
   SuperscriptBox["z", "2"]}], "+", 
  RowBox[{
   FractionBox["1", "4"], " ", 
   SuperscriptBox["x", "2"], " ", 
   SuperscriptBox["y", "2"], " ", 
   SuperscriptBox["z", "2"]}], "-", 
  RowBox[{
   FractionBox["1", "8"], " ", "x", " ", 
   SuperscriptBox["y", "3"], " ", 
   SuperscriptBox["z", "2"]}], "-", 
  RowBox[{
   FractionBox["1", "8"], " ", "x", " ", "y", " ", 
   SuperscriptBox["z", "3"]}], "-", 
  RowBox[{
   FractionBox["1", "8"], " ", 
   SuperscriptBox["x", "2"], " ", "y", " ", 
   SuperscriptBox["z", "3"]}], "-", 
  RowBox[{
   FractionBox["1", "8"], " ", "x", " ", 
   SuperscriptBox["y", "2"], " ", 
   SuperscriptBox["z", "3"]}], "+", 
  RowBox[{
   FractionBox["1", "8"], " ", "x", " ", "y", " ", 
   SuperscriptBox["z", "4"]}]}]], "Output",
 CellChangeTimes->{3.746200015581238*^9, 3.7462071449910593`*^9, 
  3.746207268439125*^9, 3.746207724500959*^9, 3.746276958853643*^9, 
  3.7463524739528112`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"FullSimplify", "[", 
  RowBox[{"Laplacian", "[", 
   RowBox[{
    RowBox[{"u", "[", 
     RowBox[{"x", ",", "y", ",", "z"}], "]"}], ",", " ", 
    RowBox[{"{", 
     RowBox[{"x", ",", " ", "y", ",", " ", "z"}], "}"}]}], "]"}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.746199936778077*^9, 3.74619996280914*^9}}],

Cell[BoxData[
 RowBox[{
  FractionBox["1", "4"], " ", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{
     RowBox[{"-", 
      SuperscriptBox["x", "3"]}], " ", 
     RowBox[{"(", 
      RowBox[{"y", "+", "z"}], ")"}]}], "-", 
    RowBox[{"x", " ", 
     RowBox[{"(", 
      RowBox[{"y", "-", 
       RowBox[{"2", " ", 
        SuperscriptBox["y", "2"]}], "+", 
       SuperscriptBox["y", "3"], "+", 
       RowBox[{"9", " ", "y", " ", "z"}], "+", 
       RowBox[{
        SuperscriptBox[
         RowBox[{"(", 
          RowBox[{
           RowBox[{"-", "1"}], "+", "z"}], ")"}], "2"], " ", "z"}]}], ")"}]}],
     "+", 
    RowBox[{"2", " ", 
     SuperscriptBox["x", "2"], " ", 
     RowBox[{"(", 
      RowBox[{"y", "+", 
       SuperscriptBox["y", "2"], "+", "z", "+", 
       SuperscriptBox["z", "2"]}], ")"}]}], "-", 
    RowBox[{"y", " ", "z", " ", 
     RowBox[{"(", 
      RowBox[{
       SuperscriptBox["y", "2"], "+", 
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{
          RowBox[{"-", "1"}], "+", "z"}], ")"}], "2"], "-", 
       RowBox[{"2", " ", "y", " ", 
        RowBox[{"(", 
         RowBox[{"1", "+", "z"}], ")"}]}]}], ")"}]}]}], ")"}]}]], "Output",
 CellChangeTimes->{{3.746207142091167*^9, 3.7462071451384077`*^9}, 
   3.7462072709774218`*^9, 3.74635247419053*^9, 3.746355071997981*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{"f", "[", 
   RowBox[{"x_", ",", " ", "y_", ",", " ", "z_"}], "]"}], ":=", " ", 
  RowBox[{
   FractionBox["1", "4"], " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{
      RowBox[{"-", 
       SuperscriptBox["x", "3"]}], " ", 
      RowBox[{"(", 
       RowBox[{"y", "+", "z"}], ")"}]}], "-", 
     RowBox[{"x", " ", 
      RowBox[{"(", 
       RowBox[{"y", "-", 
        RowBox[{"2", " ", 
         SuperscriptBox["y", "2"]}], "+", 
        SuperscriptBox["y", "3"], "+", 
        RowBox[{"9", " ", "y", " ", "z"}], "+", 
        RowBox[{
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{
            RowBox[{"-", "1"}], "+", "z"}], ")"}], "2"], " ", "z"}]}], 
       ")"}]}], "+", 
     RowBox[{"2", " ", 
      SuperscriptBox["x", "2"], " ", 
      RowBox[{"(", 
       RowBox[{"y", "+", 
        SuperscriptBox["y", "2"], "+", "z", "+", 
        SuperscriptBox["z", "2"]}], ")"}]}], "-", 
     RowBox[{"y", " ", "z", " ", 
      RowBox[{"(", 
       RowBox[{
        SuperscriptBox["y", "2"], "+", 
        SuperscriptBox[
         RowBox[{"(", 
          RowBox[{
           RowBox[{"-", "1"}], "+", "z"}], ")"}], "2"], "-", 
        RowBox[{"2", " ", "y", " ", 
         RowBox[{"(", 
          RowBox[{"1", "+", "z"}], ")"}]}]}], ")"}]}]}], ")"}]}]}]], "Input",
 CellChangeTimes->{{3.7462000584201202`*^9, 3.746200066050599*^9}, 
   3.746207278424735*^9}],

Cell[BoxData[
 RowBox[{
  RowBox[{"bernstein1", "[", 
   RowBox[{"x_", ",", " ", "y_", ",", " ", "z_"}], "]"}], " ", ":=", " ", 
  RowBox[{"12", 
   RowBox[{
    RowBox[{"b11", "[", 
     RowBox[{"x", ",", " ", "y", ",", " ", "z"}], "]"}], "^", "2"}], 
   RowBox[{"b21", "[", 
    RowBox[{"x", ",", " ", "y", ",", " ", "z"}], "]"}], 
   RowBox[{"b31", "[", 
    RowBox[{"x", ",", " ", "y", ",", " ", "z"}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.746202508274185*^9, 3.7462025370407763`*^9}, {
   3.746202699080057*^9, 3.7462027290427027`*^9}, 3.746202793359346*^9, {
   3.746208322134407*^9, 3.74620834264937*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"bernstein2", "[", 
   RowBox[{"x_", ",", " ", "y_", ",", " ", "z_"}], "]"}], " ", ":=", "  ", 
  RowBox[{
   RowBox[{"b12", "[", 
    RowBox[{"x", ",", " ", "y", ",", " ", "z"}], "]"}], " ", 
   RowBox[{"b22", "[", 
    RowBox[{"x", ",", " ", "y", ",", " ", "z"}], "]"}], " ", 
   RowBox[{"b32", "[", 
    RowBox[{"x", ",", " ", "y", ",", " ", "z"}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.74620419632096*^9, 3.7462042343005943`*^9}, 
   3.7462776422354813`*^9}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"NIntegrate", "[", 
  RowBox[{
   RowBox[{
    RowBox[{"bernstein1", "[", 
     RowBox[{"x", ",", "y", ",", "z"}], "]"}], 
    RowBox[{"f", "[", 
     RowBox[{"x", ",", " ", "y", ",", " ", "z"}], "]"}]}], ",", "  ", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"x", ",", "y", ",", "z"}], "}"}], " ", "\[Element]", 
    RowBox[{"Tetrahedron", "[", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"1", ",", " ", "0", ",", "0"}], "}"}], ",", " ", 
       RowBox[{"{", 
        RowBox[{"0", ",", " ", "1", ",", " ", "0"}], "}"}], " ", ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", " ", "0", ",", " ", "1"}], "}"}], ",", " ", 
       RowBox[{"{", 
        RowBox[{"0", ",", "0", ",", "0"}], "}"}]}], "}"}], "]"}]}]}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.746207420954064*^9, 3.746207424441461*^9}, {
  3.746207464924481*^9, 3.7462074694668407`*^9}, {3.74620834756888*^9, 
  3.746208352280923*^9}}],

Cell[BoxData[
 RowBox[{"-", "0.00010401635401635402`"}]], "Output",
 CellChangeTimes->{
  3.746207426742296*^9, 3.7462074791555367`*^9, 3.7462083528523703`*^9, 
   3.746208420052775*^9, 3.7462771341713247`*^9, {3.746277191031736*^9, 
   3.74627720841302*^9}, 3.7463524744289303`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"NIntegrate", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"bernstein2", "[", 
      RowBox[{"x", ",", "y", ",", "z"}], "]"}], 
     RowBox[{"f", "[", 
      RowBox[{"x", ",", " ", "y", ",", " ", "z"}], "]"}]}], ",", "  ", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"x", ",", "y", ",", "z"}], "}"}], " ", "\[Element]", 
     RowBox[{"Tetrahedron", "[", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{"1", ",", " ", "0", ",", "0"}], "}"}], ",", " ", 
        RowBox[{"{", 
         RowBox[{"0", ",", " ", "1", ",", " ", "0"}], "}"}], " ", ",", 
        RowBox[{"{", 
         RowBox[{"0", ",", " ", "0", ",", " ", "1"}], "}"}], ",", " ", 
        RowBox[{"{", 
         RowBox[{"1", ",", "1", ",", "1"}], "}"}]}], "}"}], "]"}]}]}], "]"}], 
  " ", "+", " ", 
  RowBox[{"NIntegrate", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"bernstein1", "[", 
      RowBox[{"x", ",", "y", ",", "z"}], "]"}], 
     RowBox[{"f", "[", 
      RowBox[{"x", ",", " ", "y", ",", " ", "z"}], "]"}]}], ",", "  ", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"x", ",", "y", ",", "z"}], "}"}], " ", "\[Element]", 
     RowBox[{"Tetrahedron", "[", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{"1", ",", " ", "0", ",", "0"}], "}"}], ",", " ", 
        RowBox[{"{", 
         RowBox[{"0", ",", " ", "1", ",", " ", "0"}], "}"}], " ", ",", 
        RowBox[{"{", 
         RowBox[{"0", ",", " ", "0", ",", " ", "1"}], "}"}], ",", " ", 
        RowBox[{"{", 
         RowBox[{"0", ",", "0", ",", "0"}], "}"}]}], "}"}], "]"}]}]}], 
   "]"}]}]], "Input",
 CellChangeTimes->{{3.746207488202045*^9, 3.746207498064513*^9}}],

Cell[BoxData[
 RowBox[{"-", "0.00024290524290524292`"}]], "Output",
 CellChangeTimes->{3.746207498574736*^9, 3.7463524745431547`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"NIntegrate", "[", 
  RowBox[{
   RowBox[{"6", 
    RowBox[{"b11", "[", 
     RowBox[{"x", ",", " ", "y", ",", " ", "z"}], "]"}], 
    RowBox[{"b21", "[", 
     RowBox[{"x", ",", " ", "y", ",", " ", "z"}], "]"}], 
    RowBox[{"b41", "[", 
     RowBox[{"x", ",", " ", "y", ",", " ", "z"}], "]"}], 
    RowBox[{"f", "[", 
     RowBox[{"x", ",", " ", "y", ",", " ", "z"}], "]"}]}], ",", "  ", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"x", ",", "y", ",", "z"}], "}"}], " ", "\[Element]", 
    RowBox[{"Tetrahedron", "[", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"1", ",", " ", "0", ",", "0"}], "}"}], ",", " ", 
       RowBox[{"{", 
        RowBox[{"0", ",", " ", "1", ",", " ", "0"}], "}"}], " ", ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", " ", "0", ",", " ", "1"}], "}"}], ",", " ", 
       RowBox[{"{", 
        RowBox[{"0", ",", "0", ",", "0"}], "}"}]}], "}"}], "]"}]}]}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.746202730524185*^9, 3.7462027338976707`*^9}, {
  3.7462028008073387`*^9, 3.7462028296889153`*^9}, {3.7462038682331448`*^9, 
  3.7462038993679123`*^9}, {3.746203976302899*^9, 3.746203976461631*^9}}],

Cell[BoxData[
 RowBox[{"-", "0.00010251322751322751`"}]], "Output",
 CellChangeTimes->{
  3.7462027344180937`*^9, 3.746202795914765*^9, 3.746202830368927*^9, {
   3.74620387188198*^9, 3.74620387613323*^9}, 3.746203976995339*^9, 
   3.7462071456546373`*^9, 3.74635247471074*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Dot", "[", 
  RowBox[{
   RowBox[{"Grad", "[", 
    RowBox[{
     RowBox[{"bernstein1", "[", 
      RowBox[{"x", ",", " ", "y", ",", " ", "z"}], "]"}], ",", " ", 
     RowBox[{"{", 
      RowBox[{"x", ",", " ", "y", ",", " ", "z"}], "}"}]}], "]"}], " ", ",", 
   " ", 
   RowBox[{"Grad", "[", 
    RowBox[{
     RowBox[{"bernstein1", "[", 
      RowBox[{"x", ",", " ", "y", ",", " ", "z"}], "]"}], ",", " ", 
     RowBox[{"{", 
      RowBox[{"x", ",", " ", "y", ",", " ", "z"}], "}"}]}], "]"}]}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.7462050307948847`*^9, 3.7462050804646273`*^9}, {
  3.7462774706351137`*^9, 3.746277474676105*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"144", " ", 
   SuperscriptBox["x", "4"], " ", 
   SuperscriptBox["y", "2"]}], "+", 
  RowBox[{"144", " ", 
   SuperscriptBox["x", "4"], " ", 
   SuperscriptBox["z", "2"]}], "+", 
  RowBox[{"576", " ", 
   SuperscriptBox["x", "2"], " ", 
   SuperscriptBox["y", "2"], " ", 
   SuperscriptBox["z", "2"]}]}]], "Output",
 CellChangeTimes->{3.746352474797348*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"NIntegrate", "[", 
  RowBox[{
   RowBox[{
    RowBox[{"144", " ", 
     SuperscriptBox["x", "4"], " ", 
     SuperscriptBox["y", "2"]}], "+", 
    RowBox[{"144", " ", 
     SuperscriptBox["x", "4"], " ", 
     SuperscriptBox["z", "2"]}], "+", 
    RowBox[{"576", " ", 
     SuperscriptBox["x", "2"], " ", 
     SuperscriptBox["y", "2"], " ", 
     SuperscriptBox["z", "2"]}]}], ",", "   ", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"x", ",", "y", ",", "z"}], "}"}], " ", "\[Element]", 
    RowBox[{"Tetrahedron", "[", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"1", ",", " ", "0", ",", "0"}], "}"}], ",", " ", 
       RowBox[{"{", 
        RowBox[{"0", ",", " ", "1", ",", " ", "0"}], "}"}], " ", ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", " ", "0", ",", " ", "1"}], "}"}], ",", " ", 
       RowBox[{"{", 
        RowBox[{"0", ",", "0", ",", "0"}], "}"}]}], "}"}], "]"}]}]}], "]"}]], \
"Input",
 CellChangeTimes->{{3.746277480771747*^9, 3.746277499568499*^9}}],

Cell[BoxData["0.050793650793650794`"], "Output",
 CellChangeTimes->{3.746277500329803*^9, 3.7463524748930073`*^9}]
}, Open  ]],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.746352536679401*^9, 3.746352539354685*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Dot", "[", 
  RowBox[{
   RowBox[{"Grad", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"b42", "[", 
       RowBox[{"x", ",", " ", "y", ",", " ", "z"}], "]"}], "^", "2"}], ",", 
     " ", 
     RowBox[{"{", 
      RowBox[{"x", ",", " ", "y", ",", " ", "z"}], "}"}]}], "]"}], " ", ",", 
   RowBox[{"Dot", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{
         RowBox[{"x", "^", "2"}], ",", " ", "0", ",", " ", "0"}], "}"}], ",", 
       " ", 
       RowBox[{"{", 
        RowBox[{"0", ",", "y", ",", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", "0", ",", "z"}], "}"}]}], "}"}], ",", " ", 
     RowBox[{"Grad", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"b12", "[", 
         RowBox[{"x", ",", " ", "y", ",", " ", "z"}], "]"}], "^", "2"}], ",", 
       " ", 
       RowBox[{"{", 
        RowBox[{"x", ",", " ", "y", ",", " ", "z"}], "}"}]}], "]"}]}], 
    "]"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.746352559538183*^9, 3.7463525684748983`*^9}, {
  3.746352863154333*^9, 3.7463529142122993`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{
   FractionBox["1", "4"], " ", 
   SuperscriptBox["x", "2"], " ", 
   RowBox[{"(", 
    RowBox[{"1", "+", "x", "-", "y", "-", "z"}], ")"}], " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"-", "1"}], "+", "x", "+", "y", "+", "z"}], ")"}]}], "+", 
  RowBox[{
   FractionBox["1", "4"], " ", "y", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"-", "1"}], "-", "x", "+", "y", "+", "z"}], ")"}], " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"-", "1"}], "+", "x", "+", "y", "+", "z"}], ")"}]}], "+", 
  RowBox[{
   FractionBox["1", "4"], " ", "z", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"-", "1"}], "-", "x", "+", "y", "+", "z"}], ")"}], " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"-", "1"}], "+", "x", "+", "y", "+", "z"}], ")"}]}]}]], "Output",\

 CellChangeTimes->{
  3.746352595180044*^9, {3.7463528679858923`*^9, 3.746352917244864*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"NIntegrate", "[", 
  RowBox[{
   RowBox[{
    RowBox[{
     FractionBox["1", "4"], " ", 
     SuperscriptBox["x", "2"], " ", 
     RowBox[{"(", 
      RowBox[{"1", "+", "x", "-", "y", "-", "z"}], ")"}], " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", "1"}], "+", "x", "+", "y", "+", "z"}], ")"}]}], "+", 
    RowBox[{
     FractionBox["1", "4"], " ", "y", " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", "1"}], "-", "x", "+", "y", "+", "z"}], ")"}], " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", "1"}], "+", "x", "+", "y", "+", "z"}], ")"}]}], "+", 
    RowBox[{
     FractionBox["1", "4"], " ", "z", " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", "1"}], "-", "x", "+", "y", "+", "z"}], ")"}], " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", "1"}], "+", "x", "+", "y", "+", "z"}], ")"}]}]}], ",", 
   "  ", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"x", ",", "y", ",", "z"}], "}"}], " ", "\[Element]", 
    RowBox[{"Tetrahedron", "[", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"1", ",", " ", "0", ",", "0"}], "}"}], ",", " ", 
       RowBox[{"{", 
        RowBox[{"0", ",", " ", "1", ",", " ", "0"}], "}"}], " ", ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", " ", "0", ",", " ", "1"}], "}"}], ",", " ", 
       RowBox[{"{", 
        RowBox[{"1", ",", "1", ",", "1"}], "}"}]}], "}"}], "]"}]}]}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.746352597090507*^9, 3.7463526134601307`*^9}, {
  3.7463528951148863`*^9, 3.7463529236678658`*^9}}],

Cell[BoxData[
 RowBox[{"-", "0.00873015873015873`"}]], "Output",
 CellChangeTimes->{
  3.746352613949704*^9, {3.746352895738183*^9, 3.7463529242432137`*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"u", "[", 
  RowBox[{
   RowBox[{"2", "/", "4"}], ",", 
   RowBox[{"2.5", "/", "4"}], ",", 
   RowBox[{"2.5", "/", "4"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.746353476863179*^9, 3.746353515404572*^9}}],

Cell[BoxData["0.01373291015625`"], "Output",
 CellChangeTimes->{{3.746353488308941*^9, 3.7463535158739843`*^9}}]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{"u", "[", 
   RowBox[{"x_", ",", " ", "y_", ",", " ", "z_"}], "]"}], " ", ":=", " ", 
  RowBox[{
   RowBox[{"b11", "[", 
    RowBox[{"x", ",", "y", ",", " ", "z"}], "]"}], 
   RowBox[{"b21", "[", 
    RowBox[{"x", ",", "y", ",", "z"}], "]"}], 
   RowBox[{"b31", "[", 
    RowBox[{"x", ",", "y", ",", "z"}], "]"}], 
   RowBox[{"b41", "[", 
    RowBox[{"x", ",", "y", ",", "z"}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.746354244949687*^9, 3.746354248429521*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"u", "[", 
  RowBox[{"x", ",", " ", "y", ",", " ", "z"}], "]"}]], "Input",
 CellChangeTimes->{{3.7463542499906387`*^9, 3.74635425115166*^9}}],

Cell[BoxData[
 RowBox[{"x", " ", "y", " ", 
  RowBox[{"(", 
   RowBox[{"1", "-", "x", "-", "y", "-", "z"}], ")"}], " ", "z"}]], "Output",
 CellChangeTimes->{3.746354251583268*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Expand", "[", 
  RowBox[{"x", " ", "y", " ", 
   RowBox[{"(", 
    RowBox[{"1", "-", "x", "-", "y", "-", "z"}], ")"}], " ", "z"}], 
  "]"}]], "Input",
 NumberMarks->False],

Cell[BoxData[
 RowBox[{
  RowBox[{"x", " ", "y", " ", "z"}], "-", 
  RowBox[{
   SuperscriptBox["x", "2"], " ", "y", " ", "z"}], "-", 
  RowBox[{"x", " ", 
   SuperscriptBox["y", "2"], " ", "z"}], "-", 
  RowBox[{"x", " ", "y", " ", 
   SuperscriptBox["z", "2"]}]}]], "Output",
 CellChangeTimes->{3.746354257914152*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{"Laplacian", "[", 
  RowBox[{
   RowBox[{
    RowBox[{"b11", "[", 
     RowBox[{"x", ",", "y", ",", " ", "z"}], "]"}], 
    RowBox[{"b21", "[", 
     RowBox[{"x", ",", "y", ",", "z"}], "]"}], 
    RowBox[{"b31", "[", 
     RowBox[{"x", ",", "y", ",", "z"}], "]"}], 
    RowBox[{"b41", "[", 
     RowBox[{"x", ",", "y", ",", "z"}], "]"}]}], ",", " ", 
   RowBox[{"{", 
    RowBox[{"x", ",", " ", "y", ",", " ", "z"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.746354128099709*^9, 3.746354156690267*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Simplify", "[", 
  RowBox[{
   RowBox[{
    RowBox[{"-", "2"}], " ", "x", " ", "y"}], "-", 
   RowBox[{"2", " ", "x", " ", "z"}], "-", 
   RowBox[{"2", " ", "y", " ", "z"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.746354158968383*^9, 3.746354161278295*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"-", "2"}], " ", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"y", " ", "z"}], "+", 
    RowBox[{"x", " ", 
     RowBox[{"(", 
      RowBox[{"y", "+", "z"}], ")"}]}]}], ")"}]}]], "Output",
 CellChangeTimes->{3.74635416155444*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{"basis", "[", 
   RowBox[{"x_", ",", " ", "y_", ",", " ", "z_"}], "]"}], " ", ":=", " ", 
  RowBox[{"24", 
   RowBox[{"b11", "[", 
    RowBox[{"x", ",", "y", ",", " ", "z"}], "]"}], 
   RowBox[{"b21", "[", 
    RowBox[{"x", ",", "y", ",", "z"}], "]"}], 
   RowBox[{"b31", "[", 
    RowBox[{"x", ",", "y", ",", "z"}], "]"}], 
   RowBox[{"b41", "[", 
    RowBox[{"x", ",", "y", ",", "z"}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.746354353722204*^9, 3.746354378623274*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"NIntegrate", "[", 
  RowBox[{
   RowBox[{
    RowBox[{"basis", "[", 
     RowBox[{"x", ",", "y", ",", "z"}], "]"}], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{
       RowBox[{"-", "2"}], " ", "x", " ", "y"}], "-", 
      RowBox[{"2", " ", "x", " ", "z"}], "-", 
      RowBox[{"2", " ", "y", " ", "z"}]}], ")"}]}], ",", " ", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"x", ",", "y", ",", "z"}], "}"}], " ", "\[Element]", 
    RowBox[{"Tetrahedron", "[", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"1", ",", " ", "0", ",", "0"}], "}"}], ",", " ", 
       RowBox[{"{", 
        RowBox[{"0", ",", " ", "1", ",", " ", "0"}], "}"}], " ", ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", " ", "0", ",", " ", "1"}], "}"}], ",", " ", 
       RowBox[{"{", 
        RowBox[{"0", ",", "0", ",", "0"}], "}"}]}], "}"}], "]"}]}]}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.746354313239141*^9, 3.746354339031829*^9}, {
  3.746354369434269*^9, 3.746354384899166*^9}}],

Cell[BoxData[
 RowBox[{"-", "0.0015873015873015873`"}]], "Output",
 CellChangeTimes->{3.746354341354127*^9, 3.746354385548594*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{"Dot", "[", 
  RowBox[{
   RowBox[{"Grad", "[", 
    RowBox[{
     RowBox[{"basis", "[", 
      RowBox[{"x", ",", "y", ",", "z"}], "]"}], ",", " ", 
     RowBox[{"{", 
      RowBox[{"x", ",", " ", "y", ",", " ", "z"}], "}"}]}], "]"}], ",", " ", 
   RowBox[{"Grad", "[", 
    RowBox[{
     RowBox[{"basis", "[", 
      RowBox[{"x", ",", "y", ",", "z"}], "]"}], ",", " ", 
     RowBox[{"{", 
      RowBox[{"x", ",", " ", "y", ",", " ", "z"}], "}"}]}], "]"}]}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.746354651150185*^9, 3.7463546693314466`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"NIntegrate", "[", 
  RowBox[{
   RowBox[{
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{
       RowBox[{"24", " ", "x", " ", "y", " ", 
        RowBox[{"(", 
         RowBox[{"1", "-", "x", "-", "y", "-", "z"}], ")"}]}], "-", 
       RowBox[{"24", " ", "x", " ", "y", " ", "z"}]}], ")"}], "2"], "+", 
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{
       RowBox[{
        RowBox[{"-", "24"}], " ", "x", " ", "y", " ", "z"}], "+", 
       RowBox[{"24", " ", "x", " ", 
        RowBox[{"(", 
         RowBox[{"1", "-", "x", "-", "y", "-", "z"}], ")"}], " ", "z"}]}], 
      ")"}], "2"], "+", 
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{
       RowBox[{
        RowBox[{"-", "24"}], " ", "x", " ", "y", " ", "z"}], "+", 
       RowBox[{"24", " ", "y", " ", 
        RowBox[{"(", 
         RowBox[{"1", "-", "x", "-", "y", "-", "z"}], ")"}], " ", "z"}]}], 
      ")"}], "2"]}], ",", " ", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"x", ",", "y", ",", "z"}], "}"}], " ", "\[Element]", 
    RowBox[{"Tetrahedron", "[", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"1", ",", " ", "0", ",", "0"}], "}"}], ",", " ", 
       RowBox[{"{", 
        RowBox[{"0", ",", " ", "1", ",", " ", "0"}], "}"}], " ", ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", " ", "0", ",", " ", "1"}], "}"}], ",", " ", 
       RowBox[{"{", 
        RowBox[{"0", ",", "0", ",", "0"}], "}"}]}], "}"}], "]"}]}]}], "]"}]], \
"Input",
 CellChangeTimes->{{3.746354672865818*^9, 3.746354680560017*^9}}],

Cell[BoxData["0.0380952380952381`"], "Output",
 CellChangeTimes->{3.746354681871682*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Solve", "[", 
  RowBox[{
   RowBox[{
    RowBox[{"0.0380952380952381`", " ", "x"}], " ", "\[Equal]", " ", 
    "0.0015873015873015827"}], ",", " ", "x"}], "]"}]], "Input",
 CellChangeTimes->{{3.746354723887937*^9, 3.7463547559696198`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"{", 
   RowBox[{"x", "\[Rule]", "0.041666666666666546`"}], "}"}], "}"}]], "Output",\

 CellChangeTimes->{{3.746354737592248*^9, 3.746354757250121*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{".0416667", 
  RowBox[{"basis", "[", 
   RowBox[{"x", ",", " ", "y", ",", " ", "z"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.746354766631817*^9, 3.74635478296106*^9}}],

Cell[BoxData[
 RowBox[{"1.0000008`", " ", "x", " ", "y", " ", 
  RowBox[{"(", 
   RowBox[{"1", "-", "x", "-", "y", "-", "z"}], ")"}], " ", "z"}]], "Output",
 CellChangeTimes->{3.746354783309657*^9}]
}, Open  ]]
},
WindowSize->{1440, 874},
WindowMargins->{{Automatic, -1}, {-241, Automatic}},
FrontEndVersion->"11.0 for Linux x86 (64-bit) (July 28, 2016)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 780, 22, 99, "Input"],
Cell[1341, 44, 719, 21, 110, "Input"],
Cell[2063, 67, 695, 21, 110, "Input"],
Cell[2761, 90, 726, 18, 33, "Input"],
Cell[CellGroupData[{
Cell[3512, 112, 153, 3, 32, "Input"],
Cell[3668, 117, 435, 10, 51, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4140, 132, 169, 3, 32, "Input"],
Cell[4312, 137, 89, 1, 63, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4438, 143, 239, 5, 32, "Input"],
Cell[4680, 150, 2432, 74, 94, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7149, 229, 335, 9, 35, "Input"],
Cell[7487, 240, 1319, 41, 51, "Output"]
}, Open  ]],
Cell[8821, 284, 1417, 44, 58, InheritFromParent],
Cell[10241, 330, 618, 14, 33, InheritFromParent],
Cell[10862, 346, 501, 12, 33, "Input"],
Cell[CellGroupData[{
Cell[11388, 362, 950, 25, 35, "Input"],
Cell[12341, 389, 283, 5, 30, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[12661, 399, 1684, 46, 59, "Input"],
Cell[14348, 447, 133, 2, 30, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[14518, 454, 1179, 29, 32, "Input"],
Cell[15700, 485, 277, 5, 30, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[16014, 495, 661, 18, 35, "Input"],
Cell[16678, 515, 392, 12, 36, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[17107, 532, 1025, 29, 42, InheritFromParent],
Cell[18135, 563, 114, 1, 30, "Output"]
}, Open  ]],
Cell[18264, 567, 92, 1, 30, "Input"],
Cell[CellGroupData[{
Cell[18381, 572, 1100, 33, 32, "Input"],
Cell[19484, 607, 894, 28, 51, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[20415, 640, 1561, 45, 92, "Input"],
Cell[21979, 687, 157, 3, 30, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[22173, 695, 229, 6, 32, "Input"],
Cell[22405, 703, 112, 1, 30, "Output"]
}, Open  ]],
Cell[22532, 707, 498, 13, 33, "Input"],
Cell[CellGroupData[{
Cell[23055, 724, 164, 3, 32, "Input"],
Cell[23222, 729, 179, 4, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[23438, 738, 195, 6, 32, "Input"],
Cell[23636, 746, 319, 9, 36, "Output"]
}, Open  ]],
Cell[23970, 758, 531, 14, 32, "Input"],
Cell[CellGroupData[{
Cell[24526, 776, 282, 7, 32, InheritFromParent],
Cell[24811, 785, 259, 9, 32, "Output"]
}, Open  ]],
Cell[25085, 797, 508, 13, 33, "Input"],
Cell[CellGroupData[{
Cell[25618, 814, 1018, 28, 35, "Input"],
Cell[26639, 844, 130, 2, 30, "Output"]
}, Open  ]],
Cell[26784, 849, 569, 16, 35, "Input"],
Cell[CellGroupData[{
Cell[27378, 869, 1543, 44, 73, InheritFromParent],
Cell[28924, 915, 88, 1, 30, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[29049, 921, 263, 6, 35, "Input"],
Cell[29315, 929, 191, 5, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[29543, 939, 192, 4, 32, "Input"],
Cell[29738, 945, 198, 4, 32, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

