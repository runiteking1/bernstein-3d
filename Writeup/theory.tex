\documentclass[11pt, letterpaper]{article}
\usepackage[utf8]{inputenc}
 
\usepackage{amsmath}
\usepackage{listings}
\usepackage{commath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{amsthm}

\newtheorem{theorem}{Theorem}[section]
\newtheorem{corollary}{Corollary}[theorem]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem*{remark}{Remark}

\DeclareMathOperator{\spn}{span}
\DeclareMathOperator*{\argmin}{\arg\!\min}
\newcommand{\F}{\mathcal{F}}
\newcommand{\E}{\mathcal{E}}

\begin{document}
	Let $T$ be the reference tetrahedron, and we will denote 
	\begin{align*}
		X = \mathbb{P}_p(T) = \{ x^\alpha y^\beta z^\gamma : \alpha + \beta + \gamma \le p  \}.
	\end{align*}
	The tentative splitting of the space is as follows: first the classic interior space is $X_I = X \cap H_1^0$. 
	Next, we will have four face spaces which are defined as 
	\begin{align*}
		X_{F_i} = \spn\{ \psi_{ij} : 0 \le i, j, i + j \le p - 2 \}
	\end{align*}
	and the 6 edge spaces
	\begin{align*}
		X_{E_i} = \spn\{\chi_n^{i} : 0 \le n \le p - 2 \}
	\end{align*}
	where $\chi_n^i$ is a basis for edge $i$. 
	Finally, we have the vertex spaces which are just $X_V = \spn \{ \varphi_i \}$ the span of 4 vertex functions. 

	We first create the minimal energy space by relying on the tilde notation, hence we have that 
	\begin{align*}
		X_B = \oplus \widetilde X_{F_i} \oplus \widetilde X_{E_i} \oplus \widetilde X_V
	\end{align*}
	where $X_B$ is the boundary space (i.e. $X_B \perp X_I$ and $X_B \oplus X_I = X$).
	Let's start with seeing what type of estimate we need to prove. 

	We first define a decomposition of $u \in X_B$.
	We have that 
	\begin{align*}
		u_V = \sum_{i=1}^4 u(v_i)\widetilde \varphi_i
	\end{align*}
	where $u(v_i)$ is the value at vertex $i$ and $\widetilde \varphi_i$ is the minimal extension of basis $i$. 
	Now we look at $u - u_V$ which is 0 at all the vertices. 
	Again, assuming that we have a basis for the edge space, then we can easily define $u_{E_i}$ in an analogous manner
	\begin{align*}
		u_{E_i} = \sum_{j=0}^{p-2} (u - u_V, \chi_j^i)|_E \widetilde \chi_j^i
	\end{align*}
	where $(\cdot, \cdot)|_E$ is the inner-product on the edge. 
	Finally, the four faces can be defined after we see that $u - u_V - \sum_{i=1}^6 u_{E_i}$ can be split into four faces in the minimal extension space. 

	Once again, we need to prove the three lemmas for the Toselli book. 
	Two of them are trivial once again, and really only requires us to be more exact with notation, so let's focus on the hard one (stable decomposition).
	What we need to show is 
	\begin{align*}
		\norm{u_V}^2 + \sum_{i=1}^6 \norm{u_{E_i}}^2 + \sum_{i=1}^4 \norm{u_{F_i}}^2 \le C(p) \norm{u}^2.
	\end{align*}

	So first, we have 
	\begin{align*}
		\norm{u_V}^2 \le \norm{\widetilde \varphi}^2 \sum_{i=1}^4 \abs{u(v_i)}^2 = \frac{1}{p^6} \max \abs{u(v_i)}^2 \le C\norm{u}^2
	\end{align*}
	assuming that we can have $\norm{\widetilde \varphi}^2 \sim p^{-6}$ (which is probably true).
	And we have to do a similar thing for the edge space (dropping the $i$ from symmetry)
	\begin{align*}
		\norm{u_{E}}^2 &= \norm{\sum_{j=0}^{p-2} (u - u_V, \chi_j)|_E \widetilde \chi_j }^2 \\
		&\le C\max_j \norm{\widetilde \chi_j}^2 \sum_{j=0}^{p-2} \abs{(u - u_V, \chi_j)|_E}^2 \\
		&\le C\max_j \norm{\widetilde \chi_j}^2 \sum_{j=0}^{p-2} \norm{u - u_V}_E^2 \norm{\chi_j}_E^2 \\
		&\le C\max_j \norm{\widetilde \chi_j}^2 \sum_{j=0}^{p-2} (\norm{u}_E^2 + \norm{u_V}_E^2) \norm{\chi_j}_E^2.
	\end{align*}
	We note that $\norm{u}_E^2 \le p^2 \norm{u}^2$, and that 
	\begin{align*}
		\norm{u_V}^2_E &= \norm{u(v_1)\varphi_1 + u(v_2) \varphi_2}^2_E \\
		&\le \abs{u(v_i)}^2 \norm{\varphi}^2_E \le C p^6 \norm{u}^2 p^{-2}
	\end{align*}
	where $v_1, v_2$ are the two vertices on the edge. 
	Hence, our estimate is 
	\begin{align*}
		\norm{u_E}^2 &\le C \norm{u} \max_j \norm{\widetilde \chi_j}^2 \sum_{j=0}^{p-2} p^4 \norm{\chi_j}^2_E.
	\end{align*}
	It might be better to not do a maximum, and keep it within the sum: 
	\begin{align*}
		\norm{u_E}^2 &\le C \norm{u}  \sum_{j=0}^{p-2} p^4 \norm{\chi_j}^2_E \norm{\widetilde \chi_j}^2
	\end{align*}	
	hence it is like a balancing act of having good norm growth on the edges and good growth on the total tetrahedron. 

	Finally, for the face, we can possibly have a lemma like the following:
	\begin{align*}
		\norm{u_{F_i}}^2 \le C(p)\norm{u - u_V - \sum_{i=1}^4 u_{E_i}}^2 
	\end{align*}
	which is to say that the norm on the individual face is less than sum of all of them (which we proved with some Hardy's inequality for the triangle case). 
	If we have that, then 
	\begin{align*}
		\norm{u_{F_i}}^2 &\le C(p)\norm{u - u_V - \sum_{i=1}^4 u_{E_i}}^2  \\
		&\le \norm{u}^2 + \norm{u_V}^2 + \sum_{i=1}^4 \norm{u_{E_i}}^2. 
	\end{align*}


	Let's prove a few lemma we know we will need anyways... 
	\begin{lemma}\label{lem:vertex-l2-bound}
		For $u \in \mathbb{P}_p(T)$, we have that 
		\begin{align*}
			\max_{i} \abs{u(v_i)} \le\frac{1}{2\sqrt{6}} (p+1) (p+2) (p+3) \norm{u}.
		\end{align*}
	\end{lemma}
	\begin{proof}
		For $0 \le i, j, k, i + j + k \le p$, define 
		\begin{align*}
			\psi_{ijk} = \sqrt{\frac{(2i+1)(i+j+1)(2 i+2 j+2 k+3)}{2}}P_i(\xi) \left( \frac{1-\eta}{2} \right)^i P_{j}^{(2i + 1, 0)}(\eta) \left( \frac{1 - \theta}{2} \right)^{i + j} P_{k}^{(2i + 2j + 2, 0)}  
		\end{align*}
		where 
		\begin{align*}
			\xi = \frac{2(1+x)}{-y-z} - 1 \qquad \eta = \frac{2(1+y)}{1 - z} - 1 \qquad \theta = z.
		\end{align*}
		This is an orthonormal basis for $\mathbb{P}_p(T)$. 
		Hence $u \in \mathbb{P}_p(T)$ can be written as $u = \sum_{i + j + k \le p} u_{ijk}\psi_{ijk}$ with $\norm{u} = \sum_{i + j + k \le p} u_{ijk}^2$.
		By symmetry, it suffices to prove the inequality for $(-1,-1, -1)$, then Cauchy-Schwarz gives
		\begin{align*}
			\abs{u(-1, -1, -1)}^2 &= \left(\sum_{i + j + k \le p} (-1)^{i+j+k}u_{ijk} \sqrt{\frac{(2i+1)(i+j+1)(2 i+2 j+2 k+3)}{2}}\right)^2 \\
			&\le \sum_{i+j+k\le p} u_{ijk}^2 \sum_{i+j+k\le p} \frac{(2i+1)(i+j+1)(2 i+2 j+2 k+3)}{2} \\
			&= \frac{1}{24} (p+1)^2 (p+2)^2 (p+3)^2\norm{u}^2.
		\end{align*}

	\end{proof}
		\begin{lemma}
		Need to bound the minimal energy extension of the vertex function... which is actually much harder to construct. 
		Maybe instead of a construction proof, we just show existence? 
	\end{lemma}
	\begin{proof}
	For the lower bound, we apply Lemma~\ref{lem:vertex-l2-bound}.

	For the upper-bound, by the property of the minimal $L^2$ extension, we simply need to construct a function $\bar \varphi$ such that $\widetilde \varphi = \bar \varphi$ on $\partial T$, and $\norm{\bar \varphi}^2 \le p^{-6}$ as $\norm{\widetilde \varphi} \le \norm{\bar \varphi}$.

	Using the collapsed Cartesian coordinate system, we have that for $u \in \mathbb{P}_p(T)$
	\begin{align*}
		\int_T u^2 \, dxdydz &= \int_{-1}^1 \int_{-1}^1 \int_{-1}^1 u^2(\xi, \eta, \theta) \frac{1 - \eta}{2} \left(\frac{1-\theta}{2} \right)^2 \, d\xi d\eta d\theta \\
		&= \sum_{i=0}^{N - 2} \sum_{j=0}^{N-2} \sum_{j=0}^{N-2} u^2(\xi, \eta, \theta) \frac{1 - \eta}{2} \left(\frac{1-\theta}{2} \right)^2
	\end{align*}
	where 
			\begin{align*}
			\xi = \frac{2(1+x)}{-y-z} - 1 \qquad \eta = \frac{2(1+y)}{1 - z} - 1 \qquad \theta = z.
		\end{align*}

	 	So we need to carefully choose the number of quadrature points such that we have 
	 	\begin{enumerate}
	 		\item Enough points such that we can establish the edge values
	 		\item Enough dofs in $\bar \varphi$ such that we can set everything else to be 0
	 		\item Enough accuracy such that the integral is close.
	 	\end{enumerate}
	\end{proof}

\end{document}