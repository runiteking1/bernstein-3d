\documentclass[11pt, letterpaper]{article}
\usepackage[utf8]{inputenc}
 
\usepackage{amsmath}
\usepackage{listings}
\usepackage{commath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{amsthm}
\usepackage{siunitx}
\newtheorem{theorem}{Theorem}[section]
\newtheorem{corollary}{Corollary}[theorem]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem*{remark}{Remark}

\DeclareMathOperator{\spn}{span}
\DeclareMathOperator*{\argmin}{\arg\!\min}

\begin{document}
	This document is for looking at the minimal extensions for a tet.
	Let $T$ be the reference tetrahedron, and the transformation from the tetrahedron to a cube is by
	\begin{align*}
		\xi = \frac{2(1+x)}{-y-z} - 1 \qquad \eta = \frac{2(1+y)}{1 - z} - 1 \qquad \theta = z.
	\end{align*}
	The Jacobian of this transformation is 
	\begin{align*}
		J = \frac{1-\eta}{2}\left( \frac{1 - \theta}{2}\right)^2.
	\end{align*}

	\section{Face}
	With this in mind, an orthogonal basis such that it is supported on the face $z=\theta =-1$ and zero on three other faces is 
	\begin{align*}
		\widetilde \varphi_{ijk} = \frac{1-\xi}{2} \frac{1 + \xi}{2} P_i^{(2,2)}(\xi) \left(\frac{1 - \eta}{2} \right)^{i+2} \frac{1+\eta}{2} P_j^{(2i + 5, 2)}(\eta) \left( \frac{1-\theta}{2}\right)^{i + 3 + j} P_k^{(2i + 2j + 8, 0)} (\theta).
	\end{align*}
	The indices for $i, j, k$ is $0 \le i, j, k, i + j + k \le p-3$ (e.g. for $p=4$, there are 3 face degree of freedom, and 1 interior basis to minimize).
	% Note that we can have a different basis if we change the indices (i.e. from $i$ to $i-1$)? 
	% Would this matter? 
	% From numerical experiments, this didn't matter, but I should update this pdf to reflect this.
	% Furthermore, it is unusual that the extension into the tetrahedron mattered. 

	Let us verify that this is zero on the faces that are not $z = -1$: 
	\begin{enumerate}
		\item Face where $x = -1$: this corresponds to when $\xi = -1$, 
		\item Face where $y = -1$: this corresponds to when $\eta = -1$, 
		\item Face through points $ (1, -1, -1), (-1, 1, -1), (-1, -1, 1)$ (i.e. $x + y + z + 1 = 0$): corresponds to $\xi = 1$.
	\end{enumerate}
	Let $\widetilde u = \sum_{i + j + k \le p - 3} u_{ijk} \widetilde \varphi_{ijk}$ be a minimal extension of some face function.
	We wish to minimize the $L^2$ norm, subject to the face constraint which will be specified later, which is 
	\begin{align*}
		\norm{\widetilde u}^2_T = \sum_{i + j + k \le p - 3} u_{ijk}^2 \mu_i \nu_j \rho_{k}
	\end{align*}
	where 
	\begin{align*}
		\mu_i &= \int \left(\frac{1-x^2}{4}\right)^2(P_i^{(2,2)})^2 \, dx = \frac{2 (i+1) (i+2)}{(i+3) (i+4) (2 i+5)} \\
		\nu_j &= \int \left(\frac{1-x}{2}\right)^{2i + 5} \left(\frac{1 + x}{2}\right)^2(P_j^{(2i + 5,2)})^2 \, dx = \frac{(j+1) (j+2)}{(i+j+4) (2 i+j+6) (2 i+j+7)} \\
		\rho_k &= \int \left(\frac{1-x}{2}\right)^{2i+2j + 8} (P_k^{(2i + 2j + 8,0)})^2 \, dx = \frac{2}{2k + 2i + 2j + 9}.
	\end{align*}

	Now let's determine the face constraint. 
	The value is at face $F$ is $z = -1 \implies \xi = \frac{2(1+x)}{1 - y} - 1, \eta = y, \theta = -1$: 
	\begin{align*}
		-\frac{1}{4} (x+1) (x+y) P_i^{(2,2)}(\frac{2 x+y+1}{1-y}) \left(\frac{1 - y}{2} \right)^{i} \frac{1+y}{2} P_j^{(2i + 5, 2)}(y) P_k^{(2i + 2j + 8, 0)} (-1).
	\end{align*}
	Now let us find an orthogonal basis for strictly the interior of a face of a triangle in terms of the collapsed coordinates in 2D, let the collapsed coordinate for the strictly 2D face triangle be $\xi_2 = \frac{2(1+x)}{1 - y} - 1, \eta_2 = y$ or $x, y$ (e.g. $\xi|_{z=-1}, \eta|_{z=-1}$).
	Recall that the Jacobian of the 2D version is $J = (1 - \eta_2)/2$, then a face function is for $w_{ij} \in \mathbb{R}$ 
	\begin{align*}
		u(x, y) = \sum_{i + j\le p-3} w_{ij}\frac{1 - \xi_2}{2} \frac{1 + \xi_2}{2} P_i^{(2,2)}(\xi_2) \left( \frac{1 - \eta_2}{2}\right)^{i+2} \frac{1 + \eta_2}{2} P_j^{(2i + 5, 2)}(\eta_2)
	\end{align*}
	which when plugged in with the $x, y$ coordinates, we have 
	\begin{align*}
		\frac{-1}{4}(x+1)(x + y)P_i^{(2,2)}(\frac{2 x+y+1}{1-y}) \left( \frac{1-y}{2}\right)^i \frac{1 + y}{2} P_j^{(2i + 5, 2)}(y)
	\end{align*}
	In order for $\widetilde u$ to be a minimal extension of the face function with $w_{ij}$, it must equal the face at $z = -1$.
	Hence, we have that 
	\begin{align*}
		w_{ij} &= \sum_{k=0}^{p-3-i-j} \widetilde u_{ijk} P_k^{(2i + 2j + 8, 0)}(-1) =  \sum_{k=0}^{p-3-i-j} (-1)^k \widetilde u_{ijk} .
	\end{align*}
	Now, by C-S
	\begin{align*}
		w_{ij}^2 &\le \sum_{k = 0}^{p-3-i-j} \widetilde u_{ijk}^2 \rho_k \sum_{k=0}^{p-3-i-j} \rho_k^{-1} \\
		&=  \sum_{k = 0}^{p-3-i-j} \widetilde u_{ijk}^2 \rho_k \sum_{k=0}^{p-3-i-j} \frac{2k + 2i + 2j +9}{2} \\
		&= \frac{1}{2} (p - i - j - 2) (i+j+p+6)\sum_{k=0}^{p-3-i-j} \widetilde u_{ijk}^2 \rho_k
	\end{align*}
	with equality if there exists a $\lambda$ such that $\widetilde u_{ijk} P^{(2i + 2j + 8, 0)}_k(-1) \rho_k^{1/2} = \lambda \rho_{k}^{-1/2}$.

	Now, we have that 
	\begin{align*}
		\norm{\widetilde u}^2 &= \sum_{i + j + k\le p - 3} u_{ijk}^2 \mu_i \nu_j \rho_k \\
		&= \sum_{i=0}^{p-3} \mu_i \sum_{j=0}^{p-3-i}  \nu_j\sum_{k=0}^{p-3-i-j} u_{ijk}^2  \rho_k \\
		&\ge \sum_{i=0}^{p-3} \mu_i \sum_{j=0}^{p-3-i}  \nu_j \frac{2w_{ij}^2}{(p-i-j-2)(i+j + p + 6)} \\
		&= \sum_{i=0}^{p-3} \sum_{j=0}^{p-3-i} \frac{4 (i+1) (i+2) (j+1) (j+2)w_{ij}^2}{(i+3) (i+4) (2 i+5) (i+j+4) (2 i+j+6) (2 i+j+7) (p-i-j-2) (i+j+p+6)}
	\end{align*}
	Hence, this is actually the minimal energy extension norm from the face, as there are no more $k$ indices.

	When implementing, I actually use this basis (shifted $i, j$ coordinates). 
	The basis I actually use is: 
	\begin{align*}
		\widetilde \varphi_{ijk} = \frac{1-\xi}{2} \frac{1 + \xi}{2} P_{i-1}^{(2,2)}(\xi) \left(\frac{1 - \eta}{2} \right)^{i+1} \frac{1+\eta}{2} P_{j-1}^{(2i + 3, 2)}(\eta) \left( \frac{1-\theta}{2}\right)^{i + j + 1} P_k^{(2i + 2j + 4, 0)} (\theta).
	\end{align*}
	The number I get is the same, but what is more disconcerting is that changing the $k$ index changes the diagonal. 
	This doesn't match the theory at all, as $k$ doesn't factor into the whole basis thing; only the first few indices match.


	% \subsection{Different basis}
	% In this subsection, we do a slightly different calculation. 
	% When implementing, I actually use this basis (shifted $i, j$ coordinates). 
	% In theory this should be the same, but let us verify this. 
	% The basis I actually use is: 
	% \begin{align*}
	% 	\widetilde \varphi_{ijk} = \frac{1-\xi}{2} \frac{1 + \xi}{2} P_{i-1}^{(2,2)}(\xi) \left(\frac{1 - \eta}{2} \right)^{i+1} \frac{1+\eta}{2} P_{j-1}^{(2i + 3, 2)}(\eta) \left( \frac{1-\theta}{2}\right)^{i + j + 1} P_k^{(2i + 2j + 4, 0)} (\theta).
	% \end{align*}
	% The indices is such that $\mathcal{I} = 1 \le i, j, i + j \le p - 1 - k, 0 \le k \le p-3$.
	% The $L^2$ norm written in this basis is 
	% 	\begin{align*}
	% 	\norm{\widetilde u}^2_T = \sum_{i, j, k \in \mathcal{I}} u_{ijk}^2 \mu_i \nu_j \rho_{k}
	% \end{align*}
	% where 
	% \begin{align*}
	% 	\mu_i &= \int \left(\frac{1-x^2}{4}\right)^2(P_{i-1}^{(2,2)})^2 \, dx = \frac{2 i (i+1)}{(i+2) (i+3) (2 i+3)} \\
	% 	\nu_j &= \int \left(\frac{1-x}{2}\right)^{2i + 3} \left(\frac{1 + x}{2}\right)^2(P_{j-1}^{(2i + 3,2)})^2 \, dx = \frac{j (j+1)}{(i+j+2) (2 i+j+3) (2 i+j+4)} \\
	% 	\rho_k &= \int \left(\frac{1-x}{2}\right)^{2i+2j + 4} (P_k^{(2i + 2j + 4,0)})^2 \, dx = \frac{2}{2 i+2 j+2 k+5}.
	% \end{align*}

	% Note that we can have a different basis if we change the indices (i.e. from $i$ to $i-1$)? 
	% Would this matter? 
	% From numerical experiments, this didn't matter, but I should update this pdf to reflect this.
	% Furthermore, it is unusual that the extension into the tetrahedron mattered. 


	\section{Edge}
	Let us do a similar thing on an edge. 
	First, we need a basis which is orthogonal on $T$ such that it is zero on the other 5 edges (we will be non-zero on the edge $z = -1, y = -1$): 
	\begin{align*}
		\widetilde \varphi_{ijk} = \frac{1-\xi}{2} \frac{1 + \xi}{2} P_i^{(2,2)}(\xi) \left(\frac{1 - \eta}{2} \right)^{i+2} P_j^{(2i + 5, 0)}(\eta) \left( \frac{1-\theta}{2}\right)^{i + 2 + j} P_k^{(2i + 2j + 6, 0)} (\theta).
	\end{align*}
	Let $\widetilde u = \sum_{i + j + k \le p - 2} u_{ijk} \widetilde \varphi_{ijk}$ be a minimal extension, we need to minimize this against the edges (note the change in $i+j+k \le p -2$ rather than $p-3$ as there are two faces), then we have that 
	\begin{align*}
		\norm{\widetilde u}^2 = \sum_{i + j + k \le p - 2} u_{ijk}^2 \mu_i \nu_j \rho_{k}
	\end{align*}
	where 
	\begin{align*}
		\mu_i &= \int \left(\frac{1-x^2}{4}\right)^2(P_i^{(2,2)})^2 \, dx =  \frac{2 (i+1) (i+2)}{(i+3) (i+4) (2 i+5)}\\
		\nu_j &= \int \left(\frac{1-x}{2}\right)^{2i + 5}(P_j^{(2i + 5,0)})^2 \, dx = \frac{1}{j+i+3}\\
		\rho_k &= \int \left(\frac{1-x}{2}\right)^{2i+2j + 6} (P_k^{(2i + 2j + 6,0)})^2 \, dx = \frac{2}{2i + 2j + 2k + 7}
	\end{align*}
	Now at the $z = -1, y = -1$ we have that $\xi = x, \eta = -1, \theta = -1$, then it really just collapses to 
	\begin{align*}
		\frac{1-x}{2} \frac{1 + x}{2} P_i^{(2,2)}(x)  P_j^{(2i + 5, 0)}(-1)P_k^{(2i + 2j + 6, 0)} (-1).
	\end{align*}
	Recall that the 1D basis for our edge functions are 
	\begin{align*}
		u(x) = \frac{1}{4}\sum_{i=0}^{p-2} w_i (1-x^2)P_i^{(2,2)}(x).
	\end{align*}
	Hence, to satisfy the condition that on the edges they are equal, we have for all $i$
	\begin{align*}
		w_i = \sum_{j=0}^{p-2-i} \sum_{k=0}^{p-2-j-i} (-1)^{k+j}\widetilde u_{ijk} = \sum_{j+k \le p-2-i}(-1)^{k+j}\widetilde u_{ijk}.
	\end{align*}
	So by C-S, we have \footnote{Think of this double sum as an inner-product, equality should be reachable.}
	\begin{align*}
		w_i^2 &\le \sum_{j+k \le p-2-i}\widetilde u_{ijk}^2 \nu_j \rho_k \sum_{j+k\le p-2-i} \nu_j^{-1} \rho_k^{-1} \\
		&= \sum_{j+k \le p-2-i}\widetilde u_{ijk}^2 \nu_j \rho_k \sum_{j = 0}^{p-2-i} \sum_{k=0}^{p-2-i-j} (i + j + 3) \frac{2i + 2j + 2k + 7}{2} \\
		&= \left(\frac{1}{8} (i-p+1) (i+p+4) \left(i^2+5 i-p (p+5)\right)\right) \sum_{j+k \le p-2-i}\widetilde u_{ijk}^2 \nu_j \rho_k
	\end{align*}

	Hence, we have that 
	\begin{align*}
		\norm{\widetilde u}^2 &= \sum_{i + j + k\le p - 2} u_{ijk}^2 \mu_i \nu_j \rho_k \\
		&= \sum_{i=0}^{p-2} \mu_i \sum_{j+k=0}^{p-2-i}  u_{ijk}^2\nu_j\rho_k    \\
		&\ge \sum_{i=0}^{p-2} \mu_i \frac{w_i^2}{\left(\frac{1}{8} (i-p+1) (i+p+4) \left(i^2+5 i-p (p+5)\right)\right)}    \\
		&= \sum_{i=0}^{p-2}\frac{16 (i+1) (i+2)w_i^2}{(i+3) (i+4) (2 i+5) (i-p+1) (i+p+4) \left(i^2+5 i-p (p+5)\right)}
	\end{align*}
	This is what we get as the minimal energy extension norm from the edge.

	Unfortunately, the coefficient does not hold with my current implementation (we do get diagonals!).
	Implementing the basis with $j=0, k=0$, for $i=p-2$, we have the exact quantity, but even while altering $j,k$ across the acceptable ranges, we do not get the remaining values.
	What is curious is that for $i=p-3$, the coefficient is actually of the correct amount, but for $\widetilde p = p-1$.

	Example: here's the table of coefficients for $p=2, \ldots, 6$ (each row) and columns are the $i$ index.
	\begin{align*}
		\left(
\begin{array}{ccccc}
 0.00634921 & \text{} & \text{} & \text{} & \text{} \\
 0.0015873 & 0.0047619 & \text{} & \text{} & \text{} \\
 0.000617284 & 0.00126984 & 0.00323232 & \text{} & \text{} \\
 0.000296296 & 0.000519481 & 0.000897868 & 0.00222 & \text{} \\
 0.000161616 & 0.00025974 & 0.000379867 & 0.000634286 & 0.00156986 \\
\end{array}\right)
	\end{align*}
	For $p=4$, we have the following on the diagonal: 
		\SI{3.924162e-03}, \SI{4.761905e-03}, \SI{3.232323e-03} with $i$ index increasing. 
	Note that it matches on the diagonal for $i=1, 2$, but $i=1$ matches on the wrong row...
	This seems to hold for different $p$ also. 

	Somehow our coefficents gives much \emph{better} norms than what we expect? 
	Are we minimizing the coefficients too good? 

	The reason for this, I believe, is that for a given mode on the edge, there are several different face functions. 
	What we are doing is actually taking a linear combination of these modes with different face functions, and minimizing over this. 
	Let us take a deeper look: 
	The minimum is achieved for the C-S inequality if there exists a constant $\lambda$ such that the vectors are collinear
	\begin{align*}
		(-1)^{k+j}\widetilde u_{ijk} (\nu_j \rho_k)^{1/2} = \lambda (\nu_j \rho_k)^{-1/2}.
	\end{align*}
	Taking the sum over $j + k \le p - 2- i$, we have that 
	\begin{align*}
		w_i = \sum_{j + k \le p - 2 - i} (-1)^{k+j}\widetilde u_{ijk} =  \lambda \sum_{j + k \le p - 2 - i}(\nu_j \rho_k)^{-1} \implies \lambda_i := \frac{w_i}{ \sum_{j + k \le p - 2 - i}(\nu_j \rho_k)^{-1} }.
	\end{align*}
	Using this, we can actually figure the \emph{exact} face function coefficient as 
	\begin{align*}
		\widetilde u_{ijk} = (-1)^{k+j} (\nu_j \rho_k)^{-1} \lambda_i = (-1)^{k+j} (\nu_j \rho_k)^{-1} \frac{w_i}{ \sum_{j + k \le p - 2 - i}(\nu_j \rho_k)^{-1} }.
	\end{align*}
	Looking at $ijk = p-2, 0, 0$, we see that the coefficient is indeed 1, hence that's why it's matching (also common sense).
	Indeed, for a given $i=1, p = 4$, we actually have the correct diagonal if we add the correct face terms into the basis functions (which makes computation a hassle, need something like sum-factorization just to evaluate the basis?... we should discuss this a bit more).

	\subsection{Suboptimal edges}
	Let us do a similar thing on an edge. 
	First, we need a basis which is orthogonal on $T$ such that it is zero on the other 5 edges (we will be non-zero on the edge $z = -1, y = -1$): 
	\begin{align*}
		\widetilde \varphi_{ijk} = \frac{1-\xi}{2} \frac{1 + \xi}{2} P_i^{(2,2)}(\xi) \left(\frac{1 - \eta}{2} \right)^{i+2} P_j^{(2i + 5, 0)}(\eta) \left( \frac{1-\theta}{2}\right)^{i + 2 + j} P_k^{(2i + 2j + 6, 0)} (\theta).
	\end{align*}
	Let us see its values on the faces which it is supported on: 
	\begin{enumerate}
		\item The face $z = -1$: this corresponds to $\xi = \frac{2(1+x)}{1-y} - 1, \eta = y, \theta = -1$,
		\begin{align*}
			\widetilde \varphi_{ijk} = \frac{1-\xi}{2} \frac{1 + \xi}{2} P_i^{(2,2)}(\xi) \left(\frac{1 - y}{2} \right)^{i+2} P_j^{(2i + 5, 0)}(y)(-1)^k.
		\end{align*}

		\item The face $y=-1$: this corresponds to $\xi = \frac{2(1+x)}{1-z} - 1, \eta = -1, \theta = z$,
		\begin{align*}
			\widetilde \varphi_{ijk} = \frac{1-\xi}{2} \frac{1 + \xi}{2} P_i^{(2,2)}(\xi) (-1)^{j} \left( \frac{1-z}{2}\right)^{i + 2 + j} P_k^{(2i + 2j + 6, 0)} (z).	
		\end{align*}
	\end{enumerate}


\end{document}