import numpy as np


def volume(v0, v1, v2, v3):
    """
    Calculates volume of tetrahedron (source: https://people.eecs.berkeley.edu/~wkahan/VtetLang.pdf)

    Also returns area of the faces opposite of the vertices

    :param v0: vertices in any order
    :param v1: vertices in any order
    :param v2: vertices in any order
    :param v3: vertices in any order
    :return: tuple where the first index is the volume of the vertices, and then area of the 4 faces
    """
    U = np.linalg.norm(v1 - v0)
    V = np.linalg.norm(v2 - v1)
    W = np.linalg.norm(v0 - v2)

    u = np.linalg.norm(v3 - v2)
    v = np.linalg.norm(v3 - v0)
    w = np.linalg.norm(v3 - v1)

    s = (u + V + w)/2
    area0 = np.sqrt(s * (s - u) * (s - V) * (s - w))

    s = (u + v + W)/2
    area1 = np.sqrt(s * (s - u) * (s - v) * (s - W))

    s = (U + v + w)/2
    area2 = np.sqrt(s * (s - U) * (s - v) * (s - w))

    s = (U + V + W)/2
    area3 = np.sqrt(s * (s - U) * (s - V) * (s - W))

    if np.sqrt((4 * u * u * v * v * w * w - u * u * ((v * v + w * w - U * U) ** 2) - v * v * (
            (w * w + u * u - V * V) ** 2) - w * w * ((u * u + v * v - W * W) ** 2) +
                    (v * v + w * w - U * U) * (w * w + u * u - V * V) * (u * u + v * v - W * W))) / 12 == 0.0:
        print(U, V, W, u, v, w, area0, area1, area2, area3)

        X = (w - U + v) * (U + v + w)
        Y = (u - V + w) * (V + w + u)
        Z = (v - W + u) * (W + u + v)
        x = (U - v + w) * (v - w + U)
        y = (V - w + u) * (w - u + V)
        z = (W - u + v) * (u - v + W)
        xi = np.sqrt(x * Y * Z)
        eta = np.sqrt(y * Z * X)
        zeta = np.sqrt(z * X * Y)
        lamb = np.sqrt(x*y*z)
        print(np.sqrt(xi + eta + zeta - lamb) * (lamb + xi + eta - zeta) * (eta + zeta + lamb - xi) * (zeta + lamb + xi - eta) / (192*u * v* w))
        print('\n\n')
    return (np.sqrt((4 * u * u * v * v * w * w - u * u * ((v * v + w * w - U * U) ** 2) - v * v * (
            (w * w + u * u - V * V) ** 2) - w * w * ((u * u + v * v - W * W) ** 2) +
                    (v * v + w * w - U * U) * (w * w + u * u - V * V) * (u * u + v * v - W * W))) / 12, area0, area1,
            area2, area3)




if __name__ == '__main__':
    vertices = np.zeros((3, 4))
    vertices[:, 3] = [0, 0, 0]
    vertices[:, 0] = [1, 0, 0]
    vertices[:, 1] = [0, 1, 0]
    vertices[:, 2] = [0, 0, 1]

    print(volume(vertices[:, 0], vertices[:, 1], vertices[:, 2], vertices[:, 3]))
