"""
Computes the stiffness mass matrix in 3D

"""

# import sys
# sys.path.append('../')

import numpy as np
from scipy.special import binom
# from position import pos3d
from CythonKernels.pos3d import pos3d
from volume import volume


# @profile
def stiffness(n: int, moments: np.ndarray, grad_bary):
    """
    Basic skeleton for element mass matrix

    :param grad_bary: gradient of barycentric coordinate with first term opposite v0, second opposite v1 etc.
    :param n: order of approximation
    :param moments: array of moments arranged such that (alpha_1, alpha_2, alpha_3,alpha_4) corresponds to
            moments[alpha_1, alpha_2, alpha_3]. This should be of order 2n - 2
    :return: Stiffness matrix
    arranged using pos3d
    """
    # Precompute
    mukl = np.zeros((2 * n - 1, 2 * n - 1, 2 * n - 1, 4, 4))
    for k in range(4):
        for l in range(4):
            for alpha_1 in range(2 * n - 1):
                for alpha_2 in range(2 * n - 1 - alpha_1):
                    for alpha_3 in range(2 * n - 1 - alpha_1 - alpha_2):
                        mukl[alpha_1, alpha_2, alpha_3, k, l] = np.dot(grad_bary[l],
                                                                       np.dot(moments[alpha_1, alpha_2, alpha_3],
                                                                              grad_bary[k]))
    # Create output matrix
    elem_stiff = np.zeros((int(binom(n + 3, 3)), int(binom(n + 3, 3))))

    # Scale down
    n = n - 1

    # e_i vectors
    ei = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1], [0, 0, 0]], dtype=np.int)

    for alpha_1 in range(n + 1):
        for beta_1 in range(n + 1):
            w_1 = binom(alpha_1 + beta_1, alpha_1) / binom(2 * n, n)

            for alpha_2 in range(n - alpha_1 + 1):
                for beta_2 in range(n - beta_1 + 1):
                    w_2 = w_1 * binom(alpha_2 + beta_2, alpha_2)

                    for alpha_3 in range(n - alpha_1 - alpha_2 + 1):
                        for beta_3 in range(n - beta_1 - beta_2 + 1):
                            w_3 = w_2 * binom(alpha_3 + beta_3, alpha_3)

                            w_4 = (n+1)**2 * w_3 * binom(2 * n - alpha_1 - alpha_2 - alpha_3 - beta_1 - beta_2 - beta_3,
                                              n - alpha_1 - alpha_2 - alpha_3)

                            for k in range(4):
                                for l in range(4):
                                    indices_1 = tuple(ei[k] + [alpha_1, alpha_2, alpha_3])
                                    indices_2 = tuple(ei[l] + [beta_1, beta_2, beta_3])
                                    # elem_stiff[pos3d(*indices_1, n + 1), pos3d(*indices_2, n + 1)] += \
                                    #     (n + 1)**2 * w_4 * np.dot(grad_bary[k], np.dot(
                                    #     moments[alpha_1 + beta_1, alpha_2 + beta_2, alpha_3 + beta_3], grad_bary[l]))
                                    elem_stiff[pos3d(*indices_1, n + 1), pos3d(*indices_2, n + 1)] += \
                                        w_4 * mukl[
                                            alpha_1 + beta_1, alpha_2 + beta_2, alpha_3 + beta_3, k, l]

    return elem_stiff


def gradient_barycentric(v0, v1, v2, v3):
    vol, area0, area1, area2, area3 = volume(v0, v1, v2, v3)

    # Generate normal vectors to planes
    n0 = np.cross(v2 - v1, v3 - v1)
    n1 = np.cross(v2 - v0, v3 - v0)
    n2 = np.cross(v1 - v0, v3 - v0)
    n3 = np.cross(v2 - v0, v1 - v0)

    # Make sure normals point out
    n0 = n0 / np.linalg.norm(n0)
    if np.dot(n0, v0 - v1) < 0:
        n0 = -n0

    n1 = n1 / np.linalg.norm(n1)
    if np.dot(n1, v1 - v0) < 0:
        n1 = -n1

    n2 = n2 / np.linalg.norm(n2)
    if np.dot(n2, v2 - v0) < 0:
        n2 = -n2

    n3 = n3 / np.linalg.norm(n3)
    if np.dot(n3, v3 - v0) < 0:
        n3 = -n3
    # print(vol, area0, area1, area2, area3)
    return [n0 * area0 / (3 * vol), n1 * area1 / (3 * vol), n2 * area2 / (3 * vol), n3 * area3 / (3 * vol)]


def A(x):
    # return np.array([[x[0],0, np.exp(x[1])], [0, np.sin(x[2]), 0], [np.exp(x[1]),0,1]])
    return np.array([[x[0] * x[0], 0, 0], [0, x[1] / 2, 0], [0, 0, x[2] + 5]])


if __name__ == '__main__':
    from scipy import special
    from duffy import duffy
    from moment import moment

    np.set_printoptions(precision=6, linewidth=160)

    # Create tetrahedron
    vertices = np.zeros((3, 4))
    vertices[:, 0] = [0, 0, 0]
    vertices[:, 1] = [1, 0, 0]
    vertices[:, 2] = [0, 1, 0]
    vertices[:, 3] = [0, 0, 1]

    # Obtain normals
    normal = gradient_barycentric(vertices[:, 0], vertices[:, 1], vertices[:, 2], vertices[:, 3])
    vol, _, _, _, _ = volume(vertices[:, 0], vertices[:, 1], vertices[:, 2], vertices[:, 3])

    # First, generate the quadrature points
    p = 10
    my_q = p + 2
    my_xis = np.zeros((3, my_q))
    my_omegas = np.zeros((3, my_q))
    for i in range(3):
        nodes, weights = special.roots_sh_jacobi(my_q, i + 1, 1)
        my_xis[i, :] = nodes
        my_omegas[i, :] = weights

    fa = np.zeros((my_q, my_q, my_q, 3, 3))
    for i in range(my_q):
        for j in range(my_q):
            for k in range(my_q):
                fa[i, j, k] = A(duffy([my_xis[2, i], my_xis[1, j], my_xis[0, k]], vertices))

    # Return moment (need to scale correctly with area)
    mmts = np.zeros((2 * p - 1, 2 * p - 1, 2 * p - 1, 3, 3))
    for i in range(3):
        for j in range(3):
            mmts[:, :, :, i, j] = moment(fa[:, :, :, i, j], my_q, 2 * p - 2, my_xis, my_omegas)

    from CythonKernels.stiffness import stiffness as test

    # stiff = vol * 6 * (stiffness(p, mmts, normal))
    stiff = (test(p, mmts, normal))
    print(stiff)
