from unittest import TestCase
from volume import volume
import numpy as np


class TestVolume(TestCase):
    """
    Test cases for volume pulled from https://people.sc.fsu.edu/~jburkardt/datasets/tetrahedrons/tetrahedrons.html
    """
    def test_volume_standard(self):
        vertices = np.zeros((3, 4))
        vertices[:, 3] = [0, 0, 0]
        vertices[:, 0] = [1, 0, 0]
        vertices[:, 1] = [0, 1, 0]
        vertices[:, 2] = [0, 0, 1]

        np.testing.assert_almost_equal(1 / 6, volume(vertices[:, 0], vertices[:, 1], vertices[:, 2], vertices[:, 3])[0])

    def test_volume_2(self):
        vertices = np.zeros((3, 4))
        vertices[:, 3] = [1, 2, 3]
        vertices[:, 0] = [2, 2, 3]
        vertices[:, 1] = [1, 3, 3]
        vertices[:, 2] = [1, 2, 9]

        np.testing.assert_almost_equal(1, volume(vertices[:, 0], vertices[:, 1], vertices[:, 2], vertices[:, 3])[0])

    def test_volume_3(self):
        vertices = np.zeros((3, 4))
        vertices[:, 3] = [1, 2, 3]
        vertices[:, 0] = [4, 1, 2]
        vertices[:, 1] = [2, 4, 4]
        vertices[:, 2] = [3, 2, 5]

        np.testing.assert_almost_equal(8 / 3, volume(vertices[:, 0], vertices[:, 1], vertices[:, 2], vertices[:, 3])[0])
