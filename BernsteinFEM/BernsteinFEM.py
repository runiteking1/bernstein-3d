import numpy as np
from typing import Callable
from bernstein_mesh import BernsteinMesh
from scipy import special
from scipy.sparse import lil_matrix, csr_matrix, coo_matrix
# from duffy import duffy
# from moment import moment
from mass import mass
from stiffness import gradient_barycentric, volume
from CythonKernels.pos3d import pos3d
from CythonKernels.stiffness import stiffness
from CythonKernels.mass import mass
from CythonKernels.moment import moment
from CythonKernels.duffy import duffy
from CythonKernels.convect import convection

# Plotting imports
from pyevtk.hl import unstructuredGridToVTK
from pyevtk.vtk import VtkTetra, VtkWedge, VtkPyramid, VtkTriangle


def default_mass(x):
    return 1


def default_stiff(x):
    return np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])


def default_convec(x):
    return np.array([1, 1, 1])


def default_load(x):
    return 1


class BernsteinFEM(object):
    """
    Creates a Bernstein FEM object which creates matrices for 3D
    """

    def __init__(self, mymesh: BernsteinMesh):
        # self.mesh = mymesh
        self.p = mymesh.p
        self.eldof = mymesh.eldof
        self.elnode = mymesh.elnode
        self.elements = mymesh.elements
        self.dirchlet_bc = mymesh.dirchlet_bc
        self.dirchlet_dofs = mymesh.dirchlet_dofs
        self.coordinates = mymesh.coordinates
        self.dofs_per_element = mymesh.dofs_per_element
        self.lookup_dof = mymesh.lookup_dof
        self.lookup_multi = mymesh.lookup_multi
        self.num_edges = mymesh.num_edges
        self.num_faces = mymesh.num_faces
        self.el_face = mymesh.el_face
        # for i in range(mymesh.dofs_per_element):
        #     print(i, self.lookup_dof[i])
        # Set up quadrature points
        self.q = self.p + 2
        self.xis = np.zeros((3, self.q))
        self.omegas = np.zeros((3, self.q))
        for i in range(3):
            nodes, weights = special.roots_sh_jacobi(self.q, i + 1, 1)
            self.xis[i, :] = nodes
            self.omegas[i, :] = weights

        # Precompute the binomial coefficients
        self.binom = np.zeros((2 * self.p + 1, 2 * self.p + 1), dtype=np.int)
        for i in range(2 * self.p + 1):
            for j in range(2 * self.p + 1):
                self.binom[i, j] = int(special.binom(i, j))

        # self.b1, self.b2, self.b3, self.b4 = self.generate_bary()
        self.b4, self.b1, self.b2, self.b3 = self.generate_bary()
        self.conn, self.tid, self.offsets = self.make_conn()
        self.dofs = self.binom[self.p + 3, 3]

    def assemble_matrix(self, mass_func=default_mass, stiff_func=default_stiff, force_func=default_load):
        """
        Generates the stiffness and mass matrix
        :return:
        """
        # Make matrices
        force_raw = np.zeros((self.eldof.max() + 1,))

        # Helper arrs for COO-matrix maker
        mass_is = np.zeros((self.elements * self.dofs_per_element ** 2), dtype=np.int)
        mass_js = np.zeros((self.elements * self.dofs_per_element ** 2), dtype=np.int)
        mass_ds = np.zeros((self.elements * self.dofs_per_element ** 2))

        stiff_is = np.zeros((self.elements * self.dofs_per_element ** 2), dtype=np.int)
        stiff_js = np.zeros((self.elements * self.dofs_per_element ** 2), dtype=np.int)
        stiff_ds = np.zeros((self.elements * self.dofs_per_element ** 2))

        # Parameters to see if mass, stiff, and force are linear/nonlinear functions
        linear = False
        if mass_func == default_mass and stiff_func == default_stiff and force_func == default_load:
            linear = True

        # Loop through elements
        vertices = np.zeros((3, 4))
        m_quad = np.zeros((self.q, self.q, self.q))
        s_quad = np.zeros((3, 3, self.q, self.q, self.q))
        f_quad = np.zeros((self.q, self.q, self.q))
        mmts_stiff = np.zeros((2 * self.p - 1, 2 * self.p - 1, 2 * self.p - 1, 3, 3))

        # Preallocate some matrices
        mukl = np.zeros((2 * self.p - 1, 2 * self.p - 1, 2 * self.p - 1, 4, 4))
        elem_stiff = np.zeros((self.binom[self.p + 3, 3], self.binom[self.p + 3, 3]))
        elem_mass = np.zeros((self.binom[self.p + 3, 3], self.binom[self.p + 3, 3]))
        for element in range(self.elements):
            vertices[:, 0] = self.coordinates[self.elnode[element, 0]]
            vertices[:, 1] = self.coordinates[self.elnode[element, 1]]
            vertices[:, 2] = self.coordinates[self.elnode[element, 2]]
            vertices[:, 3] = self.coordinates[self.elnode[element, 3]]

            # Compute some quantities on the tetrahedron
            normal = gradient_barycentric(vertices[:, 0], vertices[:, 1], vertices[:, 2], vertices[:, 3])
            vol, _, _, _, _ = volume(vertices[:, 0], vertices[:, 1], vertices[:, 2], vertices[:, 3])
            # print(vol)
            # Evaluate mass, stiffness and load functions
            if not linear:
                for i in range(self.q):
                    for j in range(self.q):
                        for k in range(self.q):
                            coordinates = np.array([self.xis[2, i], self.xis[1, j], self.xis[0, k]])
                            m_quad[i, j, k] = mass_func(duffy(coordinates, vertices))
                            s_quad[:, :, i, j, k] = stiff_func(duffy(coordinates, vertices))
                            f_quad[i, j, k] = force_func(duffy(coordinates, vertices))
            else:
                m_quad.fill(1)
                f_quad.fill(1)
                for i in range(self.q):
                    for j in range(self.q):
                        for k in range(self.q):
                            s_quad[:, :, i, j, k] = np.eye(3)

            # Create element matrices
            mmts = moment(m_quad, self.q, 2 * self.p, self.xis, self.omegas)
            mass(self.p, mmts, self.binom, elem_mass)
            mk = 6 * vol * elem_mass

            # Stiffness element matrices
            for i in range(3):
                for j in range(3):
                    # print(s_quad[:, :, :, i, j].flags, self.xis.flags, self.omegas.flags)
                    mmts_stiff[:, :, :, i, j] = moment(s_quad[i, j, :, :, :], self.q, 2 * self.p - 2, self.xis,
                                                       self.omegas)
            stiffness(self.p, mmts_stiff, self.binom, np.array(normal), mukl, elem_stiff)
            sk = 6 * vol * elem_stiff

            fk = 6 * vol * moment(f_quad, self.q, self.p, self.xis, self.omegas)
            counter = 0
            for i in range(self.dofs_per_element):
                iglobal = self.eldof[element, i]
                i_dof = self.lookup_dof[i]
                force_raw[iglobal] += fk[i_dof[0:3]]

                for j in range(self.dofs_per_element):
                    jglobal = self.eldof[element, j]
                    j_dof = self.lookup_dof[j]

                    mass_is[element * (self.dofs_per_element ** 2) + counter] = iglobal
                    mass_js[element * (self.dofs_per_element ** 2) + counter] = jglobal
                    mass_ds[element * (self.dofs_per_element ** 2) + counter] = mk[
                        pos3d(*i_dof[0:3], self.p), pos3d(*j_dof[0:3], self.p)]

                    stiff_is[element * (self.dofs_per_element ** 2) + counter] = iglobal
                    stiff_js[element * (self.dofs_per_element ** 2) + counter] = jglobal
                    stiff_ds[element * (self.dofs_per_element ** 2) + counter] = sk[
                        pos3d(*i_dof[0:3], self.p), pos3d(*j_dof[0:3], self.p)]

                    # mass_raw[iglobal, jglobal] += mk[pos3d(*i_dof[0:3], self.p), pos3d(*j_dof[0:3], self.p)]
                    # stiffness_raw[iglobal, jglobal] += sk[pos3d(*i_dof[0:3], self.p), pos3d(*j_dof[0:3], self.p)]

                    counter += 1

        # Depopulate the dirichlet boundary conditions
        mass_raw = lil_matrix(coo_matrix((mass_ds, (mass_is, mass_js))))
        stiffness_raw = lil_matrix(coo_matrix((stiff_ds, (stiff_is, stiff_js))))
        for i in self.dirchlet_dofs:
            stiffness_raw[:, i] = 0
            stiffness_raw[i, :] = 0
            stiffness_raw[i, i] = 1

            mass_raw[:, i] = 0
            mass_raw[i, :] = 0
            mass_raw[i, i] = 1

            force_raw[i] = 0

        return csr_matrix(mass_raw), csr_matrix(stiffness_raw), force_raw

    def assemble_convection(self, convect_func=default_convec):
        """
        Generates the convection
        :return:
        """
        # Make matrices

        convect_is = np.zeros((self.elements * self.dofs_per_element ** 2), dtype=np.int)
        convect_js = np.zeros((self.elements * self.dofs_per_element ** 2), dtype=np.int)
        convect_ds = np.zeros((self.elements * self.dofs_per_element ** 2))

        # Parameters to see if linear/nonlinear functions
        linear = False
        if convect_func == default_convec:
            linear = True

        # Loop through elements
        vertices = np.zeros((3, 4))
        c_quad = np.zeros((3, self.q, self.q, self.q))
        mmts_convect = np.zeros((2 * self.p, 2 * self.p, 2 * self.p, 3))

        # Preallocate some matrices
        mukl = np.zeros((2 * self.p, 2 * self.p, 2 * self.p, 4))
        elem_convect = np.zeros((self.binom[self.p + 3, 3], self.binom[self.p + 3, 3]))
        for element in range(self.elements):
            vertices[:, 0] = self.coordinates[self.elnode[element, 0]]
            vertices[:, 1] = self.coordinates[self.elnode[element, 1]]
            vertices[:, 2] = self.coordinates[self.elnode[element, 2]]
            vertices[:, 3] = self.coordinates[self.elnode[element, 3]]

            # Compute some quantities on the tetrahedron
            normal = gradient_barycentric(vertices[:, 0], vertices[:, 1], vertices[:, 2], vertices[:, 3])
            vol, _, _, _, _ = volume(vertices[:, 0], vertices[:, 1], vertices[:, 2], vertices[:, 3])
            # print(vol)

            # Evaluate mass, stiffness and load functions
            if not linear:
                for i in range(self.q):
                    for j in range(self.q):
                        for k in range(self.q):
                            coordinates = np.array([self.xis[2, i], self.xis[1, j], self.xis[0, k]])
                            c_quad[:, i, j, k] = convect_func(duffy(coordinates, vertices))
            else:
                c_quad.fill(1)
                # for i in range(self.q):
                #     for j in range(self.q):
                #         for k in range(self.q):
                #             c_quad[:, i, j, k] = np.array([1, 1, 1])

            # Convect element matrices
            for i in range(3):
                mmts_convect[:, :, :, i] = moment(c_quad[i, :, :, :], self.q, 2 * self.p - 1, self.xis,
                                                       self.omegas)

            # print(mmts_convect.shape)
            # print(mukl.shape)
            convection(self.p, mmts_convect, self.binom, np.array(normal), mukl, elem_convect)
            ck = 6 * vol * elem_convect

            counter = 0
            for i in range(self.dofs_per_element):
                iglobal = self.eldof[element, i]
                i_dof = self.lookup_dof[i]

                for j in range(self.dofs_per_element):
                    jglobal = self.eldof[element, j]
                    j_dof = self.lookup_dof[j]

                    convect_is[element * (self.dofs_per_element ** 2) + counter] = iglobal
                    convect_js[element * (self.dofs_per_element ** 2) + counter] = jglobal
                    convect_ds[element * (self.dofs_per_element ** 2) + counter] = ck[
                        pos3d(*i_dof[0:3], self.p), pos3d(*j_dof[0:3], self.p)]

                    counter += 1

        # Depopulate the dirichlet boundary conditions
        convect_raw = lil_matrix(coo_matrix((convect_ds, (convect_is, convect_js))))
        for i in self.dirchlet_dofs:
            convect_raw[:, i] = 0
            convect_raw[i, :] = 0
            convect_raw[i, i] = 0

        return csr_matrix(convect_raw)

    def l2_projection(self, func: Callable):
        """
        Given a function, gives the load vector

        :param func: function to find projection to
        :return:
        """
        u = np.zeros((self.eldof.max() + 1))
        vertices = np.zeros((3, 4))
        f_quad = np.zeros((self.q, self.q, self.q))
        for element in range(self.elements):
            vertices[:, 0] = self.coordinates[self.elnode[element, 0]]
            vertices[:, 1] = self.coordinates[self.elnode[element, 1]]
            vertices[:, 2] = self.coordinates[self.elnode[element, 2]]
            vertices[:, 3] = self.coordinates[self.elnode[element, 3]]

            vol, _, _, _, _ = volume(vertices[:, 0], vertices[:, 1], vertices[:, 2], vertices[:, 3])

            # Evaluate mass, stiffness and load functions
            for i in range(self.q):
                for j in range(self.q):
                    for k in range(self.q):
                        coordinates = np.array([self.xis[2, i], self.xis[1, j], self.xis[0, k]])
                        f_quad[i, j, k] = func(duffy(coordinates, vertices))

            fk = 6 * vol * moment(f_quad, self.q, self.p, self.xis, self.omegas)
            for i in range(self.dofs_per_element):
                u[self.eldof[element, i]] += fk[self.lookup_dof[i][0:3]]

        return u

    def decast(self, lambdas, values, elem) -> float:
        """
        Applies de Casteljau algorithm to the elemth element

        :param lambdas:
        :return:
        """
        # Get the control points from the element
        cp = self.element_to_control(values, elem)

        vertices = np.zeros((3, 4))
        vertices[:, 0] = self.coordinates[self.elnode[elem, 0]]
        vertices[:, 1] = self.coordinates[self.elnode[elem, 1]]
        vertices[:, 2] = self.coordinates[self.elnode[elem, 2]]
        vertices[:, 3] = self.coordinates[self.elnode[elem, 3]]
        # print(vertices, solution(np.sum(vertices, axis=1)/4))

        # print(cp, end='\n\n\n')

        for order in range(self.p, 0, -1):  # Iterate p times
            for i in range(order):  # How many levels there are at the orderth
                for j in range(order - i):  # Start from top of pyramid
                    for k in range(order - i - j):
                        cp[i, j, k] = lambdas[3] * cp[i, j, k] + lambdas[0] * cp[i + 1, j, k] + \
                                      lambdas[1] * cp[i, j + 1, k] + lambdas[2] * cp[i, j, k + 1]
            # print(cp)
        return cp[0, 0, 0]

    def element_to_control(self, values: np.ndarray, elem: int) -> np.ndarray:
        """
        TODO: Make more efficient

        :param values: 1D vector such as force vector or solution vector associated with this FEM
        :param elem: the element to output the control points
        :return: the control points in a 3D array
        """
        cp = np.zeros((self.p + 1,) * 3)
        for i in range(len(self.eldof[elem])):
            cp[self.lookup_dof[i][0:3]] = values[self.eldof[elem][i]]

        return cp

    def element_to_control_boundary(self, values: np.ndarray, elem: int):
        """
        Given values, places

        :param valesu:
        :param elem:
        :return:
        """
        fp1 = np.zeros((self.p + 1,) * 2)
        fp2 = np.zeros((self.p + 1,) * 2)
        fp3 = np.zeros((self.p + 1,) * 2)
        fp4 = np.zeros((self.p + 1,) * 2)

        for i in range(self.p + 1):
            for j in range(self.p + 1 - i):
                k = self.p - i - j
                fp1[i, j] = values[self.eldof[elem][self.lookup_multi[(i, j, k, 0)]]]
                fp2[i, j] = values[self.eldof[elem][self.lookup_multi[(i, j, 0, k)]]]
                fp3[i, j] = values[self.eldof[elem][self.lookup_multi[(i, 0, j, k)]]]
                fp4[i, j] = values[self.eldof[elem][self.lookup_multi[(0, i, j, k)]]]

        coords1 = self.coordinates[self.elnode[elem][[2, 1, 0]]]
        coords2 = self.coordinates[self.elnode[elem][[3, 1, 0]]]
        coords3 = self.coordinates[self.elnode[elem][[3, 2, 0]]]
        coords4 = self.coordinates[self.elnode[elem][[3, 2, 1]]]
        return (fp1, coords1), (fp2, coords2), (fp3, coords3), (fp4, coords4)

    def decast(self, lambdas: list, ctrl: np.ndarray) -> (np.ndarray, np.ndarray, np.ndarray):
        """
        de Castlejau algorithm returns control nets to the three subdivided triangles.

        Triangle ABC is oriented such that A is [0,0], B, is [1, 0], C is [0, 1].
        Call the barycentric coordinate L.

        They way we store the indices allow us to have triangle LBC easily; just need to store ALC, ALB.

        :param ctrl: control point in 2d array form (NOTE: THIS IS OVERWRITTEN)
        :param lambdas: barycnetric coordinates
        :return: 4 Numpy arrays
        """
        degree = self.p

        # Storage for two other triangles
        alc = np.zeros_like(ctrl)
        abl = np.zeros_like(ctrl)

        alc[:, 0] = ctrl[:, 0]
        abl[0, :] = ctrl[0, :]

        # Iterate p times
        for r in range(degree):
            for order in range(degree - r):
                for i in range(order + 1):
                    ctrl[i, order - i] = lambdas[2] * ctrl[i, order - i] + lambdas[0] * ctrl[i + 1, order - i] + \
                                         lambdas[1] * ctrl[i, order - i + 1]
            alc[0:degree - r, r + 1] = ctrl[0:degree - r, 0]
            abl[r + 1, 0:degree - r] = ctrl[0, 0:degree - r]

        return ctrl, abl, alc

    def subdivide_2d_gallier(self, ctrl: np.ndarray, abscissa: np.ndarray):
        """
        Subdivides a triangle in an efficient manner into 4 triangle; follows the algorithm in Jean Gallier's book

        :param ctrl: control points
        :param abscissa: coordiantes corresponding to the current control points
        :return: a tuple of 4 triangles
        """

        # Step 1: Initial decast algorithm
        _, abr, arc = self.decast([.5, .5, 0], ctrl)

        # Step 2: calculate TBR
        tbr, _, _ = self.decast([0, .5, .5], abr)

        # Step 3: Calculate SRC:
        src, ars, _ = self.decast([.5, 0, .5], arc)

        # Step 4: Calculate the two remaining triangles
        trs, _, ats = self.decast([-1, 1, 1], ars)

        # Step 5: Give correct vertices data
        a_v = abscissa[0]
        b_v = abscissa[1]
        c_v = abscissa[2]
        t_v = (a_v + b_v) / 2
        r_v = (b_v + c_v) / 2
        s_v = (a_v + c_v) / 2

        return (ats, [a_v, t_v, s_v]), (trs, [t_v, r_v, s_v]), (tbr, [t_v, b_v, r_v]), (src, [s_v, r_v, c_v])

    def plot_boundaries(self, u: np.ndarray, name = None):
        """
        Crude method to plot the boundaries of a solution

        :param u:
        :return:
        """
        triangles = list()

        for elem in range(self.elements):
            triangles.extend(self.element_to_control_boundary(u, elem))

        subdivision = 2
        for s in range(subdivision):
            holder = list()
            for t in triangles:
                holder.extend(self.subdivide_2d_gallier(t[0], t[1]))

            triangles = holder

        all_xs = list()
        all_ys = list()
        all_zs = list()
        all_us = list()
        all_conn = list()

        element_connectivity = self.return_triangulation()
        trianglebary1, trianglebary2, trianglebary3 = self.generate_bary_triangle()
        for t in triangles:
            xs1, ys1, zs1, us1 = self.plot_triangle(t[0], t[1], trianglebary1, trianglebary2, trianglebary3)

            # if np.allclose(zs1, np.zeros_like(zs1)):
            all_conn.extend(element_connectivity + len(all_xs)) # Extend connectivity first
            all_xs.extend(xs1)
            all_ys.extend(ys1)
            all_zs.extend(zs1)
            all_us.extend(us1)

        offset = np.arange(1, len(all_conn) + 1, dtype=int) * 3
        ctype = np.ones(len(all_conn)) * VtkTriangle.tid

        if name is None:
            unstructuredGridToVTK('plot_surface', np.array(all_xs), np.array(all_ys),
                              np.array(all_zs),
                              connectivity=np.concatenate(all_conn), offsets=offset, cell_types=ctype,
                              cellData=None, pointData={'values': np.array(all_us)})
        else:
            unstructuredGridToVTK(name, np.array(all_xs), np.array(all_ys),
                              np.array(all_zs),
                              connectivity=np.concatenate(all_conn), offsets=offset, cell_types=ctype,
                              cellData=None, pointData={'values': np.array(all_us)})

    def generate_bary_triangle(self) -> (np.ndarray, np.ndarray, np.ndarray):
        """
        Generates the 3 barycentric matrices

        :return: three matrices corresponding to the barycentric coordinates in subdivide_2d
        """
        # Generate some barycentric coordinates
        lambda_1 = np.zeros((self.p + 1, self.p + 1))
        lambda_2 = np.zeros((self.p + 1, self.p + 1))
        lambda_3 = np.zeros((self.p + 1, self.p + 1))

        for i in range(self.p + 1):
            # Because I'm lazy, and numpy is actually stupid for not having off diagonals
            for j in range(self.p - i + 1):
                lambda_1[j, self.p - i - j] = i / self.p
            lambda_2[0:self.p - i + 1, i] = i / self.p
            lambda_3[i, 0: self.p - i + 1] = i / self.p

        return lambda_1, lambda_2, lambda_3

    @staticmethod
    def plot_triangle(ctrl: np.ndarray, vertices: np.ndarray, bary1, bary2, bary3):
        """
        Returns 1d arrays of x, y, z for trisurf plotting

        :param bary3: Third barycentric coordinates
        :param bary2: Second barycentric coordinates
        :param bary1: First barycentric coordinates
        :param ctrl: control points
        :param vertices: self-explanatory
        :return:
        """
        p = ctrl.shape[0] - 1

        xs = vertices[0][0] * bary1 + vertices[1][0] * bary2 + vertices[2][0] * bary3
        ys = vertices[0][1] * bary1 + vertices[1][1] * bary2 + vertices[2][1] * bary3
        zs = vertices[0][2] * bary1 + vertices[1][2] * bary2 + vertices[2][2] * bary3

        return np.rot90(xs, -1)[np.triu_indices(p + 1)], np.rot90(ys, -1)[np.triu_indices(p + 1)], \
               np.rot90(zs, -1)[np.triu_indices(p + 1)], np.rot90(ctrl, -1)[np.triu_indices(p + 1)]

    def return_triangulation(self):
        out = list()

        # Create a 2D matrix for helping (only need to run this once, so it shouldn't be bad)
        helper = np.zeros((self.p + 1, self.p + 1), dtype=int)
        helper[np.triu_indices(self.p + 1)] = range(int((self.p + 1) * (self.p + 2) / 2))

        # Now create top triangles
        for i in range(self.p):
            for j in range(self.p - i):
                out.append((helper[i, j + i], helper[i, j + i + 1], helper[i + 1, j + i + 1]))

        # Now the more awkward bottom triangles (Can combine with above but for readability)
        for i in range(self.p - 1):
            for j in range(self.p - 1 - i):
                out.append((helper[i, j + i + 1], helper[i + 1, j + i + 1], helper[i + 1, j + i + 2]))

        return np.array(out)

    def plot(self, u: np.ndarray):
        tetrahedrons = list()

        for elem in range(self.elements):
            tetrahedrons.append(tuple((self.element_to_control(u, elem), self.coordinates[self.elnode[elem]])))

        all_xs = np.zeros((self.elements * self.dofs))
        all_ys = np.zeros((self.elements * self.dofs))
        all_zs = np.zeros((self.elements * self.dofs))
        all_us = np.zeros((self.elements * self.dofs))
        all_connectivity = list()
        all_offset = list()

        counter = 0
        for tet in tetrahedrons:
            # Need to extract x, y, z from tetrahedron
            # xs, ys, zs, us = self.plot_tetrahedron(tet[0], tet[1], self.b1, self.b2, self.b3, self.b4)
            xs, ys, zs, us = self.plot_tetrahedron_naive(tet[0], tet[1])

            all_offset.extend(self.offsets + len(all_connectivity))
            all_connectivity.extend(self.conn + counter * self.dofs)

            all_xs[self.dofs * counter: self.dofs * (counter + 1)] = (xs)
            all_ys[self.dofs * counter: self.dofs * (counter + 1)] = (ys)
            all_zs[self.dofs * counter: self.dofs * (counter + 1)] = (zs)
            all_us[self.dofs * counter: self.dofs * (counter + 1)] = (us)

            counter += 1

        all_types = np.tile(self.tid, self.elements)  # Tids doesn't need any incrementing

        unstructuredGridToVTK('convec', all_xs, all_ys, all_zs,
                              connectivity=np.array(all_connectivity), offsets=np.array(all_offset),
                              cell_types=np.array(all_types), pointData={'u': all_us})

    def plot_tetrahedron(self, ctrl: np.ndarray, vertices: np.ndarray, bary1, bary2, bary3, bary4):
        xs = np.zeros(self.binom[self.p + 3, 3])
        ys = np.zeros(self.binom[self.p + 3, 3])
        zs = np.zeros(self.binom[self.p + 3, 3])
        us = np.zeros(self.binom[self.p + 3, 3])

        ind = 0
        for i in range(self.p + 1):
            for j in range(self.p - i + 1):
                for k in range(self.p - i - j + 1):
                    xs[ind] = bary1[i, j, k] * vertices[0][0] + bary2[i, j, k] * vertices[1][0] + bary3[i, j, k] * \
                              vertices[2][0] + bary4[i, j, k] * vertices[3][0]
                    ys[ind] = bary1[i, j, k] * vertices[0][1] + bary2[i, j, k] * vertices[1][1] + bary3[i, j, k] * \
                              vertices[2][1] + bary4[i, j, k] * vertices[3][1]
                    zs[ind] = bary1[i, j, k] * vertices[0][2] + bary2[i, j, k] * vertices[1][2] + bary3[i, j, k] * \
                              vertices[2][2] + bary4[i, j, k] * vertices[3][2]
                    us[ind] = ctrl[i, j, k]  # TODO: make simple plotting
                    # us[ind] = self.decast_element((bary1[i,j,k], bary3[i,j,k], bary2[i,j,k], bary4[i,j,k]), ctrl, vertices)
                    # print(bary1[i,j,k], bary2[i,j,k], bary3[i,j,k], bary4[i,j,k], ctrl[i,j,k])
                    # print()
                    ind += 1

        return xs, ys, zs, us

    def plot_tetrahedron_naive(self, ctrl, vertices):
        spacing = self.p

        xs = np.zeros(int(special.binom(spacing + 3, 3)))
        ys = np.zeros(int(special.binom(spacing + 3, 3)))
        zs = np.zeros(int(special.binom(spacing + 3, 3)))
        us = np.zeros(int(special.binom(spacing + 3, 3)))

        ind = 0
        for i in range(spacing + 1):
            for j in range(spacing + 1 - i):
                for k in range(spacing + 1 - i - j):
                    bary1 = i / spacing
                    bary2 = j / spacing
                    bary3 = k / spacing
                    bary4 = 1 - bary1 - bary2 - bary3

                    xs[ind] = bary1 * vertices[0][0] + bary2 * vertices[1][0] + bary3 * vertices[2][0] + bary4 * \
                              vertices[3][0]
                    ys[ind] = bary1 * vertices[0][1] + bary2 * vertices[1][1] + bary3 * vertices[2][1] + bary4 * \
                              vertices[3][1]
                    zs[ind] = bary1 * vertices[0][2] + bary2 * vertices[1][2] + bary3 * vertices[2][2] + bary4 * \
                              vertices[3][2]

                    copy = np.copy(ctrl)  # Needed because i'm dumb
                    us[ind] = self.decast_element((bary1, bary2, bary3, bary4), copy)

                    ind += 1

        return xs, ys, zs, us

    def decast_element(self, lambdas, cp) -> float:
        """
        Applies de Casteljau algorithm to the elemth element

        :param lambdas:
        :return:
        """
        # Get the control points from the element
        # cp = self.element_to_control(values, elem)

        # vertices = np.zeros((3, 4))
        # vertices[:, 0] = self.coordinates[self.elnode[elem, 0]]
        # vertices[:, 1] = self.coordinates[self.elnode[elem, 1]]
        # vertices[:, 2] = self.coordinates[self.elnode[elem, 2]]
        # vertices[:, 3] = self.coordinates[self.elnode[elem, 3]]
        # print(vertices, solution(np.sum(vertices, axis=1)/4))

        # print(cp, end='\n\n\n')

        for order in range(self.p, 0, -1):  # Iterate p times
            for i in range(order):  # How many levels there are at the orderth
                for j in range(order - i):  # Start from top of pyramid
                    for k in range(order - i - j):
                        cp[i, j, k] = lambdas[3] * cp[i, j, k] + lambdas[0] * cp[i + 1, j, k] + \
                                      lambdas[1] * cp[i, j + 1, k] + lambdas[2] * cp[i, j, k + 1]
            # print(cp)
        return cp[0, 0, 0]

    def generate_bary(self) -> (np.ndarray, np.ndarray, np.ndarray, np.ndarray):
        """
        Generates a net of coefficients for domain points; this can also be thought of as
        arrays of barycentric coordinates of the control points

        :return: 4 barycentric coordinates
        """

        lambda_1 = np.zeros((self.p + 1, self.p + 1, self.p + 1))
        lambda_2 = np.zeros((self.p + 1, self.p + 1, self.p + 1))
        lambda_3 = np.zeros((self.p + 1, self.p + 1, self.p + 1))
        lambda_4 = np.zeros((self.p + 1, self.p + 1, self.p + 1))

        for i in range(self.p + 1):
            for j in range(self.p - i + 1):
                for k in range(self.p - i - j + 1):
                    lambda_1[j, k, self.p - j - k - i] = i / self.p

                lambda_2[i, 0:self.p + 1 - j - i, j] = i / self.p
                lambda_3[0:self.p + 1 - j - i, i, j] = i / self.p
                lambda_4[0:self.p + 1 - j - i, j, i] = i / self.p

        return lambda_1, lambda_2, lambda_3, lambda_4

    def make_conn(self):
        # First create helper
        helper = np.zeros((self.p + 1, self.p + 1, self.p + 1), dtype=np.int)
        counter = 0
        for i in range(self.p + 1):
            for j in range(self.p - i + 1):
                for k in range(self.p - i - j + 1):
                    helper[i, j, k] = counter
                    counter += 1

        out = list()
        tids = list()
        offsets = list()
        # Now we loop through each level first, creating the "regular tetrahedrons"
        for i in range(self.p):
            # Loop through each triangle on the level
            for j in range(self.p - i):
                for k in range(self.p - i - j):
                    out.extend((helper[i, j, k], helper[i, j + 1, k], helper[i, j, k + 1], helper[i + 1, j, k]))
                    tids.append(VtkTetra.tid)
                    offsets.append(len(out))

        # Now we add the "inverted" triangle parts, there are less levels, which is just one octahedron
        for i in range(self.p - 1):
            # Now go through the triangles in the smaller levels
            for j in range(self.p - i - 1):
                for k in range(self.p - 1 - i - j):
                    out.extend((helper[i, j + 1, k], helper[i, j + 1, k + 1], helper[i + 1, j, k + 1],
                                helper[i + 1, j, k], helper[i + 1, j + 1, k]))
                    tids.append(VtkPyramid.tid)
                    offsets.append(len(out))

                    out.extend((helper[i, j + 1, k], helper[i, j + 1, k + 1], helper[i + 1, j, k + 1],
                                helper[i + 1, j, k], helper[i, j, k + 1]))
                    tids.append(VtkPyramid.tid)
                    offsets.append(len(out))

        # Finally, need to add one final tet for the interiors
        for i in range(self.p - 2):
            for j in range(self.p - i - 2):
                for k in range(self.p - 2 - i - j):
                    out.extend((helper[i, j + 1, k + 1], helper[i + 1, j, k + 1], helper[i + 1, j + 1, k],
                                helper[i + 1, j + 1, k + 1]))
                    tids.append(VtkTetra.tid)
                    offsets.append(len(out))

        return np.array(out), np.array(tids), np.array(offsets)


if __name__ == '__main__':
    import read_msh
    from scipy.sparse.linalg import spsolve

    np.set_printoptions(linewidth=140)


    def force(x):
        return 100


    coords_oneelement = np.array([[0, 0, 0], [0, 0, 1], [0, 1, 0], [1, 0, 0]])
    elnode_oneelement = np.array([[0, 1, 2, 3]])
    faces = set([(0, 1, 2), (0, 1, 3), (0, 2, 3), (1, 2, 3)])
    lines = set()
    for face in faces:
        lines.add(tuple(sorted((face[0], face[1]))))
        lines.add(tuple(sorted((face[0], face[2]))))
        lines.add(tuple(sorted((face[1], face[2]))))
    bc_oneelement = {'faces': faces, 'lines': lines}
    m = BernsteinMesh(coords_oneelement, elnode_oneelement, {'faces': set(), 'lines': set()}, 3)

    coords_twoelement = np.array([[0, 0, 0], [0, 0, 1], [0, 1, 0], [1, 0, 0], [1, 1, 1]])
    elnode_twoelement = np.array([[0, 1, 2, 3], [1, 2, 3, 4]])
    faces = set([(0, 1, 2), (0, 1, 3), (0, 2, 3), (1, 2, 4), (1, 3, 4), (2, 3, 4)])
    lines = set()
    for face in faces:
        lines.add(tuple(sorted((face[0], face[1]))))
        lines.add(tuple(sorted((face[0], face[2]))))
        lines.add(tuple(sorted((face[1], face[2]))))
    bc_twoelement = {'faces': faces, 'lines': lines}
    # m = BernsteinMesh(coords_twoelement, elnode_twoelement, {'faces': set(), 'lines' : set()}, 3)

    vertices, elements, bc = read_msh.load_file('../cube.geo', refine=0)
    m = BernsteinMesh(vertices, elements, bc, 6)

    fem = BernsteinFEM(m)
    m, s, f = fem.assemble_matrix(force_func=force)
    alphas = spsolve(s, f)
    fem.plot(alphas)

    # x = np.array([0, .5, 1, 0, .5, 0, 0, .5, 0, 0])
    # y = np.array([0, 0, 0, .5, .5, 1, 0, 0, .5, 0])
    # z = np.array([0, 0, 0, 0, 0, 0, .5, .5, .5, 1])
    # conn = np.array([0, 1, 3, 6, 1, 2, 4, 7, 3, 4, 5, 8, 6,7,8,9, 1,3,4,6, 1,6,7,4, 3,4,7,6,8])
    # offsets = np.array([4,8,12,16,20, 24, 29])
    # types = np.array([VtkTetra.tid,VtkTetra.tid,VtkTetra.tid,VtkTetra.tid, VtkTetra.tid, VtkTetra.tid, VtkPyramid.tid])
    #
    # conn = np.array([3,4,7,6,8, 3,4,7,6,1])
    # offsets = np.array([5, 10])
    # types = np.array([VtkPyramid.tid, VtkPyramid.tid])
    # # conn = np.array([3,6,7,4,8])
    # # offsets = np.array([5])
    # # types = np.array([VtkPyramid.tid])
    # # unstructuredGridToVTK("test_2", x, y, z, conn, offsets, types,pointData={'test': 5 *np.cosh(y + z)})
    # unstructuredGridToVTK("glue", x, y, z, conn, offsets, types,pointData={'test': 5 *np.cosh(y + z)})
    # unstructuredGridToVTK("test", np.array([0, 1.0, 0, 0]), np.array([0, 0, 1.0, 0]), np.array([0, 0, 0, 1.0]),
    #                       np.array([0, 1, 2, 3]), offsets=np.array([4]),
    #                       cell_types=np.array([VtkTetra.tid]), pointData={'test': np.array([1, 2, 3, 4])})
