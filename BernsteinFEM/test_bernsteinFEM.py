from unittest import TestCase


class TestBernsteinFEM(TestCase):
    def test_mass_matrix(self):
        import read_msh
        from bernstein_mesh import BernsteinMesh
        from BernsteinFEM import BernsteinFEM
        from scipy.sparse.linalg import spsolve
        import numpy as np

        def solution(x):
            return x[0] ** 4 + x[1] ** 2 + x[2] ** 5 + x[0] * x[1] * x[2] * x[2] + 5 * x[1] * x[2] * x[0] ** 4 + x[
                0] ** 6 + 5

        vertices, elements, _ = read_msh.load_file('../cube.geo')
        m = BernsteinMesh(vertices, elements, {'faces': set(), 'lines': set()}, 6)

        fem = BernsteinFEM.BernsteinFEM(m)
        m, s, f = fem.assemble_matrix(force_func=solution)
        alphas = spsolve(m, f)

        for i in range(fem.elements):
            vertex = np.zeros((3, 4))
            vertex[:, 0] = fem.coordinates[fem.elnode[i, 0]]
            vertex[:, 1] = fem.coordinates[fem.elnode[i, 1]]
            vertex[:, 2] = fem.coordinates[fem.elnode[i, 2]]
            vertex[:, 3] = fem.coordinates[fem.elnode[i, 3]]
            centroid = np.sum(vertex, axis=1) / 4
            face1 = (vertex[:, 0] + vertex[:, 1] + vertex[:, 2]) / 3
            face2 = (vertex[:, 0] + vertex[:, 1] + vertex[:, 3]) / 3
            face3 = (vertex[:, 0] + vertex[:, 2] + vertex[:, 3]) / 3
            face4 = (vertex[:, 1] + vertex[:, 2] + vertex[:, 3]) / 3
            np.testing.assert_almost_equal(solution(face1), fem.decast([1 / 3.0, 1 / 3.0, 1 / 3.0, 0], alphas, i))
            np.testing.assert_almost_equal(solution(face2), fem.decast([1 / 3.0, 1 / 3.0, 0, 1 / 3.0], alphas, i))
            np.testing.assert_almost_equal(solution(face3), fem.decast([1 / 3.0, 0, 1 / 3.0, 1 / 3.0], alphas, i))
            np.testing.assert_almost_equal(solution(face4), fem.decast([0, 1 / 3.0, 1 / 3.0, 1 / 3.0], alphas, i))
            np.testing.assert_almost_equal(solution(vertex[:, 0]), fem.decast([1, 0, 0, 0], alphas, i))
            np.testing.assert_almost_equal(solution(vertex[:, 1]), fem.decast([0, 1, 0, 0], alphas, i))
            np.testing.assert_almost_equal(solution(vertex[:, 2]), fem.decast([0, 0, 1, 0], alphas, i))
            np.testing.assert_almost_equal(solution(vertex[:, 3]), fem.decast([0, 0, 0, 1], alphas, i))
            np.testing.assert_almost_equal(solution(centroid), fem.decast([.25, .25, .25, .25], alphas, i))

        print(
            "Testing complete. Projected a polynomial of degree 8 onto a cube. Evaluated at vertices of tet and centroid and faces.")

    def test_mass_matrix_with_boundary(self):
        import read_msh
        from bernstein_mesh import BernsteinMesh
        from BernsteinFEM import BernsteinFEM
        from scipy.sparse.linalg import spsolve
        import numpy as np

        def solution(x):
            return x[0]*(1-x[0])*x[1]*(1-x[1])*x[2]*(1-x[2])

        vertices, elements, boundary = read_msh.load_file('../cube.geo')
        m = BernsteinMesh(vertices, elements, boundary, 6)

        fem = BernsteinFEM.BernsteinFEM(m)
        m, s, f = fem.assemble_matrix(force_func=solution)
        alphas = spsolve(m, f)

        for i in range(fem.elements):
            vertex = np.zeros((3, 4))
            vertex[:, 0] = fem.coordinates[fem.elnode[i, 0]]
            vertex[:, 1] = fem.coordinates[fem.elnode[i, 1]]
            vertex[:, 2] = fem.coordinates[fem.elnode[i, 2]]
            vertex[:, 3] = fem.coordinates[fem.elnode[i, 3]]
            centroid = np.sum(vertex, axis=1) / 4
            arbit_point = vertex[:, 0] * .5 + vertex[:, 1] * .1 + vertex[:, 2] * .1 + vertex[:, 3] * .3
            np.testing.assert_almost_equal(solution(vertex[:, 0]), fem.decast([1, 0, 0, 0], alphas, i))
            np.testing.assert_almost_equal(solution(vertex[:, 1]), fem.decast([0, 1, 0, 0], alphas, i))
            np.testing.assert_almost_equal(solution(vertex[:, 2]), fem.decast([0, 0, 1, 0], alphas, i))
            np.testing.assert_almost_equal(solution(vertex[:, 3]), fem.decast([0, 0, 0, 1], alphas, i))
            np.testing.assert_almost_equal(solution(centroid), fem.decast([.25, .25, .25, .25], alphas, i))
            np.testing.assert_almost_equal(solution(arbit_point), fem.decast([.5, .1, .1, .3], alphas, i))

        print(
            "Testing complete. Projected a polynomial of degree 6 onto a cube with Dirichlet boundary condition. "
            "Evaluated at vertices of tet and centroid.")

    def test_stiffness_single(self):
        from bernstein_mesh import BernsteinMesh
        from BernsteinFEM import BernsteinFEM
        from scipy.sparse.linalg import spsolve
        import numpy as np

        def solution(x):
            return x[0] * x[1] * (1 - x[0] - x[1] - x[2]) * x[2]

        def force(x):
            return -(-2 * x[0] * x[1] - 2 * x[0] * x[2] - 2 * x[1] * x[2])

        coords_oneelement = np.array([[0, 0, 0], [0, 0, 1], [0, 1, 0], [1, 0, 0]])
        elnode_oneelement = np.array([[0, 1, 2, 3]])
        faces = set([(0, 1, 2), (0, 1, 3), (0, 2, 3), (1, 2, 3)])
        lines = set()
        for face in faces:
            lines.add(tuple(sorted((face[0], face[1]))))
            lines.add(tuple(sorted((face[0], face[2]))))
            lines.add(tuple(sorted((face[1], face[2]))))
        bc_oneelement = {'faces': faces, 'lines': lines}
        m = BernsteinMesh(coords_oneelement, elnode_oneelement, bc_oneelement, 8)

        fem = BernsteinFEM.BernsteinFEM(m)
        m, s, f = fem.assemble_matrix(force_func=force)

        alphas = spsolve(s, f)
        for i in range(fem.elements):
            vertex = np.zeros((3, 4))
            vertex[:, 0] = fem.coordinates[fem.elnode[i, 0]]
            vertex[:, 1] = fem.coordinates[fem.elnode[i, 1]]
            vertex[:, 2] = fem.coordinates[fem.elnode[i, 2]]
            vertex[:, 3] = fem.coordinates[fem.elnode[i, 3]]
            centroid = np.sum(vertex, axis=1) / 4
            arbit_point = vertex[:, 0] * .5 + vertex[:, 1] * .1 + vertex[:, 2] * .1 + vertex[:, 3] * .3
            np.testing.assert_almost_equal(solution(vertex[:, 0]), fem.decast([1, 0, 0, 0], alphas, i))
            np.testing.assert_almost_equal(solution(vertex[:, 1]), fem.decast([0, 1, 0, 0], alphas, i))
            np.testing.assert_almost_equal(solution(vertex[:, 2]), fem.decast([0, 0, 1, 0], alphas, i))
            np.testing.assert_almost_equal(solution(vertex[:, 3]), fem.decast([0, 0, 0, 1], alphas, i))
            np.testing.assert_almost_equal(solution(centroid), fem.decast([.25, .25, .25, .25], alphas, i))
            np.testing.assert_almost_equal(solution(arbit_point), fem.decast([.5, .1, .1, .3], alphas, i))

        print(
            "Testing complete. Solved poisson's problem on single element and evaluated at centroid and arbitrary point"
            "with a degree 8 polynomial")

    def test_stiffness_single_nonreference(self):
        from bernstein_mesh import BernsteinMesh
        from BernsteinFEM import BernsteinFEM
        from scipy.sparse.linalg import spsolve
        import numpy as np

        def solution(x):
            return x[1]*(1 - x[2])*(1-x[0])*(x[0] - x[1] + x[2] - 1)

        def force(x):
            return -(-2*(1 - x[0])*x[1] - 2*(1 - x[0])*(1 - x[2]) - 2*x[1]*(1 - x[2]))

        coords_oneelement = np.array([[1, 0, 0], [1, 0, 1], [0, 0, 1], [1, 1, 1]])
        elnode_oneelement = np.array([[0, 1, 2, 3]])
        faces = set([(0, 1, 2), (0, 1, 3), (0, 2, 3), (1, 2, 3)])
        lines = set()
        for face in faces:
            lines.add(tuple(sorted((face[0], face[1]))))
            lines.add(tuple(sorted((face[0], face[2]))))
            lines.add(tuple(sorted((face[1], face[2]))))
        bc_oneelement = {'faces': faces, 'lines': lines}
        m = BernsteinMesh(coords_oneelement, elnode_oneelement, bc_oneelement, 8)

        fem = BernsteinFEM.BernsteinFEM(m)
        m, s, f = fem.assemble_matrix(force_func=force)

        alphas = spsolve(s, f)
        for i in range(fem.elements):
            vertex = np.zeros((3, 4))
            vertex[:, 0] = fem.coordinates[fem.elnode[i, 0]]
            vertex[:, 1] = fem.coordinates[fem.elnode[i, 1]]
            vertex[:, 2] = fem.coordinates[fem.elnode[i, 2]]
            vertex[:, 3] = fem.coordinates[fem.elnode[i, 3]]
            centroid = np.sum(vertex, axis=1) / 4
            arbit_point = vertex[:, 0] * .5 + vertex[:, 1] * .1 + vertex[:, 2] * .1 + vertex[:, 3] * .3
            np.testing.assert_almost_equal(solution(vertex[:, 0]), fem.decast([1, 0, 0, 0], alphas, i))
            np.testing.assert_almost_equal(solution(vertex[:, 1]), fem.decast([0, 1, 0, 0], alphas, i))
            np.testing.assert_almost_equal(solution(vertex[:, 2]), fem.decast([0, 0, 1, 0], alphas, i))
            np.testing.assert_almost_equal(solution(vertex[:, 3]), fem.decast([0, 0, 0, 1], alphas, i))
            np.testing.assert_almost_equal(solution(centroid), fem.decast([.25, .25, .25, .25], alphas, i))
            np.testing.assert_almost_equal(solution(arbit_point), fem.decast([.5, .1, .1, .3], alphas, i))

        print(
            "Testing complete. Solved poisson's problem on single element (non-reference) and evaluated at centroid and arbitrary point"
            "with a degree 8 polynomial")

    def test_stiffness_two(self):
        from bernstein_mesh import BernsteinMesh
        from BernsteinFEM import BernsteinFEM
        from scipy.sparse.linalg import spsolve
        import numpy as np
        np.set_printoptions(precision=4, linewidth=140)
        def solution(x):
            return x[0]*x[1]*(1 + x[0] - x[1] - x[2])*(1 - x[0] + x[1] - x[2])*x[2]*(1 - x[0] - x[1] + x[2])/8

        def force(x):
            return -(1 / 4 * (-x[0] ** 3 * (x[1] + x[2]) - x[0] * (
                    x[1] - 2 * x[1] ** 2 + x[1] ** 3 + 9 * x[1] * x[2] + (-1 + x[2]) ** 2 * x[2]) +
                              2 * x[0] ** 2 * (x[1] + x[1] ** 2 + x[2] + x[2] ** 2) - x[1] * x[2] * (
                                      x[1] ** 2 + (-1 + x[2]) ** 2 - 2 * x[1] * (1 + x[2]))))

        coords_twoelement = np.array([[0, 0, 0], [0, 0, 1], [0, 1, 0], [1, 0, 0], [1, 1, 1]])
        elnode_twoelement = np.array([[0, 1, 2, 3], [1, 2, 3, 4]])
        faces = set([(0, 1, 2), (0, 1, 3), (0, 2, 3), (1, 2, 4), (1, 3, 4), (2, 3, 4)])
        lines = set()
        for face in faces:
            lines.add(tuple(sorted((face[0], face[1]))))
            lines.add(tuple(sorted((face[0], face[2]))))
            lines.add(tuple(sorted((face[1], face[2]))))
        bc_twoelement = {'faces': faces, 'lines': lines}
        m = BernsteinMesh(coords_twoelement, elnode_twoelement, bc_twoelement, 8)

        fem = BernsteinFEM.BernsteinFEM(m)
        m, s, f = fem.assemble_matrix(force_func=force)
        # print(s[110:131, 110:131].todense())
        alphas = spsolve(s, f)
        for i in range(fem.elements):
            vertex = np.zeros((3, 4))
            vertex[:, 0] = fem.coordinates[fem.elnode[i, 0]]
            vertex[:, 1] = fem.coordinates[fem.elnode[i, 1]]
            vertex[:, 2] = fem.coordinates[fem.elnode[i, 2]]
            vertex[:, 3] = fem.coordinates[fem.elnode[i, 3]]
            centroid = np.sum(vertex, axis=1) / 4
            arbit_point = vertex[:, 0] * .5 + vertex[:, 1] * .1 + vertex[:, 2] * .1 + vertex[:, 3] * .3
            np.testing.assert_almost_equal(solution(vertex[:, 0]), fem.decast([1, 0, 0, 0], alphas, i))
            np.testing.assert_almost_equal(solution(vertex[:, 1]), fem.decast([0, 1, 0, 0], alphas, i))
            np.testing.assert_almost_equal(solution(vertex[:, 2]), fem.decast([0, 0, 1, 0], alphas, i))
            np.testing.assert_almost_equal(solution(vertex[:, 3]), fem.decast([0, 0, 0, 1], alphas, i))
            np.testing.assert_almost_equal(solution(centroid), fem.decast([.25, .25, .25, .25], alphas, i))
            np.testing.assert_almost_equal(solution(arbit_point), fem.decast([.5, .1, .1, .3], alphas, i))

        print(
            "Testing complete. Solved poisson's problem on two elements and evaluated at centroid and arbitrary point"
            "with a degree 8 polynomial")

    def test_stiffnesss_three(self):
        from bernstein_mesh import BernsteinMesh
        from BernsteinFEM import BernsteinFEM
        from scipy.sparse.linalg import spsolve
        import numpy as np
        np.set_printoptions(precision=4, linewidth=140)

        def solution(x):
            return x[0]*x[1]*x[2]*(1-x[0])*(1-x[2])*(1 + x[0] - x[1] - x[2])*(1 - x[0] - x[1] + x[2])

        def force(x):
            return -(-2*(x[0]**4 * x[1] + x[0]**3 *(x[1] - 6* x[1]* x[2]) +
                x[1]*(-1 + x[2])*x[2]*(-1 + 2*x[1] - x[1]**2 + 2*x[2] + x[2]**2) -
                x[0]**2 *(-2* x[1]**2 + x[1]**3 - 2*(-1 + x[2])*x[2] + x[1]*(3 - 9*x[2]**2)) +
                x[0]*(-2*x[1]**2 + x[1]**3 - 2*(-1 + x[2])*x[2] + x[1]*(1 + 3*x[2] - 6*x[2]**3))))

        # Make sure my force and solution functions are correct; use mathematica
        np.testing.assert_allclose(solution([.1, .1, .1]),0.0006561000)
        np.testing.assert_allclose(solution([.8, .5, .2]),-0.001408)
        np.testing.assert_allclose(force([.1, .1, .1]),.05994)
        np.testing.assert_allclose(force([.3, .7, .2]),.11144)

        coords_threeelement = np.array([[0, 0, 0], [0, 0, 1], [0, 1, 0], [1, 0, 0], [1, 1, 1], [1, 0, 1]])
        elnode_threeelement = np.array([[0, 1, 2, 3], [1, 2, 3, 4], [1, 3, 4, 5]])
        faces = {(0, 1, 2), (0, 1, 3), (0, 2, 3), (1, 2, 4), (2, 3, 4), (1, 3, 5), (1, 4, 5), (3, 4, 5)}
        lines = set()
        for face in faces:
            lines.add(tuple(sorted((face[0], face[1]))))
            lines.add(tuple(sorted((face[0], face[2]))))
            lines.add(tuple(sorted((face[1], face[2]))))
        bc_threeelement = {'faces': faces, 'lines': lines}
        mesh = BernsteinMesh(coords_threeelement, elnode_threeelement, bc_threeelement, 8)

        fem = BernsteinFEM.BernsteinFEM(mesh)

        # First double check mesh by using mass matrix and boundary conditions
        m, s, f = fem.assemble_matrix(force_func=solution)
        # np.savetxt('three.csv', s.todense(), delimiter=',')
        alphas = spsolve(m, f)
        for i in range(fem.elements):
            vertex = np.zeros((3, 4))
            vertex[:, 0] = fem.coordinates[fem.elnode[i, 0]]
            vertex[:, 1] = fem.coordinates[fem.elnode[i, 1]]
            vertex[:, 2] = fem.coordinates[fem.elnode[i, 2]]
            vertex[:, 3] = fem.coordinates[fem.elnode[i, 3]]
            face1 = (vertex[:, 0] + vertex[:, 1] + vertex[:, 2])/3
            face2 = (vertex[:, 0] + vertex[:, 1] + vertex[:, 3])/3
            face3 = (vertex[:, 0] + vertex[:, 2] + vertex[:, 3])/3
            face4 = (vertex[:, 1] + vertex[:, 2] + vertex[:, 3])/3
            centroid = np.sum(vertex, axis=1) / 4
            arbit_point = vertex[:, 0] * .5 + vertex[:, 1] * .1 + vertex[:, 2] * .1 + vertex[:, 3] * .3
            np.testing.assert_almost_equal(solution(vertex[:, 0]), fem.decast([1, 0, 0, 0], alphas, i))
            np.testing.assert_almost_equal(solution(vertex[:, 1]), fem.decast([0, 1, 0, 0], alphas, i))
            np.testing.assert_almost_equal(solution(vertex[:, 2]), fem.decast([0, 0, 1, 0], alphas, i))
            np.testing.assert_almost_equal(solution(vertex[:, 3]), fem.decast([0, 0, 0, 1], alphas, i))
            np.testing.assert_almost_equal(solution(face1), fem.decast([1/3.0, 1/3.0, 1/3.0, 0], alphas, i))
            np.testing.assert_almost_equal(solution(face2), fem.decast([1/3.0, 1/3.0, 0, 1/3.0], alphas, i))
            np.testing.assert_almost_equal(solution(face3), fem.decast([1/3.0, 0, 1/3.0, 1/3.0], alphas, i))
            np.testing.assert_almost_equal(solution(face4), fem.decast([0, 1/3.0, 1/3.0, 1/3.0], alphas, i))
            np.testing.assert_almost_equal(solution(centroid), fem.decast([.25, .25, .25, .25], alphas, i))
            np.testing.assert_almost_equal(solution(arbit_point), fem.decast([.5, .1, .1, .3], alphas, i))

        print('Mesh checked using mass matrix')

        m, s, f = fem.assemble_matrix(force_func=force)

        alphas = spsolve(s, f)
        for i in range(fem.elements):
            vertex = np.zeros((3, 4))
            vertex[:, 0] = fem.coordinates[fem.elnode[i, 0]]
            vertex[:, 1] = fem.coordinates[fem.elnode[i, 1]]
            vertex[:, 2] = fem.coordinates[fem.elnode[i, 2]]
            vertex[:, 3] = fem.coordinates[fem.elnode[i, 3]]
            face1 = (vertex[:, 0] + vertex[:, 1] + vertex[:, 2]) / 3
            face2 = (vertex[:, 0] + vertex[:, 1] + vertex[:, 3]) / 3
            face3 = (vertex[:, 0] + vertex[:, 2] + vertex[:, 3]) / 3
            face4 = (vertex[:, 1] + vertex[:, 2] + vertex[:, 3]) / 3
            np.testing.assert_almost_equal(solution(face1), fem.decast([1 / 3.0, 1 / 3.0, 1 / 3.0, 0], alphas, i))
            np.testing.assert_almost_equal(solution(face2), fem.decast([1 / 3.0, 1 / 3.0, 0, 1 / 3.0], alphas, i))
            np.testing.assert_almost_equal(solution(face3), fem.decast([1 / 3.0, 0, 1 / 3.0, 1 / 3.0], alphas, i))
            np.testing.assert_almost_equal(solution(face4), fem.decast([0, 1 / 3.0, 1 / 3.0, 1 / 3.0], alphas, i))

            centroid = np.sum(vertex, axis=1) / 4
            arbit_point = vertex[:, 0] * .5 + vertex[:, 1] * .1 + vertex[:, 2] * .1 + vertex[:, 3] * .3
            np.testing.assert_almost_equal(solution(vertex[:, 0]), fem.decast([1, 0, 0, 0], alphas, i))
            np.testing.assert_almost_equal(solution(vertex[:, 1]), fem.decast([0, 1, 0, 0], alphas, i))
            np.testing.assert_almost_equal(solution(vertex[:, 2]), fem.decast([0, 0, 1, 0], alphas, i))
            np.testing.assert_almost_equal(solution(vertex[:, 3]), fem.decast([0, 0, 0, 1], alphas, i))
            np.testing.assert_almost_equal(solution(centroid), fem.decast([.25, .25, .25, .25], alphas, i))
            np.testing.assert_almost_equal(solution(arbit_point), fem.decast([.5, .1, .1, .3], alphas, i))

        print(
            "Testing complete. Solved poisson's problem on two elements and evaluated at centroid and arbitrary point"
            "with a degree 8 polynomial")

    def test_stiffness_cube(self):
        from bernstein_mesh import BernsteinMesh
        from BernsteinFEM import BernsteinFEM
        from scipy.sparse.linalg import spsolve
        import numpy as np
        import read_msh

        def solution(x):
            return x[0]*(1-x[0])*x[1]*(1 - x[1])*x[2]*(1 - x[2])

        def force(x):
            return -(-2 * (1 - x[0]) * x[0] * (1 - x[1]) * x[1] - 2 * (1 - x[0]) * x[0] * (1 - x[2]) * x[2] \
                     - 2 * (1 - x[1]) * x[1] * (1 - x[2]) * x[2])

        vertices, elements, boundary = read_msh.load_file('../cube.geo', refine=1)
        mesh = BernsteinMesh(vertices, elements, boundary, 6)

        fem = BernsteinFEM.BernsteinFEM(mesh)
        m, s, f = fem.assemble_matrix(force_func=force)
        alphas = spsolve(s, f)
        print(m.shape)

        for i in range(fem.elements):
            vertex = np.zeros((3, 4))
            vertex[:, 0] = fem.coordinates[fem.elnode[i, 0]]
            vertex[:, 1] = fem.coordinates[fem.elnode[i, 1]]
            vertex[:, 2] = fem.coordinates[fem.elnode[i, 2]]
            vertex[:, 3] = fem.coordinates[fem.elnode[i, 3]]
            centroid = np.sum(vertex, axis=1) / 4
            arbit_point = vertex[:, 0] * .5 + vertex[:, 1] * .1 + vertex[:, 2] * .1 + vertex[:, 3] * .3
            np.testing.assert_almost_equal(solution(vertex[:, 0]), fem.decast([1, 0, 0, 0], alphas, i))
            np.testing.assert_almost_equal(solution(vertex[:, 1]), fem.decast([0, 1, 0, 0], alphas, i))
            np.testing.assert_almost_equal(solution(vertex[:, 2]), fem.decast([0, 0, 1, 0], alphas, i))
            np.testing.assert_almost_equal(solution(vertex[:, 3]), fem.decast([0, 0, 0, 1], alphas, i))
            np.testing.assert_almost_equal(solution(centroid), fem.decast([.25, .25, .25, .25], alphas, i))
            np.testing.assert_almost_equal(solution(arbit_point), fem.decast([.5, .1, .1, .3], alphas, i))

        print(
            "Testing complete. Solved poisson's problem on cube and evaluated at centroid and arbitrary point"
            " with a degree 8 polynomial")
