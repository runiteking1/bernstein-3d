"""
Implements algorithm 3 of the AAD paper for 3D
"""
import numpy as np
from scipy import special
from duffy import duffy


def moment(f0, q, n, xis, omegas):
    """
    Evaluates moment

    :param f0: aray corresponding to values of a function f at Stroud nodes
    :param q: number of quadrature points
    :param n: order of approximation
    :param xis: location of quadrature points
    :param omegas: weights of quadrature rule
    :return: array composed of Bernstein-Bezier moments fo f using Stroud rule
    """

    step0 = np.zeros((n + 1, q, q))
    step1 = np.zeros((n + 1, n + 1, q))
    step2 = np.zeros((n + 1, n + 1, n + 1))

    _momentstep1(f0, q, n, xis, omegas, step0)
    _momentstep2(step0, q, n, xis, omegas, step1)
    _momentstep3(step1, q, n, xis, omegas, step2)

    return step2


def _momentstep1(fin, q, n, xis, omegas, fout):
    d = 3
    l = 1
    for i_l in range(q):
        xi = xis[d - l, i_l]
        omega = omegas[d - l, i_l]

        s = 1 - xi
        r = xi / s

        w = omega * s ** (n)

        for alpha_l in range(n + 1):
            for i_2 in range(q):
                for i_3 in range(q):
                    fout[alpha_l, i_2, i_3] += w * fin[i_l, i_2, i_3]

            w *= r * (n - alpha_l) / (1 + alpha_l)


def _momentstep2(fin, q, n, xis, omegas, fout):
    d = 3
    l = 2

    for i_l in range(q):
        xi = xis[d - l, i_l]
        omega = omegas[d - l, i_l]

        s = 1 - xi
        r = xi / s

        for alpha_1 in range(n + 1):
            w = omega * (s ** (n - alpha_1))

            for alpha_l in range(n - alpha_1 + 1):
                for i_3 in range(q):
                    fout[alpha_1, alpha_l, i_3] += w * fin[alpha_1, i_l, i_3]

                w *= r * (n - alpha_1 - alpha_l) / (1 + alpha_l)


def _momentstep3(fin, q, n, xis, omegas, fout):
    d = 3
    l = 3

    for i_l in range(q):
        xi = xis[d - l, i_l]
        omega = omegas[d - l, i_l]

        s = 1 - xi
        r = xi / s

        for alpha_1 in range(n + 1):
            for alpha_2 in range(n + 1 - alpha_1):
                w = omega * (s ** (n - alpha_1 - alpha_2))

                for alpha_l in range(n - alpha_1 - alpha_2 + 1):
                    fout[alpha_1, alpha_2, alpha_l] += w * fin[alpha_1, alpha_2, i_l]

                    w *= r * (n - alpha_1 - alpha_2 - alpha_l) / (1 + alpha_l)


def f(x):
    return 3 * x[0] * x[0] * x[1]

def known_two_element(x):
    return 1/4*(-x[0]**3 * (x[1] + x[2]) - x[0]*(x[1] - 2*x[1]**2 + x[1]**3 + 9*x[1]*x[2] + (-1 + x[2])**2*x[2]) +
       2*x[0]**2 * (x[1] + x[1]**2 + x[2] + x[2]**2) - x[1]*x[2]*(x[1]**2 + (-1 + x[2])**2 - 2*x[1]*(1 + x[2])))


if __name__ == '__main__':
    # Vertices

    vertices = np.zeros((3, 4))
    vertices[:, 0] = [1, 0, 0]
    vertices[:, 1] = [0, 1, 0]
    vertices[:, 2] = [0, 0, 1]
    vertices[:, 3] = [0, 0, 0]

    # First, generate the quadrature points
    my_q = 7
    my_xis = np.zeros((3, my_q))
    my_omegas = np.zeros((3, my_q))
    for i in range(3):
        nodes, weights = special.roots_sh_jacobi(my_q, i + 1, 1)
        my_xis[i, :] = nodes
        my_omegas[i, :] = weights

    fa = np.zeros((my_q, my_q, my_q))
    for i in range(my_q):
        for j in range(my_q):
            for k in range(my_q):
                fa[i, j, k] = known_two_element(duffy([my_xis[2, i], my_xis[1, j], my_xis[0, k]], vertices))

    # Return moment (need to scale correctly with area)
    result = (moment(fa, my_q, 4, my_xis, my_omegas))
    print(result[2, 1, 1])

