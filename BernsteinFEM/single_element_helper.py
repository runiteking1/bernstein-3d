import numpy as np
from BernsteinFEM import BernsteinFEM
from Mesh import bernstein_mesh
from mass_preconditioner import NaivePreconditioner
from pcg import pcg
import read_msh
from volume import volume
from _evaluate import evaluate
from CythonKernels.evaluate import evaluate as cython_evaluate
from moment import moment


class GrayScott(BernsteinFEM):
    def __init__(self, mymesh):
        super(GrayScott, self).__init__(mymesh)

    def nonlinear(self, u: np.ndarray, v: np.ndarray):
        output = np.zeros((self.eldof.max() + 1))

        vertices = np.zeros((3, 4))

        for element in range(self.elements):
            vertices[:, 0] = self.coordinates[self.elnode[element, 0]]
            vertices[:, 1] = self.coordinates[self.elnode[element, 1]]
            vertices[:, 2] = self.coordinates[self.elnode[element, 2]]
            vertices[:, 3] = self.coordinates[self.elnode[element, 3]]

            vol, _, _, _, _ = volume(vertices[:, 0], vertices[:, 1], vertices[:, 2], vertices[:, 3])

            c = self.element_to_control(u, element)
            d = self.element_to_control(v, element)
            # print(c)
            # U = evaluate(c, self.q, self.p, self.xis)
            U = cython_evaluate(c, self.q, self.p, self.xis)
            # V = evaluate(d, self.q, self.p, self.xis)
            F = U ** 2

            fk = 6 * vol * moment(F, self.q, self.p, self.xis, self.omegas)

            for i in range(self.dofs_per_element):
                output[self.eldof[element, i]] += fk[self.lookup_dof[i][0:3]]

        return output


if __name__ == '__main__':
    np.set_printoptions(linewidth=260, precision=4)
    p = 4
    # Load a gmsh file
    # vertices, elements, boundary = read_msh.load_file('../sphere_2.geo', refine=0)
    # mesh = bernstein_mesh.BernsteinMesh(vertices, elements, {'faces': set(), 'lines': set()}, p)

    # Single element
    coords_oneelement = np.array([[-1, -1, -1], [-1, -1, 1], [-1, 1, -1], [1, -1, -1]])
    elnode_oneelement = np.array([[1, 0, 2, 3]])
    mesh = bernstein_mesh.BernsteinMesh(coords_oneelement, elnode_oneelement, {'faces': set(), 'lines': set()}, p)

    # Two element
    # coords_oneelement = np.array([[-1, -1, -1], [-1, -1, 1], [-1, 1, -1], [1, -1, -1], [-1, -1, -3]])
    # elnode_oneelement = np.array([[0, 1, 2, 3], [0, 4, 2, 3]])
    # mesh = BernsteinMesh(coords_oneelement, elnode_oneelement, {'faces': set(), 'lines': set()}, p)

    # fem = BernsteinFEM(mesh)
    fem = GrayScott(mesh)

    def f(x):
        return x[0]

    m, s, f = fem.assemble_matrix(force_func=f)
    print(fem.elements)
    x, _, _ = pcg(m, f)

    uv2 = fem.nonlinear(x, x)
    # print(uv2)
    # print(m.dot(x))
    sol, _, _ = pcg(m, uv2)

    fem.plot_boundaries(sol)
    # print("Matrix constructed", m.shape)
    # Solver = NaivePreconditioner(m, fem)
    # print("Preconditioner constructed")
