"""
Implements algorithm 1 from AAD paper with loops unrolled for 3D
"""

import numpy as np
from scipy import special


def evaluate(c0: np.ndarray, q: int, n: int, xis: np.ndarray):
    """
    Algorithm 1 for 3D

    :param xis: Array of Stroud rule points (do not need weights)
    :param c0: BB-vector in 3d c0[0, 1, 2] corresponds to (0, 1, 2, n-3)
    :param q: Number of quadrature points
    :param n: Order
    :return: Array with entrees equal to value of BB-vector at Stroud node
    """
    # arrays to store outputs of
    step1 = np.zeros((n + 1, n + 1, q))
    step2 = np.zeros((n + 1, q, q))
    step3 = np.zeros((q, q, q))

    _evalstep1(c0, n, q, xis, step1)
    # step1 = np.ones_like(step1)
    _evalstep2(step1, n, q, xis, step2)
    _evalstep3(step2, n, q, xis, step3)
    return step3


def _evalstep1(cIn: np.ndarray, n: int, q: int, xis: np.ndarray, cOut: np.ndarray):
    l = 3
    d = 3
    for i_l in range(q):
        xi = xis[d - l, i_l]

        s = 1 - xi
        r = xi / s

        for alpha1 in range(n + 1):
            for alpha2 in range(n + 1 - alpha1):
                w = s ** (n - alpha1 - alpha2)
                for alpha_l in range(n - alpha1 - alpha2 + 1):
                    # For each (i_{l+1}, i_d) in {1, q}^{d-l} do
                    cOut[alpha1, alpha2, i_l] += w * cIn[alpha1, alpha2, alpha_l]

                    w *= r * (n - alpha1 - alpha2 - alpha_l) / (1.0 + alpha_l)


def _evalstep2(cIn: np.ndarray, n: int, q: int, xis: np.ndarray, cOut: np.ndarray):
    l = 2
    d = 3
    for i_l in range(q):
        xi = xis[d - l, i_l]

        s = 1 - xi
        r = xi / s

        for alpha1 in range(n + 1):
            w = s ** (n - alpha1)
            for alpha_l in range(n - alpha1 + 1):
                for i_d in range(q):
                    cOut[alpha1, i_l, i_d] += w * cIn[alpha1, alpha_l, i_d]

                w *= r * (n - alpha1 - alpha_l) / (1.0 + alpha_l)


def _evalstep3(cIn: np.ndarray, n: int, q: int, xis: np.ndarray, cOut: np.ndarray):
    l = 1
    d = 3

    for i_l in range(q):
        xi = xis[d - l, i_l]

        s = 1 - xi
        r = xi / s

        w = s ** n
        for alpha_l in range(n + 1):
            for i_2 in range(q):
                for i_3 in range(q):
                    cOut[i_l, i_2, i_3] += w * cIn[alpha_l, i_2, i_3]

            w *= r * (n - alpha_l) / (1.0 + alpha_l)


if __name__ == '__main__':
    c0 = np.zeros((4, 4, 4))

    c0[3, 0, 0] = 1  # Corresponds to (3, 0, 0, 0)

    # First, generate the quadrature points
    my_q = 5
    my_xis = np.zeros((3, my_q))
    my_omegas = np.zeros((3, my_q))
    for i in range(3):
        nodes, weights = special.roots_sh_jacobi(my_q, i + 1, 1)
        my_xis[i, :] = nodes
        my_omegas[i, :] = weights

    print(evaluate(c0, my_q, 3, my_xis))
