import numpy as np


def duffy(t: np.ndarray, x: np.ndarray) -> np.ndarray:
    """Duffy transformation for 3D"""
    l1 = t[0]
    l2 = t[1] * (1 - l1)
    l3 = t[2] * (1 - l1 - l2)
    l4 = 1 - l1 - l2 - l3
    return x[:, 0] * l1 + x[:, 1] * l2 + x[:, 2] * l3 + x[:, 3] * l4
    # return x[:, [0]] * l1 + x[:, [1]] * l2 + x[:, [2]] * l3 + x[:, [3]] * l4


def duffy2d(t, x):
    """Duffy transformation for 2D"""
    return x[:, [0]] * t[0] + x[:, [1]] * (t[1] * (1 - t[0])) + x[:, [2]] * (1 - t[0] - (t[1] * (1 - t[0])))


def duffy_1d(t, x):
    """Duffy transformation for 1D"""
    return x[:, [0]] * t + x[:, [1]] * (1 - t)


if __name__ == '__main__':
    xs = np.zeros((2, 3))

    xs[:, 0] = [0, 0.75]
    xs[:, 1] = [-.75, -.5]
    xs[:, 2] = [0.75, -.455]

    ts = np.array([[.4], [.4]])
    print(duffy2d(ts, xs))  # For this, expect .09, .0162
