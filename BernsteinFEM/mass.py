"""
Computes the element mass matrix in 3D

"""
import numpy as np
from scipy.special import binom
from position import pos3d


def mass(n: int, moments: np.ndarray):
    """
    Basic skeleton for element mass matrix

    :param n: order of approximation
    :param moments: array of moments arranged such that (alpha_1, alpha_2, alpha_3, alpha_4) corresponds to moments[alpha_1, alpha_2, alpha_3]
    :return: mass matrix arranged using pos3d
    """

    # Create output matrix
    elem_mass = np.zeros((int(binom(n+3,3)), int(binom(n+3,3))))

    for alpha_1 in range(n + 1):
        for beta_1 in range(n + 1):
            w_1 = binom(alpha_1 + beta_1, alpha_1) / binom(2 * n, n)

            for alpha_2 in range(n - alpha_1 + 1):
                for beta_2 in range(n - beta_1 + 1):
                    w_2 = w_1 * binom(alpha_2 + beta_2, alpha_2)

                    for alpha_3 in range(n - alpha_1 - alpha_2 + 1):
                        for beta_3 in range(n - beta_1 - beta_2 + 1):
                            w_3 = w_2 * binom(alpha_3 + beta_3, alpha_3)

                            w_4 = w_3 * binom(2 * n - alpha_1 - alpha_2 - alpha_3 - beta_1 - beta_2 - beta_3,
                                              n - alpha_1 - alpha_2 - alpha_3)

                            elem_mass[pos3d(alpha_1, alpha_2, alpha_3, n), pos3d(beta_1, beta_2, beta_3, n)] = \
                                    w_4 * moments[alpha_1 + beta_1, alpha_2 + beta_2, alpha_3 + beta_3]

    return elem_mass

def f(x):
    return 1
    # return np.exp(x[0])*np.cos(x[1])*np.sin(x[2])

if __name__ == '__main__':
    from scipy import special
    from duffy import duffy
    from moment import moment

    np.set_printoptions(precision=6, linewidth=160)

    # Create tetrahedron
    vertices = np.zeros((3, 4))
    vertices[:, 0] = [1, 0, 0] # Corresponds to (2, 0, 0, 0)
    vertices[:, 1] = [0, 1, 0] # Corresponds to (0, 2, 0, 0)
    vertices[:, 2] = [0, 0, 1] # Corresponds to (0, 0, 2, 0)
    vertices[:, 3] = [0, 0, 0] # Corresponds to (0, 0, 0, 2)

    # First, generate the quadrature points
    p = 8
    my_q = p+2
    my_xis = np.zeros((3, my_q))
    my_omegas = np.zeros((3, my_q))
    for i in range(3):
        nodes, weights = special.roots_sh_jacobi(my_q, i + 1, 1)
        my_xis[i, :] = nodes
        my_omegas[i, :] = weights

    fa = np.zeros((my_q, my_q, my_q))
    for i in range(my_q):
        for j in range(my_q):
            for k in range(my_q):
                fa[i, j, k] = f(duffy([my_xis[2, i], my_xis[1, j], my_xis[0, k]], vertices))

    # Return moment (need to scale correctly with volume)
    mmts = moment(fa, my_q, 2*p, my_xis, my_omegas)
    print(mass(p, mmts))
