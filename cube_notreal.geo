Point(1) = {-1, -1, -1, 1.0};
Point(2) = {1, -1, -1,  1.0};
Point(3) = {1, 1, -1,  1.0};
Point(4) = {-1, 1, -1,  1.0};
Point(5) = {-1, -1, 1,   1.0};
Point(6) = {1, -1, 1,   1.0};
Point(7) = {1, 1, 1,   1.0};
Point(8) = {-1, 1, 1,    1.0};

Line(1) = {1, 2};
Line(2) = {1, 3};
Line(3) = {1, 4};
Line(4) = {1, 5};
Line(5) = {1, 6};
Line(6) = {1, 7};
Line(7) = {1, 8};
Line(8) = {2, 3};
Line(9) = {2, 6};
Line(10) = {2, 7};
Line(11) = {3, 4};
Line(12) = {3, 7};
Line(13) = {4, 7};
Line(14) = {4, 8};
Line(15) = {5, 6};
Line(16) = {5, 7};
Line(17) = {5, 8};
Line(18) = {6, 7};
Line(19) = {7, 8};


//
//Line(14) = {2, 7};
//
//
//
//
//

//Line Loop(1) = {1, 2, 3, 4};
//Plane Surface(1) = {1};
//Line Loop(2) = {7, 9, -12, -2};
//Plane Surface(2) = {2};
//Line Loop(3) = {8, -6, -10, -9};
//Plane Surface(3) = {3};
//Line Loop(4) = {5, -6, 11, 4};
//Plane Surface(4) = {4};
//Line Loop(5) = {8, -5, 1, 7};
//Plane Surface(5) = {5};
//Line Loop(6) = {3, -11, -10, -12};
//Plane Surface(6) = {6};
//Surface Loop(1) = {2, 5, 3, 4, 6, 1};
//Volume(1) = {1};
//
//Physical Volume("Cube") = {1};
//Physical Surface("Boundary") = {1,2,3,4,5,6};
