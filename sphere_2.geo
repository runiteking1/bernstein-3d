// Gmsh project created on Mon Sep  2 14:06:44 2019
lc = 0.7; // Pretty sure 0.2 is the level at which we plotted
Point(1) = {0, 0, 0, lc};
Point(2) = {1, 0, 0, lc};
Point(3) = {-1, 0, 0, lc};
Point(4) = {0, 1, 0, lc};
Point(5) = {0, -1, 0, lc};
Point(6) = {0, 0, -1, lc};
Circle(1) = {4, 1, 3};
Circle(2) = {3, 1, 5};
Circle(3) = {5, 1, 2};
Circle(4) = {2, 1, 4};
Circle(5) = {3, 1, 6};
Circle(6) = {6, 1, 5};
Circle(7) = {6, 1, 2};
Circle(8) = {6, 1, 4};
Line Loop(9) = {2, 3, 4, 1};
Ruled Surface(10) = {9};
Line Loop(11) = {5, 6, -2};
Ruled Surface(12) = {11};
Line Loop(13) = {3, -7, 6};
Ruled Surface(14) = {13};
Line Loop(15) = {4, -8, 7};
Ruled Surface(16) = {15};
Line Loop(17) = {1, 5, 8};
Ruled Surface(18) = {17};
Surface Loop(19) = {10, 12, 18, 16, 14};
Volume(20) = {19};
Physical Volume(40) = {20};
//Symmetry {0, 0, 1, 0} {
//  Duplicata { Volume{20}; }
//}
//Coherence;
//Symmetry {1, 0, 0, 1} {
//  Volume{20, 21};
//}
//Physical Volume(40) = {20, 21};
