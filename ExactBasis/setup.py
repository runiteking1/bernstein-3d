from distutils.core import setup
from distutils.extension import Extension

from Cython.Build import cythonize

# Code to run is "python setup.py build_ext --inplace"

##
# Line profiler (https://stackoverflow.com/questions/28301931/how-to-profile-cython-functions-line-by-line)
# from Cython.Compiler.Options import directive_defaults
# import numpy
# directive_defaults['linetrace'] = True
# directive_defaults['binding'] = True
# extensions = [Extension("transforms", ["transforms.pyx"], include_dirs=[numpy.get_include()],
#                         define_macros=[('CYTHON_TRACE', '1')]), Extension("transforms_2", ["transforms_2.pyx"])]
##

extensions = [Extension("quad", ["quad.pyx"], extra_compile_args=["-O3", '-march=native', '-fopenmp', '-funroll-loops'],
                        extra_link_args=['-O3', '-march=native', '-funroll-loops', '-fopenmp']),
              ]


setup(
    ext_modules=cythonize(extensions)
)