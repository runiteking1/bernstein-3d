#cython: boundscheck=False, wraparound=False


def sum_quads(double[:, :, ::1] func1, double[:, :, ::1] func2, double[::1] w00, double[::1] w10, double[::1] w20):
    cdef double total = 0
    for i in range(len(w00)):
        for j in range(len(w10)):
            for k in range(len(w20)):
                total += w00[i]*w10[j]*w20[k] * func1[i, j, k] * func2[i, j, k]/8.0

    return total