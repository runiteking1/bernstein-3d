from multiprocessing import Pool

import numpy as np
from scipy.special import eval_jacobi as jac, roots_jacobi as rj

# from glj_quad import quad_rules
from quad import sum_quads


# Barycentric coordinates
def l1(x):
    return (- x[0] - x[1] - x[2] - 1) / 2


def l2(x):
    return (1 + x[0]) / 2


def l3(x):
    return (1 + x[1]) / 2


def l4(x):
    return (1 + x[2]) / 2


def l(x, r):
    return (-1) ** r / (r + 1) * jac(r, 0, 1, x)


def vertex(i, j, k, p):
    return (12 * (3 + 2 * i) * (2 + i + j) * (5 + 2 * i + 2 * j + 2 * k)) / (
            p * (2 + p) ** 2 * (12 + 19 * p + 8 * (p ** 2) + p ** 3))


# Vertex functions
def phi1(x, p: int):
    # return (-1) ** (p + 1) / p * l1(x) * jac(p - 1, 1, 1, 1 - 2 * l1(x)) * l1(x)
    # return (-1) ** (p) / (p + 1) * jac(p-1, 0, 1, 1 - 2 * l1(x)) * l1(x)
    #
    # return l1(x) * l(1 - 2 * (l1(x) + l3(x) + l4(x)), p) * l(1 - 2 * (l1(x) + l2(x) + l4(x)), p) * l(
    #     1 - 2 * (l1(x) + l2(x) + l3(x)), p)
    # return l1(x) * l(x[0], p) * l(x[1], p) * l(x[2] , p)

    total = np.zeros_like(x[0], dtype=np.float)
    # u = 2 * (1 + x[0]) / (-x[1] - x[2]) - 1
    # u = (l2(x) - l1(x))/(1 - l3(x) - l4(x))
    # v = 2 * (1 + x[1]) / (1 - x[2]) - 1
    # v = 2 * l3(x) /(1 - l4(x)) - 1
    # for i in range(p - 1 + 1):
    #     for j in range(0, p - 1 - i + 1):
    #         for k in range(0, p - 1 - j - i + 1):
    #             total += vertex(i, j, k, p) * (-1) ** (j + k + 2) * l1(x) * (l1(x) + l2(x) + l3(x)) ** j * (
    #                     -1 + l3(x) + l4(x)) ** i * jac(i, 2, 0, (l2(x) - l1(x)) / (1 - l3(x) - l4(x))) * \
    #                      jac(j, 2 * i + 3, 0, 2 * l3(x) / (1 - l4(x)) - 1) * jac(k, 2 * i + 2 * j + 4, 0, 2 * l4(x) - 1)
                # total += vertex(i, j, k, p)* (-1)**(i + j + k) * (1 - u) / 2 * jac(i, 2, 0, u) * ((1 - v) / 2) ** (i + 1) \
                #          * jac(j, 2 * i + 3, 0, v) * ((1 - w) / 2) ** (i + j + 1) * jac(k, 2 * i + 2 * j + 4, 0,  2*l4(x) - 1)
    i = np.floor(p/2)
    j = np.floor(i/2)
    k = p-1-i-j

    # j = np.floor((p-3)/3)
    # k = np.floor((p-3)/3)
    # i = p-1 - j - k
    total = (-1) ** (j + k + 2) * l1(x) * (l1(x) + l2(x) + l3(x)) ** j * (
                        -1 + l3(x) + l4(x)) ** i * jac(i, 2, 1, (l2(x) - l1(x)) / (1 - l3(x) - l4(x))) * \
                         jac(j, 2 * i + 4, 1, 2 * l3(x) / (1 - l4(x)) - 1) * jac(k, 2 * i + 2 * j + 6, 1, 2 * l4(x) - 1)/((i+1)*(j+1)*(k+1))
    return total


def phi2(x, p: int):
    # return (-1) ** (p + 1) / p * l2(x) * jac(p - 1, 1, 1, 1 - 2 * l2(x)) * l2(x)
    # return (-1) ** (p) / (p + 1) * jac(p-1, 0, 1, 1 - 2 * l2(x)) * l2(x)

    # return l2(x) * l(1 - 2 * (l2(x) + l1(x) + l4(x)), p) * l(1 - 2 * (l2(x) + l1(x) + l3(x)), p) * l(
    #     1 - 2 * (l2(x) + l4(x) + l3(x)), p)

    total = np.zeros_like(x[0], dtype=np.float)
    # for i in range(p - 1 + 1):
    #     for j in range(0, p - 1 - i + 1):
    #         for k in range(0, p - 1 - j - i + 1):
    #             total += vertex(i, j, k, p) * (-1) ** (j + k + 2) * l2(x) * (l2(x) + l1(x) + l3(x)) ** j * (
    #                     -1 + l3(x) + l4(x)) ** i * jac(i, 2, 0, (l1(x) - l2(x)) / (1 - l3(x) - l4(x))) * \
    #                      jac(j, 2 * i + 3, 0, 2 * l3(x) / (1 - l4(x)) - 1) * jac(k, 2 * i + 2 * j + 4, 0, 2 * l4(x) - 1)
    i = np.floor(p/2)
    j = np.floor(i/2)
    k = p-1-i-j

    # j = np.floor((p - 3) / 3)
    # k = np.floor((p - 3) / 3)
    # i = p - 1 - j - k
    total += (-1) ** (j + k + 2) * l2(x) * (l2(x) + l1(x) + l3(x)) ** j * (
            -1 + l3(x) + l4(x)) ** i * jac(i, 2, 1, (l1(x) - l2(x)) / (1 - l3(x) - l4(x))) * \
             jac(j, 2 * i + 4, 1, 2 * l3(x) / (1 - l4(x)) - 1) * jac(k, 2 * i + 2 * j + 6, 1, 2 * l4(x) - 1)/((i+1)*(j+1)*(k+1))
    return total

def phi3(x, p: int):
    # return (-1) ** (p + 1) / p * l3(x) * jac(p - 1, 1, 1, 1 - 2 * l3(x)) * l3(x)
    # return (-1) ** (p) / (p + 1) * jac(p-1, 0, 1, 1 - 2 * l3(x)) * l3(x)

    # return l3(x) * l(1 - 2 * (l3(x) + l1(x) + l4(x)), p) * l(1 - 2 * (l3(x) + l2(x) + l4(x)), p) * l(
    #     1 - 2 * (l3(x) + l2(x) + l1(x)), p)

    total = np.zeros_like(x[0], dtype=np.float)
    # for i in range(p - 1 + 1):
    #     for j in range(0, p - 1 - i + 1):
    #         for k in range(0, p - 1 - j - i + 1):
    #             total += vertex(i, j, k, p) * (-1) ** (j + k + 2) * l3(x) * (l3(x) + l2(x) + l1(x)) ** j * (
    #                     -1 + l1(x) + l4(x)) ** i * jac(i, 2, 0, (l2(x) - l3(x)) / (1 - l1(x) - l4(x))) * \
    #                      jac(j, 2 * i + 3, 0, 2 * l1(x) / (1 - l4(x)) - 1) * jac(k, 2 * i + 2 * j + 4, 0, 2 * l4(x) - 1)
    i = np.floor(p/2)
    j = np.floor(i/2)
    k = p-1-i-j

    # j = np.floor((p - 3) / 3)
    # k = np.floor((p - 3) / 3)
    # i = p - 1 - j - k
    total += (-1) ** (j + k + 2) * l3(x) * (l3(x) + l2(x) + l1(x)) ** j * (
                        -1 + l1(x) + l4(x)) ** i * jac(i, 2, 1, (l2(x) - l3(x)) / (1 - l1(x) - l4(x))) * \
                         jac(j, 2 * i + 4, 1, 2 * l1(x) / (1 - l4(x)) - 1) * jac(k, 2 * i + 2 * j + 6, 1, 2 * l4(x) - 1)/((i + 1)*(j+1)*(k+1))

    return total

def phi4(x, p: int):
    # return (-1) ** (p + 1) / p * l4(x) * jac(p - 1, 1, 1, 1 - 2 * l4(x)) * l4(x)
    # return (-1) ** (p) / (p + 1) * jac(p-1, 0, 1, 1 - 2 * l4(x)) * l4(x)

    # return l4(x) * l(1 - 2 * (l4(x) + l1(x) + l2(x)), p) * l(1 - 2 * (l4(x) + l1(x) + l3(x)), p) * l(
    #     1 - 2 * (l4(x) + l2(x) + l3(x)), p)

    total = np.zeros_like(x[0], dtype=np.float)
    # for i in range(p - 1 + 1):
    #     for j in range(0, p - 1 - i + 1):
    #         for k in range(0, p - 1 - j - i + 1):
    #             total += vertex(i, j, k, p) * (-1) ** (j + k + 2) * l4(x) * (l4(x) + l2(x) + l3(x)) ** j * (
    #                     -1 + l3(x) + l1(x)) ** i * jac(i, 2, 0, (l2(x) - l4(x)) / (1 - l3(x) - l1(x))) * \
    #                      jac(j, 2 * i + 3, 0, 2 * l3(x) / (1 - l1(x)) - 1) * jac(k, 2 * i + 2 * j + 4, 0, 2 * l1(x) - 1)
    i = np.floor(p/2)
    j = np.floor(i/2)
    k = p-1-i-j

    # j = np.floor((p - 3) / 3)
    # k = np.floor((p - 3) / 3)
    # i = p - 1 - j - k
    total += (-1) ** (j + k + 2) * l4(x) * (l4(x) + l2(x) + l3(x)) ** j * (
                        -1 + l3(x) + l1(x)) ** i * jac(i, 2, 1, (l2(x) - l4(x)) / (1 - l3(x) - l1(x))) * \
                         jac(j, 2 * i + 4, 1, 2 * l3(x) / (1 - l1(x)) - 1) * jac(k, 2 * i + 2 * j + 6, 1, 2 * l1(x) - 1)/((i+1)*(j+1)*(k+1))

    return total


# Interior function
def psi(x: np.ndarray, i: int, j: int, k: int) -> float:
    # Just to make things a bit more clear
    u = 2 * (1 + x[0]) / (-x[1] - x[2]) - 1
    v = 2 * (1 + x[1]) / (1 - x[2]) - 1
    w = x[2]

    return (1 - u) / 2 * (1 + u) / 2 * jac(i - 1, 2, 2, u) * ((1 - v) / 2) ** (i + 1) * (1 + v) / 2 \
           * jac(j - 1, 2 * i + 3, 2, v) * ((1 - w) / 2) ** (i + j + 1) * (1 + w) / 2 * jac(k - 1, 2 * i + 2 * j + 4, 2,
                                                                                            w)


# face function
def chi1(x: np.ndarray, i: int, j: int, k=1) -> float:
    return l1(x) * l2(x) * l3(x) * (-1) ** (1 - i) * (l1(x) + l2(x) + l3(x)) ** (-1 + j) * (l3(x) + l4(x) - 1) ** (
            -1 + i) * jac(
        i - 1, 2, 2, (l2(x) - l1(x)) / (1 - l3(x) - l4(x))) \
           * jac(j - 1, 2 * i + 3, 2, 2 * l3(x) / (1 - l4(x)) - 1)

    # u = 2 * (1 + x[0]) / (-x[1] - x[2]) - 1
    # u = (l2(x) - l1(x))/(1 - l3(x) - l4(x))
    # v = 2 * (1 + x[1]) / (1 - x[2]) - 1
    # v = 2 * l3(x) /(1 - l4(x)) - 1
    # w = x[2]
    # w = 2*l4(x) - 1
    # return (1 - u) / 2 * (1 + u) / 2 * jac(i - 1, 2, 2, u) * ((1 - v) / 2) ** (i + 1) * (1 + v) / 2 \
    #        * jac(j - 1, 2 * i + 3, 2, v) * ((1 - w) / 2) ** (i + j + 1) #* jac(k - 1, 2 * i + 2 * j + 4, 0, w


def chi2(x: np.ndarray, i: int, j: int, k=1) -> float:
    return l1(x) * l2(x) * l4(x) * (-1) ** (1 - i) * (l1(x) + l2(x) + l4(x)) ** (-1 + j) * (l3(x) + l4(x) - 1) ** (
            -1 + i) * jac(
        i - 1, 2, 2, (l2(x) - l1(x)) / (1 - l4(x) - l3(x))) \
           * jac(j - 1, 2 * i + 3, 2, 2 * l4(x) / (1 - l3(x)) - 1)


def chi3(x: np.ndarray, i: int, j: int, k=1) -> float:
    return l1(x) * l4(x) * l3(x) * (-1) ** (1 - i) * (l1(x) + l4(x) + l3(x)) ** (-1 + j) * (l3(x) + l2(x) - 1) ** (
            -1 + i) * jac(
        i - 1, 2, 2, (l4(x) - l1(x)) / (1 - l3(x) - l2(x))) \
           * jac(j - 1, 2 * i + 3, 2, 2 * l3(x) / (1 - l2(x)) - 1)


def chi4(x: np.ndarray, i: int, j: int, k=1) -> float:
    return l4(x) * l2(x) * l3(x) * (-1) ** (1 - i) * (l4(x) + l2(x) + l3(x)) ** (-1 + j) * (l3(x) + l1(x) - 1) ** (
            -1 + i) * jac(
        i - 1, 2, 2, (l2(x) - l4(x)) / (1 - l3(x) - l1(x))) \
           * jac(j - 1, 2 * i + 3, 2, 2 * l3(x) / (1 - l1(x)) - 1)


# Edge function
def rho(i, j, k):
    return 2 / (7 + 2 * i + 2 * j + 2 * k)


def nu(i, j):
    return 1 / (3 + i + j)


def mu(i):
    return 2 * (1 + i) * (2 + i) / ((3 + i) * (4 + i) * (5 + 2 * i))


def lamb(i, p):
    total = 0
    for j in range(0, p - 2 - i + 1):
        for k in range(0, p - 2 - i - j + 1):
            total += 1 / (nu(i, j) * rho(i, j, k))
    return 1 / total


def gamma1(x: np.ndarray, i: int, p):
    # total = 0
    # for j in range(0, p - 2 - i + 1):
    #     for k in range(0, p - 2 - j - i + 1):
    #         # print(lamb(i, p)/(nu(i, j) * rho(i, j, k)), i, j, k)
    #         # total += (-1)**(k + j) * lamb(i, p)/(nu(i, j) * rho(i, j, k)) * (1 - u) / 2 * (1 + u) / 2 * jac(i, 2, 2, u) * ((1 - v) / 2) ** (i + 2) \
    #         #     * jac(j, 2 * i + 5, 0, v) * ((1 - w) / 2) ** (i + j + 2) * jac(k, 2 * i + 2 * j + 6, 0, w)
    #         total += (-1) ** (k + j) * lamb(i, p) / (nu(i, j) * rho(i, j, k)) * (-1) ** (2 - i) * l1(x) * l2(x) * (
    #                 1 - l4(x)) ** j * (-l1(x) - l2(x)) ** i * jac(i, 2, 2, (l2(x) - l1(x)) / (1 - l3(x) - l4(x))) \
    #                  * jac(j, 2 * i + 5, 0, 2 * l3(x) / (1 - l4(x)) - 1) * jac(k, 2 * i + 2 * j + 6, 0, 2 * l4(x) - 1)

    j = np.floor((p-2-i)/2)
    k = p-2-i-j
    total = (-1) ** (2 - i + k + j) * l1(x) * l2(x) * (
                    1 - l4(x)) ** j * (-l1(x) - l2(x)) ** i * jac(i, 2, 2, (l2(x) - l1(x)) / (1 - l3(x) - l4(x))) \
                     * jac(j, 2 * i + 5, 1, 2 * l3(x) / (1 - l4(x)) - 1) * jac(k, 2 * i + 2 * j + 6, 1, 2 * l4(x) - 1)/((j+1)*(k+1))
    return total

    # u = 2 * (1 + x[0]) / (-x[1] - x[2]) - 1
    # u = (l2(x) - l1(x)) / (1 - l3(x) - l4(x))
    # v = 2 * (1 + x[1]) / (1 - x[2]) - 1
    # v = 2 * l3(x) / (1 - l4(x)) - 1
    # w = x[2]
    # w = 2 * l4(x) - 1
    # j = np.floor((p-2-i)/2)
    # k = p-2-i-j

    # if i == 1:
    #     j = 0
    #     k = 0
    #     val1 = 4/15 * (1 - u) / 2 * (1 + u) / 2 * jac(i, 2, 2, u) * ((1 - v) / 2) ** (i + 2) \
    #     * jac(j, 2 * i + 5, 0, v) * ((1 - w) / 2) ** (i + j + 2) * jac(k, 2 * i + 2 * j + 6, 0, w)
    #
    #     j = 1
    #     k = 0
    #     val2 = 11/27 * (1 - u) / 2 * (1 + u) / 2 * jac(i, 2, 2, u) * ((1 - v) / 2) ** (i + 2) \
    #     * jac(j, 2 * i + 5, 0, v) * ((1 - w) / 2) ** (i + j + 2) * jac(k, 2 * i + 2 * j + 6, 0, w)
    #
    #     j = 0
    #     k = 1
    #     val3 = 44/135 * (1 - u) / 2 * (1 + u) / 2 * jac(i, 2, 2, u) * ((1 - v) / 2) ** (i + 2) \
    #     * jac(j, 2 * i + 5, 0, v) * ((1 - w) / 2) ** (i + j + 2) * jac(k, 2 * i + 2 * j + 6, 0, w)
    #     return val1 + val2 + val3
    # else:
    #     j= 0
    #     k = 0
    #     return (1 - u) / 2 * (1 + u) / 2 * jac(i, 2, 2, u) * ((1 - v) / 2) ** (i + 2) \
    #            * jac(j, 2 * i + 5, 0, v) * ((1 - w) / 2) ** (i + j + 2) * jac(k, 2 * i + 2 * j + 6, 0, w)

    # j = 0
    # k = 0
    # if i +j + k > p-2:
    #     print("error!")
    # return(1 - u) / 2 * (1 + u) / 2 * jac(i, 2, 2, u) * ((1 - v) / 2) ** (i + 2) \
    #        * jac(j, 2 * i + 5, 1, v) * ((1 - w) / 2) ** (i + j + 2) * jac(k, 2 * i + 2 * j + 6, 1,
    #                                                                                         w)
    # return (1 - u) / 2 * (1 + u) / 2 * jac(i - 1, 2, 2, u) * ((1 - v) / 2) ** (i + 1)  \
    #        * jac(j - 1, 2 * i + 3, 0, v) * ((1 - w) / 2) ** (i + j) * jac(k - 1, 2 * i + 2 * j + 2, 0, w)
    # return (1 - u) / 2 * (1 + u) / 2 * jac(i, 2, 2, u) * ((1 - v) / 2) ** (i + 2) \
    #        * jac(j, 2 * i + 5, 0, v) * ((1 - w) / 2) ** (i + j + 2) * jac(k, 2 * i + 2 * j + 6, 0, w)


def gamma2(x: np.ndarray, i: int, p):
    total = 0
    # for j in range(0, p - 2 - i + 1):
    #     for k in range(0, p - 2 - j - i + 1):
    #         # print(lamb(i, p)/(nu(i, j) * rho(i, j, k)), i, j, k)
    #         # total += (-1)**(k + j) * lamb(i, p)/(nu(i, j) * rho(i, j, k)) * (1 - u) / 2 * (1 + u) / 2 * jac(i, 2, 2, u) * ((1 - v) / 2) ** (i + 2) \
    #         #     * jac(j, 2 * i + 5, 0, v) * ((1 - w) / 2) ** (i + j + 2) * jac(k, 2 * i + 2 * j + 6, 0, w)
    #         total += (-1) ** (k + j) * lamb(i, p) / (nu(i, j) * rho(i, j, k)) * (-1) ** (2 - i) * l1(x) * l3(x) * (
    #                 1 - l4(x)) ** j * (-l1(x) - l3(x)) ** i * jac(i, 2, 2, (l3(x) - l1(x)) / (1 - l2(x) - l4(x))) \
    #                  * jac(j, 2 * i + 5, 0, 2 * l2(x) / (1 - l4(x)) - 1) * jac(k, 2 * i + 2 * j + 6, 0, 2 * l4(x) - 1)
    j = np.floor((p-2-i)/2)
    k = p-2-i-j
    total += (-1) ** (k + j) * (-1) ** (2 - i) * l1(x) * l3(x) * (
                    1 - l4(x)) ** j * (-l1(x) - l3(x)) ** i * jac(i, 2, 2, (l3(x) - l1(x)) / (1 - l2(x) - l4(x))) \
                     * jac(j, 2 * i + 5, 1, 2 * l2(x) / (1 - l4(x)) - 1) * jac(k, 2 * i + 2 * j + 6, 1, 2 * l4(x) - 1)/((k+1)*(j+1))

    return total


def gamma3(x: np.ndarray, i: int, p):
    total = 0
    # for j in range(0, p - 2 - i + 1):
    #     for k in range(0, p - 2 - j - i + 1):
    #         total += (-1) ** (k + j) * lamb(i, p) / (nu(i, j) * rho(i, j, k)) * (-1) ** (2 - i) * l1(x) * l4(x) * (
    #                 1 - l2(x)) ** j * (-l1(x) - l4(x)) ** i * jac(i, 2, 2, (l4(x) - l1(x)) / (1 - l3(x) - l2(x))) \
    #                  * jac(j, 2 * i + 5, 0, 2 * l3(x) / (1 - l2(x)) - 1) * jac(k, 2 * i + 2 * j + 6, 0, 2 * l2(x) - 1)

    j = np.floor((p-2-i)/2)
    k = p-2-i-j
    total += (-1) ** (k + j) * (-1) ** (2 - i) * l1(x) * l4(x) * (
                    1 - l2(x)) ** j * (-l1(x) - l4(x)) ** i * jac(i, 2, 2, (l4(x) - l1(x)) / (1 - l3(x) - l2(x))) \
                     * jac(j, 2 * i + 5, 1, 2 * l3(x) / (1 - l2(x)) - 1) * jac(k, 2 * i + 2 * j + 6, 1, 2 * l2(x) - 1)/((k+1)*(j+1))

    return total


def gamma4(x: np.ndarray, i: int, p):
    total = 0
    # for j in range(0, p - 2 - i + 1):
    #     for k in range(0, p - 2 - j - i + 1):
    #         total += (-1) ** (k + j) * lamb(i, p) / (nu(i, j) * rho(i, j, k)) * (-1) ** (2 - i) * l3(x) * l2(x) * (
    #                 1 - l4(x)) ** j * (-l3(x) - l2(x)) ** i * jac(i, 2, 2, (l2(x) - l3(x)) / (1 - l1(x) - l4(x))) \
    #                  * jac(j, 2 * i + 5, 0, 2 * l1(x) / (1 - l4(x)) - 1) * jac(k, 2 * i + 2 * j + 6, 0, 2 * l4(x) - 1)
    j = np.floor((p-2-i)/2)
    k = p-2-i-j
    total += (-1) ** (k + j) * (-1) ** (2 - i) * l3(x) * l2(x) * (
                    1 - l4(x)) ** j * (-l3(x) - l2(x)) ** i * jac(i, 2, 2, (l2(x) - l3(x)) / (1 - l1(x) - l4(x))) \
                     * jac(j, 2 * i + 5, 1, 2 * l1(x) / (1 - l4(x)) - 1) * jac(k, 2 * i + 2 * j + 6, 1, 2 * l4(x) - 1)/((k+1)*(j+1))
    return total


def gamma5(x: np.ndarray, i: int, p):
    total = 0
    # for j in range(0, p - 2 - i + 1):
    #     for k in range(0, p - 2 - j - i + 1):
    #         total += (-1) ** (k + j) * lamb(i, p) / (nu(i, j) * rho(i, j, k)) * (-1) ** (2 - i) * l4(x) * l2(x) * (
    #                 1 - l1(x)) ** j * (-l4(x) - l2(x)) ** i * jac(i, 2, 2, (l2(x) - l4(x)) / (1 - l3(x) - l1(x))) \
    #                  * jac(j, 2 * i + 5, 0, 2 * l3(x) / (1 - l1(x)) - 1) * jac(k, 2 * i + 2 * j + 6, 0, 2 * l1(x) - 1)
    j = np.floor((p-2-i)*1.5/2)
    k = p-2-i-j
    total += (-1) ** (k + j)* (-1) ** (2 - i) * l4(x) * l2(x) * (
                    1 - l1(x)) ** j * (-l4(x) - l2(x)) ** i * jac(i, 2, 2, (l2(x) - l4(x)) / (1 - l3(x) - l1(x))) \
                     * jac(j, 2 * i + 5, 1, 2 * l3(x) / (1 - l1(x)) - 1) * jac(k, 2 * i + 2 * j + 6, 1, 2 * l1(x) - 1)/((k+1)*(j+1))
    return total


def gamma6(x: np.ndarray, i: int, p):
    total = 0
    # for j in range(0, p - 2 - i + 1):
    #     for k in range(0, p - 2 - j - i + 1):
    #         total += (-1) ** (k + j) * lamb(i, p) / (nu(i, j) * rho(i, j, k)) * (-1) ** (2 - i) * l4(x) * l3(x) * (
    #                 1 - l1(x)) ** j * (-l4(x) - l3(x)) ** i * jac(i, 2, 2, (l3(x) - l4(x)) / (1 - l2(x) - l1(x))) \
    #                  * jac(j, 2 * i + 5, 0, 2 * l2(x) / (1 - l1(x)) - 1) * jac(k, 2 * i + 2 * j + 6, 0, 2 * l1(x) - 1)
    j = np.floor((p-2-i)/2)
    k = p-2-i-j
    total += (-1) ** (k + j) * (-1) ** (2 - i) * l4(x) * l3(x) * (
                    1 - l1(x)) ** j * (-l4(x) - l3(x)) ** i * jac(i, 2, 2, (l3(x) - l4(x)) / (1 - l2(x) - l1(x))) \
                     * jac(j, 2 * i + 5, 1, 2 * l2(x) / (1 - l1(x)) - 1) * jac(k, 2 * i + 2 * j + 6, 1, 2 * l1(x) - 1)/((k+1)*(j+1))
    return total


def quad_tet(f, w00, w10, w20, x00, x10, x20) -> float:
    """
    Calculates the integral over the reference tetrahedron

    :param f: the function
    :param w00: weights of the specified quadrature (see Sherwin book)
    :param w10: see above
    :param w20: see above
    :param x00: quadrature nodes as specified in book
    :param x10: see above
    :param x20: see above
    :return: double
    """
    total = 0
    for i in range(len(w00)):
        for j in range(len(w10)):
            total += w00[i] * w10[j] / 2 * np.dot(w20 / 4, f(
                (.25 * (-3 + x00[i] - x10[j] - x00[i] * x10[j] + (1 + x00[i]) * (-1 + x10[j]) * x20),
                 .5 * (-1 + x10[j] - x20 - x10[j] * x20), x20)))
    return total


def plot_tet(f):
    from pyevtk.hl import pointsToVTK

    numpoints = 20
    xs = np.linspace(-1, 1, num=numpoints)
    total_x = np.zeros((numpoints ** 3))
    total_y = np.zeros((numpoints ** 3))
    total_z = np.zeros((numpoints ** 3))
    total_f = np.zeros((numpoints ** 3))
    counter = 0
    for i in range(numpoints):
        for j in range(numpoints):
            for k in range(numpoints):
                x = .25 * (-3 + xs[i] - xs[j] - xs[i] * xs[j] + (1 + xs[i]) * (-1 + xs[j]) * xs[k])
                y = .5 * (-1 + xs[j] - xs[k] - xs[j] * xs[k])
                z = xs[k]
                val = f((x, y, z))

                if np.isnan(val):
                    val = 0

                total_x[counter] = x
                total_y[counter] = y
                total_z[counter] = z
                total_f[counter] = val

                counter += 1

    pointsToVTK("./plot", total_x, total_y, total_z, data={"func": total_f})


def find_cond_num(p: int):
    # Calculates the required quadrature nodes/points
    x00, w00 = rj(p + 1, 0, 0)
    x10, w10 = rj(p + 1, 1, 0)
    x20, w20 = rj(p + 1, 2, 0)
    # x00, w00 = quad_rules(p + 3, 0, 0)
    # x10, w10 = quad_rules(p + 3, 1, 0)
    # x20, w20 = quad_rules(p + 3, 2, 0)

    func_dofs = list()
    counter = 0
    boundary_counter = 0

    # Add vertices
    boundary_counter += 4
    counter += 4
    edge_counter = 4
    # order = int(np.floor((p - 1) / 3))

    order = p
    func_dofs.append(lambda x, p=order: phi1(x, p))
    func_dofs.append(lambda x, p=order: phi2(x, p))
    func_dofs.append(lambda x, p=order: phi3(x, p))
    func_dofs.append(lambda x, p=order: phi4(x, p))

    # Add edges
    for i in range(0, p - 1):
        counter += 1
        boundary_counter += 1
        edge_counter += 1
        func_dofs.append(lambda x, i=i, p=p: gamma1(x, i, p))

    for i in range(0, p - 1):
        counter += 1
        boundary_counter += 1
        edge_counter += 1
        func_dofs.append(lambda x, i=i, p=p: gamma2(x, i, p))

    for i in range(0, p - 1):
        counter += 1
        boundary_counter += 1
        edge_counter += 1
        func_dofs.append(lambda x, i=i, p=p: gamma3(x, i, p))

    for i in range(0, p - 1):
        counter += 1
        boundary_counter += 1
        edge_counter += 1
        func_dofs.append(lambda x, i=i, p=p: gamma4(x, i, p))

    for i in range(0, p - 1):
        counter += 1
        boundary_counter += 1
        edge_counter += 1
        func_dofs.append(lambda x, i=i, p=p: gamma5(x, i, p))

    for i in range(0, p - 1):
        counter += 1
        boundary_counter += 1
        edge_counter += 1
        func_dofs.append(lambda x, i=i, p=p: gamma6(x, i, p))

    # Add faces
    for i in range(1, p - 1):
        for j in range(1, p - i):
            boundary_counter += 1
            counter += 1
            func_dofs.append(lambda x, i=i, j=j: chi1(x, i, j))

    for i in range(1, p - 1):
        for j in range(1, p - i):
            boundary_counter += 1
            counter += 1
            func_dofs.append(lambda x, i=i, j=j: chi2(x, i, j))

    for i in range(1, p - 1):
        for j in range(1, p - i):
            boundary_counter += 1
            counter += 1
            func_dofs.append(lambda x, i=i, j=j: chi3(x, i, j))

    for i in range(1, p - 1):
        for j in range(1, p - i):
            boundary_counter += 1
            counter += 1
            func_dofs.append(lambda x, i=i, j=j: chi4(x, i, j))

    # Add interiors
    for i in range(1, p - 1):
        for j in range(1, p - i):
            for k in range(1, p - i - j):
                counter += 1
                func_dofs.append(lambda x, i=i, j=j, k=k: psi(x, i, j, k))

    # Create np array of points to have one function eval
    points1 = np.zeros(len(w00) * len(w10) * len(w20))
    points2 = np.zeros(len(w00) * len(w10) * len(w20))
    points3 = np.zeros(len(w00) * len(w10) * len(w20))
    func_counter = 0
    for i in range(len(w00)):
        for j in range(len(w10)):
            for k in range(len(w20)):
                points1[func_counter], points2[func_counter], points3[func_counter] = (
                    (.25 * (-3 + x00[i] - x10[j] - x00[i] * x10[j] + (1 + x00[i]) * (-1 + x10[j]) * x20[k]),
                     .5 * (-1 + x10[j] - x20[k] - x10[j] * x20[k]), x20[k]))
                func_counter += 1

    M = np.zeros((counter, counter))
    mass_points = np.zeros((counter, len(w00), len(w10), len(w20)))
    for func in range(counter):
        mass_points[func, :, :, :] = np.reshape(func_dofs[func]((points1, points2, points3)),
                                                (len(w00), len(w10), len(w20)))
        # for i in range(len(w00)):
        #     for j in range(len(w10)):
        #         mass_points[func, i, j, :] = func_dofs[func](
        #             (.25 * (-3 + x00[i] - x10[j] - x00[i] * x10[j] + (1 + x00[i]) * (-1 + x10[j]) * x20),
        #              .5 * (-1 + x10[j] - x20 - x10[j] * x20), x20))

    # Boundary component
    for i in range(boundary_counter):
        for j in range(boundary_counter):
            M[i, j] = sum_quads(mass_points[i], mass_points[j], w00, w10, w20)
            M[j, i] = M[i, j]

    # Interior component
    for i in range(boundary_counter, counter):
        M[i, i] = sum_quads(mass_points[i], mass_points[i], w00, w10, w20)

    # Int-bound component
    for i in range(boundary_counter, counter):
        for j in range(boundary_counter):
            M[i, j] = sum_quads(mass_points[i], mass_points[j], w00, w10, w20)
            M[j, i] = M[i, j]

    from matplotlib import pyplot as plt
    plt.spy(M, precision=1e-18, markersize=1)
    plt.show()
    # Create right hand side
    # f = np.zeros((counter))
    #
    # def rhs(x):
    #     return x[0] ** 1 + x[1] * x[2] - x[1] * 2 + x[2]
    #
    # for i in range(counter):
    #     f[i] = sum_quads(np.reshape(rhs((points1, points2, points3)),
    #                                 (len(w00), len(w10), len(w20))), mass_points[i], w00, w10, w20)
    #
    # # print(phi1((-1, -1, -1), 4))
    # # print(phi1(np.array((1.00, -1.0000, -1.0)), 4))
    # # print(phi1((-1.0, 0.999999999, -1.0), 4))
    # # print(phi1((-1.0, -1.0, 0.9999999999), 4))
    #
    # u = np.linalg.solve(M, f)
    #
    # total = 0
    # test_point = (-.8, -.2, -.5)
    # for i in range(len(func_dofs)):
    #     total += func_dofs[i](test_point) * u[i]
    # print(total - rhs(test_point))

    # Create Schur complement
    cutoff = boundary_counter
    A = M[0:cutoff, 0:cutoff]
    B = M[0:cutoff, cutoff:]
    C = M[cutoff:, cutoff:]
    S = A - B @ (np.linalg.inv(C) @ B.T)

    print(S[4 + 6*(p-1):, 4 + 6*(p-1):])
    # Create preconditioner
    # print(p, S[3,3])
    P = np.diag(np.diag(S))
    # P = np.zeros_like(S)
    # P[0:4, 0:4] = S[0:4, 0:4]
    # P[4:, 4:] = S[4:, 4:]
    # P[4:edge_counter, 4:edge_counter] = S[4:edge_counter, 4:edge_counter]
    # P[edge_counter:, edge_counter:] = S[edge_counter:, edge_counter:]

    eigs = np.real(np.linalg.eigvals(np.linalg.solve(P, S)))
    # eigs = np.real(np.linalg.eigvals(S))
    print(p, max(eigs) / min(eigs), max(eigs), min(eigs))


if __name__ == '__main__':
    p = 4
    np.set_printoptions(linewidth=260, precision=3)
    print('"bad" vertex')
    pool = Pool(1)
    pool.map(find_cond_num, range(4, 5, 1))
    pool.terminate()
    # for p in [4, 5, 6]:
    #     find_cond_num(p)
