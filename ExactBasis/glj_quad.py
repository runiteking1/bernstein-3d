"""
Implements the Gauss-Lobatto-Jacobi quadrature from Spencer/Sherwin textbook Appendix b
"""
import numpy as np
from scipy.special import eval_jacobi as jac, roots_jacobi as rj
from math import factorial as fact


def quad_rules(Q: int, alpha: int, beta: int):
    # Calculate the location of evaluation
    roots, _ = rj(Q - 2, alpha + 1, beta + 1)
    roots = np.insert(roots, 0, -1)
    roots = np.append(roots, 1)

    # Calculate weights
    weights = c(alpha, beta, roots[1:-1], Q)
    weights = np.insert(weights, 0, (beta+1)*c(alpha, beta, -1, Q))
    weights = np.append(weights, (alpha+1)*c(alpha, beta, 1, Q))

    return roots, weights

def c(alpha, beta, xi, Q):
    return 2 ** (alpha + beta + 1) * fact(alpha + Q - 1) * fact(beta + Q - 1) / (
            (Q - 1) * fact(Q - 1) * fact(alpha + beta + Q) * jac(Q - 1, alpha, beta, xi) ** 2)

if __name__ == '__main__':
    def f(x):
        return x**2 - 1 + 2*x

    roots, weights = quad_rules(8, 2, 0)
    print(np.dot(f(roots), weights))