import numpy as np
from scipy.special import eval_jacobi as jac, roots_jacobi as rj
from scipy.integrate import tplquad
from typing import Callable
from scipy.integrate import quad
from matplotlib import pyplot as plt


# Barycentric coorrdinates
def l1(x, y):
    return (-x - y) / 2


def l2(x, y):
    return (1 + x) / 2


def l3(x, y):
    return (1 + y) / 2


def magicphi(x, m, q):
    return (-1) ** q / (q + 1) * jac(q, m, 1, x)


# Basis functions on the triangle
# Vertex functions
def q_vertex(l1, l2, l3, p, i, j):
    return magicphi(2 * l1 / (1 - l2 - l3) - 1, 2, i) * (1 - l2 - l3) ** i * magicphi(2 * l2 / (1 - l3) - 1, 2 * i + 3, j) \
           * (1 - l3) ** j * magicphi(2 * l3 - 1, 2 * i + 2 * j + 4, p - 1 - i - j)


def phi1(x, y, p: int):
    # return -2**(-1-i)*(1-x[1])**i * (x[0]+x[1]) * jac(i, 2, 0, 2*(1 + x[0])/(1 - x[1]) - 1) * jac(j, 2*i + 3, 0, x[1])
    # return (-1) ** (p + 1) / p * l1(x, y) * jac(p - 1, 1, 1, 1 - 2 * l1(x, y))
    i = np.floor(p / 2)
    j = np.floor(i / 2)
    return l1(x, y) / 2 * (magicphi(2 * l2(x, y) / (1 - l3(x, y)) - 1, 2, i) * (1 - l3(x, y)) ** i *
                           magicphi(2 * l3(x, y) - 1, 2 * i + 3, p - i - 1)
                           + magicphi(2 * l3(x, y) / (1 - l2(x, y)) - 1, 2, i) * (1 - l2(x, y)) ** i *
                           magicphi(2 * l2(x, y) - 1, 2 * i + 3, p - i - 1)

                           )
    # return l1(x, y) / 3 * (q_vertex(l2(x, y), l3(x, y), 0, p, i, j) + q_vertex(l3(x, y), 0, l2(x, y), p, i, j) +
    #                        q_vertex(0, l2(x, y), l3(x, y), p, i, j))



def phi2(x, y, p: int):
    # return (-1) ** (p + 1) / p * l2(x, y) * jac(p - 1, 1, 1, 1 - 2 * l2(x, y))
    i = np.floor(p / 2)
    j = np.floor(i / 2)
    return l2(x, y) / 2 * (magicphi(2 * l1(x, y) / (1 - l3(x, y)) - 1, 2, i) * (1 - l3(x, y)) ** i *
                           magicphi(2 * l3(x, y) - 1, 2 * i + 3, p - i - 1)
                           + magicphi(2 * l3(x, y) / (1 - l1(x, y)) - 1, 2, i) * (1 - l1(x, y)) ** i *
                           magicphi(2 * l1(x, y) - 1, 2 * i + 3, p - i - 1))

    # return l2(x, y) / 3 * (q_vertex(l1(x, y), l3(x, y), 0, p, i, j) + q_vertex(l3(x, y), 0, l1(x, y), p, i, j) +
    #                        q_vertex(0, l1(x, y), l3(x, y), p, i, j))

def phi3(x, y, p: int):
    # return (-1) ** (p + 1) / p * l3(x, y) * jac(p - 1, 1, 1, 1 - 2 * l3(x, y))
    i = np.floor(p / 2)
    j = np.floor(i / 2)
    return l3(x, y) / 2 * (magicphi(2 * l1(x, y) / (1 - l2(x, y)) - 1, 2, i) * (1 - l2(x, y)) ** i *
                           magicphi(2 * l2(x, y) - 1, 2 * i + 3, p - i - 1)
                           + magicphi(2 * l2(x, y) / (1 - l1(x, y)) - 1, 2, i) * (1 - l1(x, y)) ** i *
                           magicphi(2 * l1(x, y) - 1, 2 * i + 3, p - i - 1))
    # return l3(x, y) / 3 * (q_vertex(l1(x, y), l2(x, y), 0, p, i, j) + q_vertex(l2(x, y), 0, l1(x, y), p, i, j) +
    #                        q_vertex(0, l1(x, y), l2(x, y), p, i, j))


# Interior functions
def psi(x, y, i: int, j: int):
    return -2 ** (-2 - i) * (1 + x) * (1 - y) ** (-1 + i) * (1 + y) * (x + y) * \
           jac(i - 1, 2, 2, (1 + 2 * x + y) / (1 - y)) * jac(-1 + j, 3 + 2 * i, 2, y)


# Edge functions
def p22(l1, l2, q):
    return jac(q, 2, 2, 2 * l2 / (l1 + l2) - 1) * (l1 + l2) ** q


def q_edge(l1, l2, p, i, j):
    return magicphi(2 * l1 / (1 - l2) - 1, 2 * i + 5, j) * (1 - l2) ** j * magicphi(2 * l2 - 1, 2 * i + 2 * j + 6,
                                                                                    p - 2 - i - j)
    # return magicphi(2 * l1 / (1 - l2) - 1, 2 * i + 5, p - i) * (1 - l1) ** (p - i)


def chi1(x, y, n, p):
    # return l2(x, y) * l3(x, y) * jac(n, 2, 2, l3(x, y) - l2(x, y))
    j = np.int(np.floor(p - n - 2) / 2)
    return l2(x, y) * l3(x, y) * p22(l2(x, y), l3(x, y), n) * (
            q_edge(l1(x, y), 0, p, n, j) + q_edge(0, l1(x, y), p, n, j)) / 2


def chi2(x, y, n, p):
    # return 4 * l1(x, y) * l3(x, y) * jac(n, 2, 2, l1(x, y) - l3(x, y))
    j = np.int(np.floor(p - n - 2) / 2)
    return l1(x, y) * l3(x, y) * p22(l1(x, y), l3(x, y), n) * (
            q_edge(l2(x, y), 0, p, n, j) + q_edge(0, l2(x, y), p, n, j)) / 2


def chi3(x, y, n, p):
    # return 4 * l1(x, y) * l2(x, y) * jac(n, 2, 2, l1(x, y) - l2(x, y))
    j = np.int(np.floor(p - n - 2) / 2)
    return l1(x, y) * l2(x, y) * p22(l1(x, y), l2(x, y), n) * (
            q_edge(l3(x, y), 0, p, n, j) + q_edge(0, l3(x, y), p, n, j)) / 2


# 1D basis function
def xik(x, k):
    return (-1) ** (k + 1) / k * (1 - x) / 2 * jac(k - 1, 1, 1, x)


def phi(x, m, q):
    return (-1) ** q / (q + 1) * jac(q, m, 1, x)

def nodal_1d_forward(x, p: int):
    # i = int(np.floor(p / 2))
    # j = int(np.floor(i / 2))
    # return (1 - x) / 2 * (
    #         phi(x, 2, i) + ((1 - x) / 2) ** (i) * phi(x, 2 * i + 3, j) + ((1 - x) / 2) ** (i + j) *
    #         phi(x, 2 * i + 2 * j + 4, p - 1 - i - j)) / 3

    return (1 - x)/2 * phi(x, 2, p-1)
    # return xik(x, p)


def internal_1d(x, i: int):
    return (1 - x) * (1 + x) * jac(i, 2, 2, x)

def quadtriangle(f, w00, w10, x00, x10):
    # # Generate points first (assumes w00 and w10 has same length)
    # points = np.zeros((2, len(w00) ** 2))
    # for i in range(len(w00)):
    #     points[:, i*len(w00): (i + 1) * len(w00)] = [(1 + x00[i]) * (1 - x10) / 2 - 1, x10]
    # return np.dot(w00, np.reshape(f(points), (len(w00), len(w00))).dot(w10/2))

    total = 0
    for i in range(len(w00)):
        total += w00[i] * np.dot(w10 / 2, f((1 + x00[i]) * (1 - x10) / 2 - 1, x10))
    return total


if __name__ == '__main__':
    for p in range(3, 51, 1):
        # First, find size of mass matrix
        num_vertices = 6
        num_edges = (p - 1) * 9
        num_faces = int((p - 1) * (p - 2) / 2 * 2) + int((p - 1) * (p - 1) * 3)  # Two faces from triangle,
        # and three quad like faces
        num_internal = int((p - 1) * (p - 2) / 2 * (p - 1))  # Internal triangle * internal interval

        total_dofs = num_vertices + num_edges + num_faces + num_internal
        boundary = total_dofs - num_internal
        mass = np.zeros((total_dofs, total_dofs))

        # Need to create quad points
        x00, w00 = rj(p + 1, 0, 0)
        x10, w10 = rj(p + 1, 1, 0)

        # Create list of lambda functions
        func_dofs = list()

        # Add nodal basis functions (total of 6)
        for i in range(3):
            for j in range(2):
                func_dofs.append((i ,j))

        # Add the edge basis functions which lies on triangles
        for i in range(3 * (p - 1)):
            for j in range(2):
                func_dofs.append((3 + i, j))

        # Now edges on the vertex of triangles
        for i in range(3):
            for j in range(p - 1):
                func_dofs.append((i, j + 2))

        # Add triangular faces
        for i in range(int((p - 1) * (p - 2) / 2)):
            for j in range(2):
                func_dofs.append((3 + 3 * (p - 1) + i, j))

        # Add square faces
        for i in range(3 * (p - 1)):
            for j in range(p - 1):
                func_dofs.append((3 + i, j + 2))

        # Add interior parts
        for i in range(int((p - 1) * (p - 2) / 2)):
            for j in range(p - 1):
                func_dofs.append((3 + 3 * (p - 1) + i, j + 2))

        # Now we create the 1D mass matrix for easy lookup
        interval_functions = list()
        interval_functions.append(lambda x: nodal_1d_forward(x, p))
        interval_functions.append(lambda x: nodal_1d_forward(-x, p))
        for i in range(p - 1):
            interval_functions.append(lambda x, i=i: internal_1d(x, i))

        interval_mass = np.zeros((p + 1, p + 1))
        for i in range(p + 1):
            for j in range(p + 1):
                interval_mass[i, j] = quad(lambda x: interval_functions[i](x) * interval_functions[j](x), -1, 1)[0]

        # Now we create the 2D triangle mass matrix for easy lookup
        triangle_functions = list()
        triangle_functions.append(lambda x, y: phi1(x, y, p))
        triangle_functions.append(lambda x, y: phi2(x, y, p))
        triangle_functions.append(lambda x, y: phi3(x, y, p))
        for i in range(p - 1):
            triangle_functions.append(lambda x, y, i=i: chi1(x, y, i, p))
        for i in range(p - 1):
            triangle_functions.append(lambda x, y, i=i: chi2(x, y, i, p))
        for i in range(p - 1):
            triangle_functions.append(lambda x, y, i=i: chi3(x, y, i, p))
        for i in range(1, p - 1):
            for j in range(1, p - i):
                triangle_functions.append(lambda x, y, i=i, j=j: psi(x, y, i, j))

        num_dofs_tri = int((p + 1) * (p + 2) / 2)
        triangle_mass = np.zeros((num_dofs_tri, num_dofs_tri))

        for i in range(num_dofs_tri):
            for j in range(i, num_dofs_tri):
                triangle_mass[i, j] = quadtriangle(lambda x, y: triangle_functions[i](x, y) * triangle_functions[j](x, y), w00, w10, x00, x10)
                triangle_mass[j, i] = triangle_mass[i, j]

        # pm = np.diag(1 / np.diag(triangle_mass))
        # eigs = np.real(np.linalg.eigvals(pm @ triangle_mass))
        # print(p, max(eigs) / min(eigs), max(eigs), min(eigs))

        # Now construct the mass matrix
        for index_i in range(boundary):
            for index_j in range(index_i, boundary):
                i = func_dofs[index_i]
                j = func_dofs[index_j]
                mass[index_i, index_j] = triangle_mass[i[0], j[0]] * interval_mass[i[1], j[1]]
                mass[index_j, index_i] = mass[index_i, index_j]

        for index_i in range(boundary, len(func_dofs)):
            for index_j in range(boundary):
                i = func_dofs[index_i]
                j = func_dofs[index_j]
                mass[index_i, index_j] = triangle_mass[i[0], j[0]] * interval_mass[i[1], j[1]]
                mass[index_j, index_i] = mass[index_i, index_j]

        for index_i in range(boundary, len(func_dofs)):
            i = func_dofs[index_i]
            j = func_dofs[index_i]
            mass[index_i, index_i] = triangle_mass[i[0], j[0]] * interval_mass[i[1], j[1]]

        # print(mass[0, 0])
        # Create Schur complement
        A = mass[0:boundary, 0:boundary]
        B = mass[0:boundary, boundary:]
        C = 1 / np.diag(mass[boundary:, boundary:])
        S = A - B.dot(np.diag(C).dot(B.T))

        # Create Precond (which is just diagonal I think)
        P = np.diag(1 / np.diag(S))

        eigs = np.real(np.linalg.eigvals(P @ S))
        print(p, max(eigs) / min(eigs), max(eigs), min(eigs))
