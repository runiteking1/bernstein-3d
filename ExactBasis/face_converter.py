import numpy as np
import scipy.special as special


def phi(x: np.ndarray, m: int, p: int):
    """
    Magic Cap phi function which helps with the "tensor" product on simplices

    :param x: values
    :param m: weight
    :param p: order
    """
    return (-1) ** p * special.eval_jacobi(p, m, 1, x) / (p + 1)


def edge_bottom(x: np.ndarray, i: int, j: int):
    """
    The edge basis function on the bottom face
    :param x: [x,y]
    :param i: index i
    :param j: index j
    :return: value on the face theta = -1
    """
    u = 2 * (1 + x[0]) / (1 - x[1]) - 1
    v = x[1]
    return (1 - u) / 2 * (1 + u) / 2 * special.eval_jacobi(i, 2, 2, u) * ((1 - v) / 2) ** (i + 2) * phi(v, 2 * i + 5, j)


def edge_front(x: np.ndarray, i: int, j: int, p: int):
    """
    The edge basis function on the front face
    :param x: [x,y]
    :param i: index i
    :param j: index j
    :return: value on the face theta = -1
    """
    u = 2 * (1 + x[0]) / (1 - x[1]) - 1
    v = x[1]
    return (1 - u) / 2 * (1 + u) / 2 * special.eval_jacobi(i, 2, 2, u) * ((1 - v) / 2) ** (i + j + 3) * \
            phi(v, 2 * i + 2 * j + 8, p - 2 - i - j)
# TODO: this is incorrect


def vertex_bottom(x: np.ndarray, i: int, j:int, p:int):
    """
    Vertex basis function on the bottom face
    """
    u = 2 * (1 + x[0]) / (1 - x[1]) - 1
    v = x[1]
    return (1 - u) / 2 * phi(u, 2, i) * ((1 - v) / 2) ** (i + 1) * phi(v, 2 * i + 3, j)


def vertex_front(x: np.ndarray, i: int, j:int, p:int):
    """
    Vertex basis function on the front face (e.g. eta = -1)
    """
    u = 2 * (1 + x[0]) / (1 - x[1]) - 1
    v = x[1]
    return (1 - u) / 2 * phi(u, 2, i) * ((1 - v) / 2) ** (i + j + 1) * phi(v, 2 * i + 2 * j + 4, p - 1 - i - j)


def vertex_side(x, i, j, p):
    """
    Vertex basis function
    """
    u = 2 * (1 + x[0]) / (1 - x[1]) - 1
    v = x[1]
    return ((1 - u) / 2) ** (i + 1) * phi(u, 2 * i + 3, j) * ((1 - v) / 2) ** (i + j + 1) * phi(v, 2 * i + 2 * j + 4, p - 1 - i - j)


def numr(m: int, r: int, alpha3: int, c: int, d: int):
    return (-1) ** (m - r - alpha3) * special.binom(m - r + c, alpha3) * special.binom(m - r + d, m - r - alpha3) / \
           special.binom(m, alpha3)


def numr_front(m: int, r: int, alpha3: int, c: int, d: int, k: int):
    """
    Needs different coefficient for the front face
    """
    return (-1) ** (m - r - alpha3) * special.binom(m - r + c, alpha3) * special.binom(m - r + d, m - r - alpha3) / \
           special.binom(m + k, alpha3)


def gammarj(r: int, j: int, a: int, b: int):
    if j < 0:
        return 0
    elif r < j:
        return 0
    else:
        return (-1.0) ** (r - j) * special.binom(r + a, j) * special.binom(r + b, r - j) / special.binom(r, j)


def gamma_sum(r: int, malpha3: int, alpha2: int, a: int, b: int):
    total = 0
    for l in range(malpha3 - r + 1):
        # print(gammarj(r, alpha2 - l, a, b),r,alpha2-l)
        total += gammarj(r, alpha2 - l, a, b) * special.binom(malpha3 - r, l) * special.binom(r, alpha2 - l) / \
                 special.binom(malpha3, alpha2)
    return total


def a_coef(m: int, r: int, alpha: np.ndarray, a: int, b: int, c: int, d: int):
    if alpha[2] <= m - r:
        return numr(m, r, alpha[2], c, d) * gamma_sum(r, m - alpha[2], alpha[1], a, b)
    else:
        return 0


def a_coef_front(m: int, r: int, alpha: np.ndarray, a: int, b: int, c: int, d: int, k: int):
    if alpha[2] <= m - r:
        return numr_front(m, r, alpha[2], c, d, k) * gamma_sum(r, m - alpha[2] + k, alpha[1], a, b)
    else:
        return 0


def l1(x):
    return .5 * (1 - x[0]) - .5 * (1 + x[1])


def l2(x):
    return .5 * (1 + x[0])


def l3(x):
    return .5 * (1 + x[1])


def bern(x: np.ndarray, alpha: np.ndarray):
    return special.binom(alpha[0] + alpha[1] + alpha[2], alpha[0]) * special.binom(alpha[1] + alpha[2], alpha[1]) * \
           l1(x) ** alpha[0] * l2(x) ** alpha[1] * l3(x) ** alpha[2]


def pmr_bern_rep(x: np.ndarray, m: int, r: int) -> float:
    """
    Given a function P^{2, 2,}_r(xi) ((1-eta)/2)^{r + k} P^{2r + 5, 1}_{m-r}(eta), gives the Bernstein coefficients
    and also evaluates it. This is verified with several small examples.

    :param x: point to evaluate the function
    :param m: m parameter
    :param r: r parameter
    :return: value at x using Bernstein coefficient
    """
    total = 0
    for alpha1 in range(m + 1):
        for alpha2 in range(m + 1 - alpha1):
            alpha3 = m - alpha1 - alpha2
            # print(i,j,k,a_coef(m, r, np.array([i,j,k]), 2, 2, 2*r + 5, 1), bern(x, [i,j,k]))
            total += a_coef(m, r, np.array([alpha1, alpha2, alpha3]), 2, 2, 2 * r + 5, 1) * bern(x, [alpha1, alpha2, alpha3])

    return total



def pmr_bern_rep_front(x: np.ndarray, m: int, r: int, k: int) -> float:
    """
    Given a function P^{2, 2,}_r(xi) ((1-eta)/2)^r P^{2r + 5, 1}_{m-r}(eta), gives the Bernstein coefficients
    and also evaluates it. This is verified with several small examples.

    :param x: point to evaluate the function
    :param m: m parameter
    :param r: r parameter
    :param p: p order
    :return: value at x using Bernstein coefficient
    """
    print("m", m, "r", r, "k", k)
    total = 0
    for alpha3 in range(m - r + 1):
        for alpha2 in range(m - alpha3 + k + 1):
            alpha1 = m - alpha3 - alpha2 + k
            # print(i,j,k,a_coef(m, r, np.array([i,j,k]), 2, 2, 2*r + 5, 1), bern(x, [i,j,k]))
            total += a_coef_front(m, r, np.array([alpha1, alpha2, alpha3]), 2, 2, 2 * r + 5, 1, k) * bern(x, [alpha1, alpha2, alpha3])

    return total


def edge_bottom_bern(x: np.ndarray, m: int, r: int) -> float:
    """
    Function which returns the value of the function of the non-symmetric basis on the bottom face using the
    solved Bernstein coefficient. This should match output of edge_bottom at any value for the same (m, r) parameter
    """
    total = 0
    # for i in range(m + 1):
    #     for j in range(m + 1 - i):
    #         k = m - i - j
    #         print(i,j,k,a_coef(m, r, np.array([i,j,k]), 2, 2, 2*r + 5, 1), bern(x, [i,j,k]))
    # total += a_coef(m, r, np.array([i,j,k]), 2, 2, 2*r + 5, 1) * bern(x, [i+1, j+1, k]) * (i + 1) * (j + 1) / \
    #          ((m + 1) * (m + 2))
    # print(i+1,j+1,k, a_coef(m, r, np.array([i,j,k]), 2, 2, 2*r + 5, 1)  * (i + 1) * (j + 1) / \
    #          ((m + 1) * (m + 2)))

    for k in range(m + 1):
        for j in range(m + 1 - k):
            i = m - k - j
            # print(i,j,k,a_coef(m, r, np.array([i,j,k]), 2, 2, 2*r + 5, 1), bern(x, [i,j,k]))
            total += a_coef(m, r, np.array([i, j, k]), 2, 2, 2 * r + 5, 1) * bern(x, [i + 1, j + 1, k]) * (i + 1) * (
                        j + 1) / \
                     ((m + 1) * (m + 2))
            print(i + 1, j + 1, k, a_coef(m, r, np.array([i, j, k]), 2, 2, 2 * r + 5, 1) * (i + 1) * (j + 1) / \
                  ((m + 1) * (m + 2)))

    return total * (-1) ** (m - r) / (m - r + 1)


def vertex_bottom_bern(x, m, r):
    """
    Function which returns the value of the function of the non-symmetric basis on the bottom face using the
    solved Bernstein coefficient. This should match output of vertex_bottom at any value for the same (m, r) parameter

    Correct.
    """
    total = 0
    for k in range(m + 1):
        for j in range(m + 1 - k):
            i = m - k - j
            # print(i,j,k,a_coef(m, r, np.array([i,j,k]), 2, 2, 2*r + 5, 1), bern(x, [i,j,k]))
            total += a_coef(m, r, np.array([i, j, k]), 2, 1, 2 * r + 3, 1) * bern(x, [i + 1, j, k]) * (i + 1)  / \
                     ((m + 1))
            # print(i + 1, j + 1, k, a_coef(m, r, np.array([i, j, k]), 2, 2, 2 * r + 5, 1) * (i + 1) * (j + 1) / \
            #       ((m + 1) * (m + 2)))

    return total * (-1) ** (m) / ((r + 1) * (m - r + 1))


def vertex_front_bern(x: np.ndarray, m: int, r: int, p: int) -> float:
    """
    Function which returns the value of the function of the non-symmetric basis on the bottom face using the
    solved Bernstein coefficient. This should match output of edge_bottom at any value for the same (m, r) parameter

    Correct (i think)
    """
    # print("m", m, "r", r, "p", p)
    # print("This implies that i", r, " and j", p - 1 - m)
    total = 0
    k = p - 1 - m
    for alpha3 in range(m - r + 1):
        for alpha2 in range(m - alpha3 + k + 1):
            alpha1 = m - alpha3 - alpha2 + k
            # print(i,j,k,a_coef(m, r, np.array([i,j,k]), 2, 2, 2*r + 5, 1), bern(x, [i,j,k]))
            # total += l1(x) * l2(x) * a_coef_front(m, r, np.array([alpha1, alpha2, alpha3]), 2, 2, 2 * r + 2 * p - 2 * m + 2, 1, p - 2 - m) * bern(x, [alpha1, alpha2, alpha3])
            total += a_coef_front(m, r, np.array([alpha1, alpha2, alpha3]), 2, 1, 2 * r + 2 * p - 2 * m + 2, 1, p - 1 - m) * bern(x, [alpha1 + 1, alpha2, alpha3]) * \
                     (alpha1 + 1)/ ((m + k + 1) )
            # print(alpha1 + 1, alpha2 + 1, alpha3, a_coef_front(m, r, np.array([alpha1, alpha2, alpha3]), 2, 2, 2 * r + 2 * p - 2 * m + 2, 1, p - 2 - m) * (alpha1 + 1) * (alpha2 + 1) / \
            #       ((m + k + 1) * (m + k + 2)))


    return (-1)**(m)/((r + 1) * (m - r + 1)) * total


def vertex_side_bern(x: np.ndarray, m: int, r: int, p: int) -> float:
    """
    Function which returns the value of the function of the non-symmetric basis on the bottom face using the
    solved Bernstein coefficient. This should match output of edge_bottom at any value for the same (m, r) parameter

    Correct (i think)
    """

    total = 0
    i = p - 1 - m

    for alpha3 in range(m - r + 1):
        for alpha2 in range(m - alpha3 + 1):
            alpha1 = m - alpha3 - alpha2

            # Constant for change
            c = special.factorial(p - 1 - i) / special.factorial(p)
            c *= special.factorial(alpha1 + i + 1) / special.factorial(alpha1)

            total += a_coef_front(m, r, np.array([alpha1, alpha2, alpha3]), 2 * i + 3, 1, 2 * i + 2 * j + 4, 1, 0) *\
                     bern(x, [alpha1 + i + 1, alpha2, alpha3]) * c

            print(alpha1 + i + 1, alpha2, alpha3)


    return (-1)**(m)/((r + 1) * (m - r + 1)) * total



def edge_front_bern(x: np.ndarray, m: int, r: int, p: int) -> float:
    """
    Function which returns the value of the function of the non-symmetric basis on the bottom face using the
    solved Bernstein coefficient. This should match output of edge_bottom at any value for the same (m, r) parameter
    """
    print("m", m, "r", r, "p", p)
    print("This implies that i", r, " and j", p - 3 - m)
    total = 0
    k = p - 2 - m
    for alpha3 in range(m - r + 1):
        for alpha2 in range(m - alpha3 + k + 1):
            alpha1 = m - alpha3 - alpha2 + k
            # print(i,j,k,a_coef(m, r, np.array([i,j,k]), 2, 2, 2*r + 5, 1), bern(x, [i,j,k]))
            # total += l1(x) * l2(x) * a_coef_front(m, r, np.array([alpha1, alpha2, alpha3]), 2, 2, 2 * r + 2 * p - 2 * m + 2, 1, p - 2 - m) * bern(x, [alpha1, alpha2, alpha3])
            total += a_coef_front(m, r, np.array([alpha1, alpha2, alpha3]), 2, 2, 2 * r + 2 * p - 2 * m + 2, 1, p - 2 - m) * bern(x, [alpha1 + 1, alpha2 + 1, alpha3]) * \
                     (alpha1 + 1) * (alpha2 + 1)/ ((m + k + 1) * (m + k + 2))
            print(alpha1 + 1, alpha2 + 1, alpha3, a_coef_front(m, r, np.array([alpha1, alpha2, alpha3]), 2, 2, 2 * r + 2 * p - 2 * m + 2, 1, p - 2 - m) * (alpha1 + 1) * (alpha2 + 1) / \
                  ((m + k + 1) * (m + k + 2)))


    return (-1)**(m - r)/(m - r + 1) * total


if __name__ == '__main__':
    # print(pmr_bern_rep([-.2, -.3], 8, 4)/(4 + 1))
    # TODO: test sign problems along the edge?
    i = 1
    j = 2
    # print(edge_bottom_bern([-.1, -.3], m, r))
    # print(edge_bottom([-.1, -.3], i, j))
    #
    # for i in range(5):
    #     for j in range(5):
    #         r = i
    #         m = r + j
    #         print(edge_bottom_bern([-.1, -.3], m, r) - edge_bottom([-.1, -.3], i, j))
    # print(a_coef(4, 0, [0,1,3], 2, 2, 5, 1))

    p = 5
    i = int(np.floor(p / 2))
    j = int(np.floor(i / 2))
    r = i
    m = r + j
    print(vertex_bottom([-.3, -.7], i, j, p))
    print(vertex_bottom_bern([-.3, -.7], m, r))

    r = i
    m = p - 1 - j
    print(vertex_front([-.3, -.7], i, j, p))
    print(vertex_front_bern([-.3, -.7], m, r, p))

    r = j
    m = p - 1 - i
    print(vertex_side([-.4, -.7], i, j, p))
    print(vertex_side_bern([-.4, -.7], m, r, p))
    # print(edge_front([-.3, -.6], 1, 4, 8))
    # print(edge_front_bern([-.3, -.6], 1, 1, 8))
    # print(pmr_bern_rep_front([-.3, -.6], 3, 2, 6))
    # print(edge_front_bern([-.4, -.3], 1, 1, 5))
    # print("")
    # print(edge_bottom_bern([-.4, -.3], 1, 1))
    # from scipy import integrate
    # print(integrate.dblquad(lambda x, y: bern([x,y], [0,0,0])*bern([x,y], [1,0,0]), -1, 1, lambda x: -1, lambda x: -x))
