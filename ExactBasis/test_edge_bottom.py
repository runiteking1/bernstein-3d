from unittest import TestCase


class TestEdge_bottom(TestCase):
    def test_edge_bottom(self):
        import face_converter
        import numpy as np
        np.testing.assert_almost_equal(face_converter.edge_bottom(np.array([-.2, -.4]), 0, 0), .12)
