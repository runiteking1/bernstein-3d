Point(1) = {0, 0, 0, 2.0};
Point(2) = {1, 0, 0, 2.0};
Point(3) = {0, 1, 0, 2.0};
Point(4) = {0, 0, 1, 2.0};
Point(5) = {1, 1, 0, 2.0};
Point(6) = {1, 0, 1, 2.0};
Point(7) = {0, 1, 1, 2.0};
Point(8) = {1, 1, 1, 2.0};

Line(1) = {7, 8};
Line(2) = {8, 6};
Line(3) = {6, 4};
Line(4) = {4, 7};
Line(5) = {7, 3};
Line(6) = {1, 3};
Line(7) = {8, 5};
Line(8) = {5, 3};
Line(9) = {5, 2};
Line(10) = {2, 1};
Line(11) = {1, 4};
Line(12) = {6, 2};

Line Loop(1) = {1, 2, 3, 4};
Plane Surface(1) = {1};
Line Loop(2) = {7, 9, -12, -2};
Plane Surface(2) = {2};
Line Loop(3) = {8, -6, -10, -9};
Plane Surface(3) = {3};
Line Loop(4) = {5, -6, 11, 4};
Plane Surface(4) = {4};
Line Loop(5) = {8, -5, 1, 7};
Plane Surface(5) = {5};
Line Loop(6) = {3, -11, -10, -12};
Plane Surface(6) = {6};
Surface Loop(1) = {2, 5, 3, 4, 6, 1};
Volume(1) = {1};

Physical Volume("Cube") = {1};
Physical Surface("Boundary") = {1,2,3,4,5,6};
//Physical Line("Boundary line") = {1,2,3,4,5,6,7,8,9,10,11,12};


