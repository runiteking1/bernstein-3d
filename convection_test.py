import numpy as np
from BernsteinFEM import BernsteinFEM
from Mesh import bernstein_mesh
from mass_preconditioner import NaivePreconditioner
from scipy.sparse.linalg import gmres
import read_msh
import scipy.sparse.linalg as spla


def force(x):
    return -2 * (1 + x[0]) * (1 + x[1]) * (1 + x[2]) * (3 + x[1] * (-2 + x[2]) - 2 * x[2] + x[0] * (-2 + x[1] + x[2]))


class gmres_counter(object):
    def __init__(self, disp=True):
        self._disp = disp
        self.niter = 0
    def __call__(self, rk=None):
        self.niter += 1
        if self._disp:
            print('iter %3i\trk = %s' % (self.niter, str(rk)))


if __name__ == '__main__':
    """
    Data saved in ref-element-cond.data
    """
    np.set_printoptions(linewidth=260, precision=4)
    delta_t = 0.1
    from scipy.sparse.linalg import LinearOperator
    for p in range(4, 5):
        vertices, elements, boundary = read_msh.load_file('cube.geo', refine=0)
        mesh = bernstein_mesh.BernsteinMesh(vertices, elements, boundary, p)
        fem = BernsteinFEM.BernsteinFEM(mesh)
        print("Elements", fem.elements)

        m, s, f = fem.assemble_matrix(force_func=force) # Force function to test with s + c
        c = fem.assemble_convection()
        print("Matrix constructed", m.shape)

        Solver = NaivePreconditioner(m, fem)
        print("Preconditioner constructed")

        M_x = lambda x: Solver.solve(x)
        M = spla.LinearOperator(m.shape, M_x)

        # fem.plot(np.linalg.solve((-s + -c).todense(), f))
        # counter = gmres_counter()
        # u = gmres(m + delta_t * c, f, callback=counter)[0]
        # print(p, delta_t, counter.niter)

        counter = gmres_counter()
        u = gmres(m + delta_t * c, f, M=M, callback=counter)[0]
        print(p, delta_t, counter.niter)

        # fem.plot(u)

