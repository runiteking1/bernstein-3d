import numpy as np
import read_msh

class Mesh(object):
    """
    General finite element mesh object for H1-conforming elements in 3D
    """

    def __init__(self, coordinates, elnode, dirchlet_bc, *, subdivided_num: int = 0, neumann_bc={}):
        """
        :type coordinates: np.ndarray
        :type elnode: np.ndarray
        :type dirchlet_bc: np.ndarray
        :type subdivided_num: int
        """

        self.neumann_bc = set(neumann_bc)
        self.dirchlet_bc = dirchlet_bc
        self.coordinates = coordinates
        self.subdiv = subdivided_num

        self.dirchlet_dofs = set()
        for edges in self.dirchlet_bc['lines']:
            self.dirchlet_dofs.add(edges[0])
            self.dirchlet_dofs.add(edges[1])

        # Preprocess elnode to make sure it is ordered
        for i in range(len(elnode)):
            elnode[i] = sorted(elnode[i])
        self.elnode = np.array(elnode, dtype=np.int)
        self.elements = len(self.elnode)

    def subdivide(self):
        """
        Refines the mesh by taking the midpoints and giving 4 triangles per single triangle

        TODO: Reference Gmsh instead

        :return: None
        """
        raise NotImplementedError("3D subdivide not implemented yet")


if __name__ == '__main__':
    # Two Element Mesh (Not Cube!)
    # coords_twoelement = np.array([[0, 0, 0], [0, 0, 1], [0, 1, 0], [1, 0, 0], [1, 1, 1]])
    # elnode_twoelement = np.array([[0, 1, 2, 3], [1, 2, 3, 4]])
    # bc_twoelement = {'faces': {(0,1,2), (0, 2, 3)}}
    vertices, elements, boundary = read_msh.load_file('../cube.msh')
    m = Mesh(vertices, elements, boundary)
