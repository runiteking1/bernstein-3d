import numpy as np

"""
Reads in a (basic) .msh file and outputs a msh with boundary conditions

Output from version 3.0.6 gmsh
"""

def load_file(name: str, refine: int = 0):
    # First, use gmsh to create msh and refine appropriately
    import subprocess
    subprocess.call(["gmsh", "-3", name])

    msh_name = name[0:-3] + 'msh'
    for i in range(refine):
        subprocess.call(["gmsh", "-refine", msh_name])

    file_in = open(msh_name, mode='r')

    for line in file_in:
        if "$Nodes" in line:
            num_nodes = int(file_in.readline())
            vertices = np.zeros((num_nodes, 3))
            for j in range(num_nodes):
                vertices[j, :] = np.array([float(x) for x in file_in.readline().strip().split(' ')[1:]])

        if "$Elements" in line:
            total_elements = int(file_in.readline())
            elements = list()
            boundary = list()
            for j in range(total_elements):
                # First classify between boundary and real element
                data = [int(x)-1 for x in file_in.readline().strip().split(' ')]

                # First case is triangle
                if data[1] == 1:
                    boundary.append(tuple(sorted(data[5:])))

                # Second case is tet
                elif data[1] == 3:
                    elements.append(sorted(data[5:]))

    faces = set(boundary)

    lines = set()
    # Now process the boundary to get lines lying on the boundary
    for face in faces:
        lines.add(tuple(sorted((face[0], face[1]))))
        lines.add(tuple(sorted((face[0], face[2]))))
        lines.add(tuple(sorted((face[1], face[2]))))
    boundaries = {'faces': faces, 'lines': lines}
    elements = np.array(elements)

    file_in.close()
    return vertices, elements, boundaries

if __name__ == '__main__':
    print(load_file('../cube.geo'))