from mesh import Mesh
import numpy as np
from position import pos3d
import read_msh

class BernsteinMesh(Mesh):
    """
    Extends Mesh object for Bernstein basis degrees of freedom.
    """

    def __init__(self, coordinates, elnode, dirchlet_bc, p: int, neumann_bc={}) -> None:
        """
        :rtype: object
        :type coordinates: np.ndarray
        :type elnode: np.ndarray
        :type dirchlet_bc: np.ndarray
        :type p: int
        :type subdiv: int
        """
        # Initialize variables
        super(BernsteinMesh, self).__init__(coordinates, elnode, dirchlet_bc, neumann_bc=neumann_bc)
        self.dofs_per_element = int((p + 1) * (p + 2) * (p + 3) / 6)
        self.p = p

        debug = False

        # Create a set of edges and faces
        edges = set()  # Edges are sorted from low dof to high dof
        faces = set()  # Faces are sorted from low dof to high dof
        for element in range(self.elements):
            edges.add(tuple(sorted((self.elnode[element, 0], self.elnode[element, 1]))))
            edges.add(tuple(sorted((self.elnode[element, 0], self.elnode[element, 2]))))
            edges.add(tuple(sorted((self.elnode[element, 0], self.elnode[element, 3]))))
            edges.add(tuple(sorted((self.elnode[element, 1], self.elnode[element, 2]))))
            edges.add(tuple(sorted((self.elnode[element, 1], self.elnode[element, 3]))))
            edges.add(tuple(sorted((self.elnode[element, 2], self.elnode[element, 3]))))

            faces.add(tuple(sorted((self.elnode[element, 0], self.elnode[element, 1], self.elnode[element, 2]))))
            faces.add(tuple(sorted((self.elnode[element, 0], self.elnode[element, 1], self.elnode[element, 3]))))
            faces.add(tuple(sorted((self.elnode[element, 0], self.elnode[element, 2], self.elnode[element, 3]))))
            faces.add(tuple(sorted((self.elnode[element, 1], self.elnode[element, 2], self.elnode[element, 3]))))

        # We need to store the number of edges and faces
        self.num_edges = len(edges)
        self.num_faces = len(faces)

        # edges = sorted(list(edges))
        # faces = sorted(list(faces))
        if p > 1:
            # Assign dofs to edges first, then faces
            dof = int(len(self.coordinates))
            edge_dofs = dict()  # associated each edge with a set of dofs
            for e in edges:
                edge_dofs[e] = range(dof, dof + p - 1)  # There are p - 1 total dofs on an edge

                if debug:
                    print('Edge from ', self.coordinates[e[0]], ' to ', self.coordinates[e[1]], ' has dofs ', edge_dofs[e], e)

                # Check if edge is on boundary, then add to dirchlet_dof
                if e in self.dirchlet_bc['lines']:
                    if debug:
                        print('\t ', e,' added to dirc')
                    self.dirchlet_dofs.update(range(dof, dof + p - 1))
                # TODO: Neumann boundary conditions

                dof += p - 1

            if p > 2:  # Assign dofs to each faces if only there is face dofs
                face_dofs = dict()
                face_dofs_counter = dict()
                dofs_in_face = int((self.p - 2) * (self.p - 1) / 2)
                face_counter = 0
                for f in faces:
                    face_dofs[f] = range(dof, dof + dofs_in_face)
                    face_dofs_counter[f] = int(face_counter)

                    if debug:
                        print('Face including ', self.coordinates[f[0]], self.coordinates[f[1]], ' and ', self.coordinates[f[2]], ' has dofs ',
                              face_dofs[f], f)
                    # Check if face is on boundary, then add to dirchlet_dof
                    if f in self.dirchlet_bc['faces']:
                        self.dirchlet_dofs.update(range(dof, dof + dofs_in_face))
                        if debug:
                            print('\t ', f,' added to dirc')

                    # TODO: Neumann boundary condition
                    dof += dofs_in_face
                    face_counter += 1

        # Now we carefully aggregate everything into an ELDOF list
        total = list()
        dofs_in_interior = int((self.p - 1) * (self.p - 2) * (self.p - 3) / 6)

        # Create a mapping from element to the nth face (will have to do for edges too)
        self.el_face = np.zeros((self.elements, 4), dtype=np.int)

        for element in range(self.elements):
            el_list = self.elnode[element].tolist()

            if self.p > 1:
                # Add edges
                el_list += edge_dofs[(self.elnode[element][0], self.elnode[element][1])]
                el_list += edge_dofs[(self.elnode[element][0], self.elnode[element][2])]
                el_list += edge_dofs[(self.elnode[element][0], self.elnode[element][3])]
                el_list += edge_dofs[(self.elnode[element][1], self.elnode[element][2])]
                el_list += edge_dofs[(self.elnode[element][1], self.elnode[element][3])]
                el_list += edge_dofs[(self.elnode[element][2], self.elnode[element][3])]

                if self.p > 2:
                    # Add faces
                    el_list += face_dofs[(self.elnode[element][0], self.elnode[element][1], self.elnode[element][2])]
                    el_list += face_dofs[(self.elnode[element][0], self.elnode[element][1], self.elnode[element][3])]
                    el_list += face_dofs[(self.elnode[element][0], self.elnode[element][2], self.elnode[element][3])]
                    el_list += face_dofs[(self.elnode[element][1], self.elnode[element][2], self.elnode[element][3])]

                    # Add to the index
                    self.el_face[element, 0] = face_dofs_counter[(self.elnode[element][0], self.elnode[element][1], self.elnode[element][2])]
                    self.el_face[element, 1] = face_dofs_counter[(self.elnode[element][0], self.elnode[element][1], self.elnode[element][3])]
                    self.el_face[element, 2] = face_dofs_counter[(self.elnode[element][0], self.elnode[element][2], self.elnode[element][3])]
                    self.el_face[element, 3] = face_dofs_counter[(self.elnode[element][1], self.elnode[element][2], self.elnode[element][3])]
                    if self.p > 3:
                        # Add interior dofs (order does not matter)
                        el_list += range(dof, dof + dofs_in_interior)
                        dof += dofs_in_interior

            total.append(el_list)

        # Now, finally make this a eldof
        self.eldof = np.array(total, dtype=np.int)

        # Create dof to face mapping
        if self.p > 2:
            # Use pos2d to create inverse
            self.face_dof_to_multi = dict()
            for i in range(self.p - 2):
                for j in range(self.p - 2 - i):
                    k = self.p - 3 - i - j
                    self.face_dof_to_multi[pos2d(i, j, self.p - 3)] = (i + 1, j + 1, k + 1)

            # Create dof to interior mapping
            if self.p > 3:
                # Use pos3d to create inverse
                self.int_dof_to_multi = dict()
                for i in range(self.p - 3):
                    for j in range(self.p - 3 - i):
                        for k in range(self.p - 3 - i - j):
                            l = self.p - 4 - i - j - k
                            self.int_dof_to_multi[pos3d(i, j, k, self.p - 4)] = (i + 1, j + 1, k + 1, l + 1)

        self.lookup_dof = dict()
        self.lookup_multi = dict()
        for i in range(self.dofs_per_element):
            self.lookup_dof[i] = self._lookup_dof(i)
            self.lookup_multi[self.lookup_dof[i]] = i

    def _lookup_dof(self, i) -> tuple:
        """
        TODO: This is fairly bad and hardcoded, anyway to make it more efficient?
        Creates a 1-1 correspondence between the dof in eldof and an multinomial; we will preprocess this function to
        obtain a faster version using dictionaries, lists or arrays.

        :param i:
        :return: tuple corresponding to multidof
        """
        # Vertices
        if i == 0:
            return self.p, 0, 0, 0
        i -= 1

        if i == 0:
            return 0, self.p, 0, 0
        i -= 1

        if i == 0:
            return 0, 0, self.p, 0
        i -= 1

        if i == 0:
            return 0, 0, 0, self.p
        i -= 1

        # 6 edges
        if i < (self.p - 1):
            return self.p - (i + 1), (i + 1), 0, 0
        i -= self.p - 1

        if i < (self.p - 1):
            return self.p - (i + 1), 0, i + 1, 0
        i -= self.p - 1

        if i < (self.p - 1):
            return self.p - (i + 1), 0, 0, i + 1
        i -= self.p - 1

        if i < (self.p - 1):
            return 0, self.p - (i + 1), i + 1, 0
        i -= self.p - 1

        if i < (self.p - 1):
            return 0, self.p - (i + 1), 0, i + 1
        i -= self.p - 1

        if i < (self.p - 1):
            return 0, 0, self.p - (i + 1), i + 1
        i -= self.p - 1

        # Now do faces
        dofs_in_face = (self.p - 2) * (self.p - 1) / 2
        if i < dofs_in_face:
            return (*self.face_dof_to_multi[i], 0)
        i -= dofs_in_face

        if i < dofs_in_face:
            a, b, c = self.face_dof_to_multi[i]
            return a, b, 0, c
        i -= dofs_in_face

        if i < dofs_in_face:
            a, b, c = self.face_dof_to_multi[i]
            return a, 0, b, c
        i -= dofs_in_face

        if i < dofs_in_face:
            a, b, c, = self.face_dof_to_multi[i]
            return 0, a, b, c
        i -= dofs_in_face

        return self.int_dof_to_multi[i]


def pos2d(i: int, j: int, p: int) -> int:
    """
    Returns the lexicographical order of the TRIANGULAR dofs points.

    See
    p = 8
    for i in range(p + 1):
        for j in range(p + 1 - i):
            k = p - i - j
            print(i, j, k, pos2d(i, j, p))

    :param i:
    :param j:
    :return:
    """
    return int(((i - 2 * p - 3) * (-i)) / 2 + j)


if __name__ == '__main__':
    # One Element Mesh
    coords_oneelement = np.array([[0, 0, 0], [0, 0, 1], [0, 1, 0], [1, 0, 0]])
    elnode_oneelement = np.array([[1, 0, 2, 3]])
    bc_oneelement = np.array([])
    m = BernsteinMesh(coords_oneelement, elnode_oneelement, bc_oneelement, 6)

    # Two Element Mesh (Not Cube!)
    coords_twoelement = np.array([[0, 0, 0], [0, 0, 1], [0, 1, 0], [1, 0, 0], [1, 1, 1]])
    elnode_twoelement = np.array([[0, 1, 2, 3], [1, 2, 3, 4]])
    faces = set([(0, 1, 2), (0, 1, 3), (0, 2, 3), (1, 2, 4), (1, 3, 4), (2, 3, 4)])
    lines = set()
    for face in faces:
        lines.add(tuple(sorted((face[0], face[1]))))
        lines.add(tuple(sorted((face[0], face[2]))))
        lines.add(tuple(sorted((face[1], face[2]))))
    bc_twoelement = {'faces': faces, 'lines': lines}
    m = BernsteinMesh(coords_twoelement, elnode_twoelement, bc_twoelement, 2)

    # Cube
    # vertices, elements, boundary = read_msh.load_file('../cube.msh')
    # m = BernsteinMesh(vertices, elements, boundary, 4)
