#cython: boundscheck=False, wraparound=False, cdivision=True

def duffy(const double[::1] t,const double[:, ::1] x):
    """Duffy transformation for 3D"""
    cdef double l1, l2, l3, l4
    l1 = t[0]
    l2 = t[1] * (1 - l1)
    l3 = t[2] * (1 - l1 - l2)
    l4 = 1 - l1 - l2 - l3

    return (x[0, 0] * l1 + x[0, 1] * l2 + x[0, 2]* l3 + x[0, 3]*l4,
            x[1, 0] * l1 + x[1, 1] * l2 + x[1, 2]* l3 + x[1, 3]*l4,
            x[2, 0] * l1 + x[2, 1] * l2 + x[2, 2]* l3 + x[2, 3]*l4)
    # return x[:, 0] * l1 + x[:, 1] * l2 + x[:, 2] * l3 + x[:, 3] * l4 (slower because double is not declared)
