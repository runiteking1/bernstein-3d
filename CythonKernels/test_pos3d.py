from unittest import TestCase
import numpy as np
import pos3d as new
from mass import pos3d as baseline

class TestPos3d(TestCase):
    def test_pos3d(self):
        p = 8
        for i in range(p+1):
            for j in range(p+1-i):
                k = p - i - j
                np.testing.assert_almost_equal(new.pos3d(i, j, k, p), baseline(i, j, k, p))

