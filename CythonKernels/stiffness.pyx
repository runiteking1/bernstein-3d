#cython: boundscheck=False, wraparound=False, cdivision=True

import numpy as np

cdef pos3d(int i, int j, int k, int p):
    """
    Gives a unique identifier to each index set

    :param p: the order
    :return: numbers the darn thing
    """
    return i*(11 + i*i + 12*p + 3*p*p - 3*i *(2+p))/6 + (-j) * (j-2*(p-i) - 3)/2 + k

def stiffness(int n, const double[:, :, :, :, ::1] moments, const long[:, ::1] binom, const double [:, ::1] grad_bary,
              double[:, :, :, :, ::1] mukl, double[:, ::1] elem_stiff):
    """
    Basic skeleton for element mass matrix

    :param grad_bary: gradient of barycentric coordinate with first term opposite v0, second opposite v1 etc.
    :param n: order of approximation
    :param moments: array of moments arranged such that (alpha_1, alpha_2, alpha_3,alpha_4) corresponds to
            moments[alpha_1, alpha_2, alpha_3]. This should be of order 2n - 2
    :return: Stiffness matrix
    arranged using pos3d
    """
    cdef int k, l, alpha_1, beta_1, alpha_2, beta_2, alpha_3, beta_3
    cdef double w_1, w_2, w_3, w_4

    # Precompute
    # mukl = np.zeros((2 * n - 1, 2 * n - 1, 2 * n - 1, 4, 4))
    for k in range(4):
        for l in range(4):
            for alpha_1 in range(2 * n - 1):
                for alpha_2 in range(2 * n - 1 - alpha_1):
                    for alpha_3 in range(2 * n - 1 - alpha_1 - alpha_2):
                        mukl[alpha_1, alpha_2, alpha_3, k, l] = np.dot(grad_bary[l, :],
                                                                       np.dot(moments[alpha_1, alpha_2, alpha_3],
                                                                              grad_bary[k, :]))
    # Create output matrix
    # elem_stiff = np.zeros((int(binom[n+3,3]), int(binom[n+3,3])))
    for alpha_1 in range(binom[n+3, 3]):
        for alpha_2 in range(binom[n+3, 3]):
            elem_stiff[alpha_1, alpha_2] = 0

    # Scale down
    n = n - 1

    cdef int k0, k1, k2, k3, l0, l1, l2, l3
    for alpha_1 in range(n + 1):
        for beta_1 in range(n + 1):
            w_1 = binom[alpha_1 + beta_1, alpha_1]
            w_1 = w_1/ binom[2 * n, n]

            for alpha_2 in range(n - alpha_1 + 1):
                for beta_2 in range(n - beta_1 + 1):
                    w_2 = w_1 * binom[alpha_2 + beta_2, alpha_2]

                    for alpha_3 in range(n - alpha_1 - alpha_2 + 1):
                        for beta_3 in range(n - beta_1 - beta_2 + 1):
                            w_3 = w_2 * binom[alpha_3 + beta_3, alpha_3]

                            w_4 = w_3 * binom[2 * n - alpha_1 - alpha_2 - alpha_3 - beta_1 - beta_2 - beta_3,
                                              n - alpha_1 - alpha_2 - alpha_3]

                            #k = 0, l = 0
                            k0 = pos3d(alpha_1 + 1, alpha_2, alpha_3, n+1)
                            l0 = pos3d(beta_1 + 1, beta_2, beta_3, n+1)
                            elem_stiff[k0, l0] += \
                                        (n + 1)**2 * w_4 * mukl[alpha_1 + beta_1, alpha_2 + beta_2, alpha_3 + beta_3, 0, 0]

                            # k = 0, l = 1
                            l1 = pos3d(beta_1, beta_2 + 1, beta_3, n+1)
                            elem_stiff[k0, l1] += \
                                        (n + 1)**2 * w_4 * mukl[alpha_1 + beta_1, alpha_2 + beta_2, alpha_3 + beta_3, 0, 1]

                            # k = 0, l = 2
                            l2 = pos3d(beta_1, beta_2, beta_3+1, n+1)
                            elem_stiff[k0, l2] += \
                                        (n + 1)**2 * w_4 * mukl[alpha_1 + beta_1, alpha_2 + beta_2, alpha_3 + beta_3, 0, 2]

                            # k = 0, l = 3
                            l3 = pos3d(beta_1, beta_2, beta_3, n+1)
                            elem_stiff[k0, l3] += \
                                        (n + 1)**2 * w_4 * mukl[alpha_1 + beta_1, alpha_2 + beta_2, alpha_3 + beta_3, 0, 3]

                            k1 = pos3d(alpha_1, alpha_2+1, alpha_3, n+1)
                            #k = 1, l = 0
                            elem_stiff[k1, l0] += \
                                        (n + 1)**2 * w_4 * mukl[alpha_1 + beta_1, alpha_2 + beta_2, alpha_3 + beta_3, 1, 0]

                            # k = 1, l = 1
                            elem_stiff[k1, l1] += \
                                        (n + 1)**2 * w_4 * mukl[alpha_1 + beta_1, alpha_2 + beta_2, alpha_3 + beta_3, 1, 1]

                            # k = 1, l = 2
                            elem_stiff[k1, l2] += \
                                        (n + 1)**2 * w_4 * mukl[alpha_1 + beta_1, alpha_2 + beta_2, alpha_3 + beta_3, 1, 2]

                            # k = 1, l = 3
                            elem_stiff[k1, l3] += \
                                        (n + 1)**2 * w_4 * mukl[alpha_1 + beta_1, alpha_2 + beta_2, alpha_3 + beta_3, 1, 3]


                            k2 = pos3d(alpha_1, alpha_2, alpha_3+1, n+1)
                            #k = 2, l = 0
                            elem_stiff[k2, l0] += \
                                        (n + 1)**2 * w_4 * mukl[alpha_1 + beta_1, alpha_2 + beta_2, alpha_3 + beta_3, 2, 0]

                            # k = 2, l = 1
                            elem_stiff[k2, l1] += \
                                        (n + 1)**2 * w_4 * mukl[alpha_1 + beta_1, alpha_2 + beta_2, alpha_3 + beta_3, 2, 1]

                            # k = 2, l = 2
                            elem_stiff[k2,l2] += \
                                        (n + 1)**2 * w_4 * mukl[alpha_1 + beta_1, alpha_2 + beta_2, alpha_3 + beta_3, 2, 2]

                            # k = 2, l = 3
                            elem_stiff[k2,l3] += \
                                        (n + 1)**2 * w_4 * mukl[alpha_1 + beta_1, alpha_2 + beta_2, alpha_3 + beta_3, 2, 3]

                            k3 = pos3d(alpha_1, alpha_2, alpha_3, n+1)
                            #k = 3, l = 0
                            elem_stiff[k3, l0] += \
                                        (n + 1)**2 * w_4 * mukl[alpha_1 + beta_1, alpha_2 + beta_2, alpha_3 + beta_3, 3, 0]

                            # k = 3, l = 1
                            elem_stiff[k3, l1] += \
                                        (n + 1)**2 * w_4 * mukl[alpha_1 + beta_1, alpha_2 + beta_2, alpha_3 + beta_3, 3, 1]

                            # k = 3, l = 2
                            elem_stiff[k3,l2] += \
                                        (n + 1)**2 * w_4 * mukl[alpha_1 + beta_1, alpha_2 + beta_2, alpha_3 + beta_3, 3, 2]

                            # k = 3, l = 3
                            elem_stiff[k3,l3] += \
                                        (n + 1)**2 * w_4 * mukl[alpha_1 + beta_1, alpha_2 + beta_2, alpha_3 + beta_3, 3, 3]

                            # for k in range(3, 4):
                            #     for l in range(4):
                            #         indices_1 = ei[k] + [alpha_1, alpha_2, alpha_3]
                            #         indices_2 = ei[l] + [beta_1, beta_2, beta_3]

                                    # elem_stiff[pos3d(*indices_1, n + 1), pos3d(*indices_2, n + 1)] += \
                                    #     (n + 1)**2 * w_4 * np.dot(grad_bary[k], np.dot(
                                    #     moments[alpha_1 + beta_1, alpha_2 + beta_2, alpha_3 + beta_3], grad_bary[l]))
                                    # elem_stiff[pos3d(indices_1[0], indices_1[1], indices_1[2], n + 1),
                                    #            pos3d(indices_2[0], indices_2[1], indices_2[2], n + 1)] += \
                                    #     (n + 1)**2 * w_4 * mukl[alpha_1 + beta_1, alpha_2 + beta_2, alpha_3 + beta_3, k, l]

    # return elem_stiff
