#cython: boundscheck=False, wraparound=False, cdivision=True
import numpy as np

def moment(double[:, :, ::1] f0, int q, int n, double[:, ::1] xis, double[:, ::1] omegas):
    """
    Evaluates moment

    :param f0: aray corresponding to values of a function f at Stroud nodes
    :param q: number of quadrature points
    :param n: order of approximation
    :param xis: location of quadrature points
    :param omegas: weights of quadrature rule
    :return: array composed of Bernstein-Bezier moments fo f using Stroud rule
    """

    step0 = np.zeros((n + 1, q, q))
    step1 = np.zeros((n + 1, n + 1, q))
    step2 = np.zeros((n + 1, n + 1, n + 1))

    _momentstep1(f0, q, n, xis, omegas, step0)
    _momentstep2(step0, q, n, xis, omegas, step1)
    _momentstep3(step1, q, n, xis, omegas, step2)

    return step2


cdef _momentstep1(double[:, :, ::1] fin, int q, int n, double[:, ::1] xis, double[:, ::1] omegas, double[:, :, ::1] fout):
    cdef int i_l, alpha_l, i_2, i_3, d, l
    cdef double xi, omega, r, s, w

    d = 3
    l = 1
    for i_l in range(q):
        xi = xis[d - l, i_l]
        omega = omegas[d - l, i_l]

        s = 1 - xi
        r = xi / s

        w = omega * s ** (n)

        for alpha_l in range(n + 1):
            for i_2 in range(q):
                for i_3 in range(q):
                    fout[alpha_l, i_2, i_3] += w * fin[i_l, i_2, i_3]

            w *= r * (n - alpha_l) / (1 + alpha_l)


cdef _momentstep2(double[:, :, ::1] fin, int q, int n, double[:, ::1] xis,double[:, ::1] omegas,double[:, :, ::1] fout):
    cdef int i_l, alpha_1, alpha_l, i_3, d, l
    cdef double xi, omega, r, s, w

    d = 3
    l = 2

    for i_l in range(q):
        xi = xis[d - l, i_l]
        omega = omegas[d - l, i_l]

        s = 1 - xi
        r = xi / s

        for alpha_1 in range(n + 1):
            w = omega * (s ** (n - alpha_1))

            for alpha_l in range(n - alpha_1 + 1):
                for i_3 in range(q):
                    fout[alpha_1, alpha_l, i_3] += w * fin[alpha_1, i_l, i_3]

                w *= r * (n - alpha_1 - alpha_l) / (1 + alpha_l)


cdef _momentstep3(double[:, :, ::1] fin,int q, int n,double[:, ::1]  xis,double[:, ::1]  omegas,double[:, :, ::1]  fout):
    cdef int i_l, alpha_1, alpha_2, alpha_l, d, l;
    cdef double xi, omega, r, s, w

    d = 3
    l = 3

    for i_l in range(q):
        xi = xis[d - l, i_l]
        omega = omegas[d - l, i_l]

        s = 1 - xi
        r = xi / s

        for alpha_1 in range(n + 1):
            for alpha_2 in range(n + 1 - alpha_1):
                w = omega * (s ** (n - alpha_1 - alpha_2))

                for alpha_l in range(n - alpha_1 - alpha_2 + 1):
                    fout[alpha_1, alpha_2, alpha_l] += w * fin[alpha_1, alpha_2, i_l]

                    w *= r * (n - alpha_1 - alpha_2 - alpha_l) / (1 + alpha_l)
