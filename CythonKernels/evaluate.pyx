#cython: boundscheck=False, wraparound=False, cdivision=True
import numpy as np

def evaluate(double[:, :, ::1] c0, int q, int n, double[:, ::1] xis):
    """
    Algorithm 1 for 3D

    :param xis: Array of Stroud rule points (do not need weights)
    :param c0: BB-vector in 3d c0[0, 1, 2] corresponds to (0, 1, 2, n-3)
    :param q: Number of quadrature points
    :param n: Order
    :return: Array with entrees equal to value of BB-vector at Stroud node
    """
    # arrays to store outputs of
    step1 = np.zeros((n + 1, n + 1, q))
    step2 = np.zeros((n + 1, q, q))
    step3 = np.zeros((q, q, q))

    _evalstep1(c0, n, q, xis, step1)
    _evalstep2(step1, n, q, xis, step2)
    _evalstep3(step2, n, q, xis, step3)
    return step3


def _evalstep1(double[:, :, ::1] cIn, int n, int q, double[:, ::1] xis, cOut: np.ndarray):
    cdef int l = 3
    cdef int d = 3

    cdef int i_l, alpha1, alpha2, alpha_l
    cdef double xi, s, r, w
    for i_l in range(q):
        xi = xis[d - l, i_l]

        s = 1 - xi
        r = xi / s

        for alpha1 in range(n + 1):
            for alpha2 in range(n + 1 - alpha1):
                w = s ** (n - alpha1 - alpha2)
                for alpha_l in range(n - alpha1 - alpha2 + 1):
                    # For each (i_{l+1}, i_d) in {1, q}^{d-l} do
                    cOut[alpha1, alpha2, i_l] += w * cIn[alpha1, alpha2, alpha_l]

                    w *= r * (n - alpha1 - alpha2 - alpha_l) / (1.0 + alpha_l)


def _evalstep2(double[:, :, ::1] cIn, int n, int q, double[:, ::1] xis, cOut: np.ndarray):
    cdef int l = 2
    cdef int d = 3

    cdef int i_l, alpha1, alpha_l, i_d
    cdef double xi, s, r, w

    for i_l in range(q):
        xi = xis[d - l, i_l]

        s = 1 - xi
        r = xi / s

        for alpha1 in range(n + 1):
            w = s ** (n - alpha1)
            for alpha_l in range(n - alpha1 + 1):
                for i_d in range(q):
                    cOut[alpha1, i_l, i_d] += w * cIn[alpha1, alpha_l, i_d]

                w *= r * (n - alpha1 - alpha_l) / (1.0 + alpha_l)


def _evalstep3(double[:, :, ::1] cIn, int n, int q, double[:, ::1] xis, cOut: np.ndarray):
    cdef int l = 1
    cdef int d = 3
    cdef double xi, s, r, w
    cdef int i_l, alpha_l, i_2, i_3

    for i_l in range(q):
        xi = xis[d - l, i_l]

        s = 1 - xi
        r = xi / s

        w = s ** n
        for alpha_l in range(n + 1):
            for i_2 in range(q):
                for i_3 in range(q):
                    cOut[i_l, i_2, i_3] += w * cIn[alpha_l, i_2, i_3]

            w *= r * (n - alpha_l) / (1.0 + alpha_l)
