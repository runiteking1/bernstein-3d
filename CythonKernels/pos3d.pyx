def pos3d(const int i, const int j, const int k, const int p):
    """
    Gives a unique identifier to each index set

    :param p: the order
    :return: numbers the darn thing
    """
    return i*(11 + i*i + 12*p + 3*p*p - 3*i *(2+p))/6 + (-j) * (j-2*(p-i) - 3)/2 + k
