import numpy as np

def pos3d(int i, int j, int k, int p):
    """
    Gives a unique identifier to each index set

    :param p: the order
    :return: numbers the darn thing
    """
    return i*(11 + i*i + 12*p + 3*p*p - 3*i *(2+p))/6 + (-j) * (j-2*(p-i) - 3)/2 + k

def convection(int n, const double[:, :, :, ::1] moments, const long[:, ::1] binom, const double [:, ::1] grad_bary,
               double[:, :, :, ::1] mukl, double[:, ::1] elem_convect):
    """
    Basic skeleton for element convection matrix

    :param grad_bary: gradient of barycentric coordinate with first term opposite v0, second opposite v1 etc.
    :param n: order of approximation
    :param moments: array of moments arranged such that (alpha_1, alpha_2, alpha_3,alpha_4) corresponds to
            moments[alpha_1, alpha_2, alpha_3]. This should be of order 2n - 2
    :return: Stiffness matrix
    arranged using pos3d
    """

    cdef int k, l, alpha_1, beta_1, alpha_2, beta_2, alpha_3, beta_3
    cdef double w_1, w_2, w_3, w_4

    # Precompute
    # mukl = np.zeros((2 * n - 1, 2 * n - 1, 2 * n - 1, 4, 4))
    for k in range(4):
        for alpha_1 in range(2 * n):
            for alpha_2 in range(2 * n - alpha_1):
                for alpha_3 in range(2 * n - alpha_1 - alpha_2):
                    mukl[alpha_1, alpha_2, alpha_3, k] = np.dot(moments[alpha_1, alpha_2, alpha_3],
                                                                              grad_bary[k, :])


    # Create output matrix
    # elem_stiff = np.zeros((int(binom[n+3,3]), int(binom[n+3,3])))
    for alpha_1 in range(binom[n+3, 3]):
        for alpha_2 in range(binom[n+3, 3]):
            elem_convect[alpha_1, alpha_2] = 0

    cdef int k0, k1, k2, k3, l0, l1, l2, l3, mult_n, mult_m

    # A bit different than before since it's skew symmetric
    mult_n = n
    mult_m = n - 1
    for alpha_1 in range(mult_m + 1):
        for beta_1 in range(mult_n + 1):
            w_1 = binom[alpha_1 + beta_1, alpha_1]
            w_1 = w_1/ binom[mult_m + mult_n, mult_m]

            for alpha_2 in range(mult_m - alpha_1 + 1):
                for beta_2 in range(mult_n - beta_1 + 1):
                    w_2 = w_1 * binom[alpha_2 + beta_2, alpha_2]

                    for alpha_3 in range(mult_m - alpha_1 - alpha_2 + 1):
                        for beta_3 in range(mult_n - beta_1 - beta_2 + 1):
                            w_3 = w_2 * binom[alpha_3 + beta_3, alpha_3]

                            w_4 = w_3 * binom[mult_m - alpha_1 - alpha_2 - alpha_3 + mult_n - beta_1 - beta_2 - beta_3,
                                              mult_m - alpha_1 - alpha_2 - alpha_3]

                            beta = pos3d(beta_1, beta_2, beta_3, n)

                            alpha = pos3d(alpha_1 + 1, alpha_2, alpha_3, n)
                            elem_convect[alpha, beta] += \
                            n * w_4 * mukl[alpha_1 + beta_1, alpha_2 + beta_2, alpha_3 + beta_3, 0]

                            # k = 0, l = 1
                            alpha = pos3d(alpha_1, alpha_2 + 1, alpha_3, n)
                            elem_convect[alpha, beta] += \
                            n * w_4 * mukl[alpha_1 + beta_1, alpha_2 + beta_2, alpha_3 + beta_3, 1]

                            # k = 0, l = 2
                            alpha = pos3d(alpha_1, alpha_2, alpha_3 + 1, n)
                            elem_convect[alpha, beta] += \
                            n * w_4 * mukl[alpha_1 + beta_1, alpha_2 + beta_2, alpha_3 + beta_3, 2]

                            # k = 0, l = 3
                            alpha = pos3d(alpha_1, alpha_2, alpha_3, n)
                            elem_convect[alpha, beta] += \
                            n* w_4 * mukl[alpha_1 + beta_1, alpha_2 + beta_2, alpha_3 + beta_3, 3]
