import mass

if __name__ == '__main__':
    from scipy import special
    from duffy import duffy
    from moment import moment
    import numpy as np
    np.set_printoptions(precision=6, linewidth=160)

    # Create tetrahedron
    vertices = np.zeros((3, 4))
    vertices[:, 0] = [1, 0, 0] # Corresponds to (2, 0, 0, 0)
    vertices[:, 1] = [0, 1, 0] # Corresponds to (0, 2, 0, 0)
    vertices[:, 2] = [0, 0, 1] # Corresponds to (0, 0, 2, 0)
    vertices[:, 3] = [0, 0, 0] # Corresponds to (0, 0, 0, 2)

    # First, generate the quadrature points
    p = 8
    my_q = p + 2
    my_xis = np.zeros((3, my_q))
    my_omegas = np.zeros((3, my_q))
    for i in range(3):
        nodes, weights = special.roots_sh_jacobi(my_q, i + 1, 1)
        my_xis[i, :] = nodes
        my_omegas[i, :] = weights

    def f(x):
        return 1

    fa = np.zeros((my_q, my_q, my_q))
    for i in range(my_q):
        for j in range(my_q):
            for k in range(my_q):
                fa[i, j, k] = f(duffy([my_xis[2, i], my_xis[1, j], my_xis[0, k]], vertices))

    # Return moment (need to scale correctly with volume)
    mmts = moment(fa, my_q, 2*p, my_xis, my_omegas)
    print(mass.mass(p, mmts))
