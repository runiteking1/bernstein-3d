#cython: boundscheck=False, wraparound=False, cdivision=True

"""
Computes the element mass matrix in 3D (Cythonized, though not completely optimized)
"""

import numpy as np
# from scipy.special import binom

cdef pos3d(int i, int j, int k, int p):
    """
    Gives a unique identifier to each index set

    :param p: the order
    :return: numbers the darn thing
    """
    return int(i*(11 + i*i + 12*p + 3*p*p - 3*i *(2+p))/6 + (-j) * (j-2*(p-i) - 3)/2 + k)


def mass(const int n, const double[:, :, ::1] moments, const long[:, ::1] binom, double[:, ::1] elem_mass):
    """
    Basic skeleton for element mass matrix

    :param n: order of approximation
    :param moments: array of moments arranged such that (alpha_1, alpha_2, alpha_3, alpha_4) corresponds to moments[alpha_1, alpha_2, alpha_3]
    :return: mass matrix arranged using pos3d
    """

    cdef int alpha_1, beta_1, alpha_2, beta_2, alpha_3, beta_3
    cdef double w_1, w_2, w_3, w_4

    for alpha_1 in range(n + 1):
        for beta_1 in range(n + 1):
            w_1 = binom[alpha_1 + beta_1, alpha_1]
            w_1 = w_1 / binom[2 * n, n]

            for alpha_2 in range(n - alpha_1 + 1):
                for beta_2 in range(n - beta_1 + 1):
                    w_2 = w_1 * binom[alpha_2 + beta_2, alpha_2]

                    for alpha_3 in range(n - alpha_1 - alpha_2 + 1):
                        for beta_3 in range(n - beta_1 - beta_2 + 1):
                            w_3 = w_2 * binom[alpha_3 + beta_3, alpha_3]

                            w_4 = w_3 * binom[2 * n - alpha_1 - alpha_2 - alpha_3 - beta_1 - beta_2 - beta_3,
                                              n - alpha_1 - alpha_2 - alpha_3]

                            elem_mass[pos3d(alpha_1, alpha_2, alpha_3, n), pos3d(beta_1, beta_2, beta_3, n)] = \
                                    w_4 * moments[alpha_1 + beta_1, alpha_2 + beta_2, alpha_3 + beta_3]

    # return elem_mass
